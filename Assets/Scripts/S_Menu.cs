﻿using UnityEngine;
using CharacterController_1Person;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

public class S_Menu : MonoBehaviour
{
    public static bool headBobbing;
    public static float sensitivityCam;
    public static float soundMaster;
    public static float soundMusic;
    public static float soundNarrator;
    public static float soundEffect;
    public GameObject optionMenu;
    public GameObject mainMenu;
    public GameObject master;
    public GameObject play;
    public GameObject Credit;
    public GameObject Retourbtn;
    public InputManager InputManager;
    public bool newGamePadConnect;
    GameObject eventsyst;
    public Animator optionAnimation;
    [Header("Resolution")]
    public TMPro.TMP_Dropdown resolutionDropdown;
    Resolution[] resolutions;
    //private int currentTime = 0;
    public string sceneName;

    public FMODUnity.StudioEventEmitter menuHoverSound;
    public FMODUnity.StudioEventEmitter menuPlaySound;
    public FMODUnity.StudioEventEmitter menuClickSound;
    public FMODUnity.StudioEventEmitter menuBackSound;
    public S_MusicManager musicManager;

    private void Start()
    {
        //menuSounds = gameObject.GetComponent<FMODUnity.StudioEventEmitter>();

        resolutions = Screen.resolutions;
        resolutionDropdown.ClearOptions();
        List<string> options = new List<string>();
        int currentResolutionIndex =  0;
        for (int i = 0; i < resolutions.Length; i++)
        {
            string option = resolutions[i].width + "x" + resolutions[i].height;
            options.Add(option);

            if(resolutions[i].width == Screen.currentResolution.width && resolutions[i].height == Screen.currentResolution.height)
            {
                currentResolutionIndex = i;
            }
        }
            
        resolutionDropdown.AddOptions(options);
        resolutionDropdown.value = currentResolutionIndex;
        resolutionDropdown.RefreshShownValue();
        
        eventsyst = GameObject.Find("EventSystem");
       if (InputManager.gamePadConnect == true)
         {
             eventsyst.GetComponent<UnityEngine.EventSystems.EventSystem>().SetSelectedGameObject(play);
             Cursor.visible = false;
            Debug.Log("true");
        }
         else
         {
             Debug.Log("false");
             Cursor.visible = true;
             eventsyst.GetComponent<UnityEngine.EventSystems.EventSystem>().SetSelectedGameObject(null);
         }
        newGamePadConnect = InputManager.gamePadConnect;
    }
    private void Update()
    {
        if (newGamePadConnect != InputManager.gamePadConnect)
        {
            if (InputManager.gamePadConnect == true)
            {

                    Debug.Log("notgood");

                    Cursor.visible = false;
                    if (mainMenu.activeSelf == true)
                    {
                        eventsyst.GetComponent<UnityEngine.EventSystems.EventSystem>().SetSelectedGameObject(play);

                    }
                    else if (optionMenu.activeSelf == true)
                    {
                        eventsyst.GetComponent<UnityEngine.EventSystems.EventSystem>().SetSelectedGameObject(master);

                    }
                    else if (Credit.activeSelf == true)
                    {
                        eventsyst.GetComponent<UnityEngine.EventSystems.EventSystem>().SetSelectedGameObject(Retourbtn);
                    }
                //currentTime = 1; 
                newGamePadConnect = InputManager.gamePadConnect;
            }
            else
            {
                    Debug.Log("isgood");
                    Cursor.visible = true;
                    eventsyst.GetComponent<UnityEngine.EventSystems.EventSystem>().SetSelectedGameObject(null);
                    newGamePadConnect = InputManager.gamePadConnect;
                    //currentTime = 0;   
            }
        }

    }

    public void SetHeadBobbing(){
        if(GameObject.Find("ToggleHeadBoding").GetComponent<Toggle>().isOn){
            headBobbing = true;
        }
        else{
            headBobbing = false;
        }
        Debug.Log(headBobbing);
    }

    public void SetSliderSensitivity(){
        sensitivityCam = GameObject.Find("sensiSlider").GetComponent<Slider>().value;
    }

    public void SetSoundMaster(Slider slider){
        soundMaster = slider.value;
    }

    public void SetSoundMusic(Slider slider){
        soundMusic = slider.value;
    }

    public void SetSoundNarrator(Slider slider){
        soundNarrator = slider.value;
    }

    public void SetSoundEffect(Slider slider){
        soundEffect = slider.value;
    }
    public void SetResolution (int resolutionIndex)
    {
        Resolution resolution = resolutions[resolutionIndex];
        Screen.SetResolution(resolution.width, resolution.height, Screen.fullScreen);
    }
    public void PlayButton()
    {
        menuPlaySound.Play();
        musicManager.FirstFloorMusic();
        SceneManager.LoadScene(sceneName);
    }
    public void OptionButton()
    {
        menuClickSound.Play();
        optionMenu.SetActive(true);
        Credit.SetActive(false);
        optionAnimation.Play("A_transitionOption");

        if (InputManager.gamePadConnect == true)
        {
            eventsyst.GetComponent<UnityEngine.EventSystems.EventSystem>().SetSelectedGameObject(master);
            Cursor.visible = false;
            Debug.Log("true");
        }
        else
        {
            Debug.Log("false");
            Cursor.visible = true;
            eventsyst.GetComponent<UnityEngine.EventSystems.EventSystem>().SetSelectedGameObject(null);
        }

    }
    public void CreditButton()
    {
        menuClickSound.Play();
        optionMenu.SetActive(false);
        Credit.SetActive(true);
        optionAnimation.Play("A_transitionOption");

        if (InputManager.gamePadConnect == true)
        {
            eventsyst.GetComponent<UnityEngine.EventSystems.EventSystem>().SetSelectedGameObject(Retourbtn);
            Cursor.visible = false;
        }
        else
        {
            Cursor.visible = true;
            eventsyst.GetComponent<UnityEngine.EventSystems.EventSystem>().SetSelectedGameObject(null);
        }
    }
    public void voidBackButton()
    {
        menuBackSound.Play();
        optionMenu.SetActive(false);
        Credit.SetActive(false);
        optionAnimation.Play("A_transitionBack");
        mainMenu.SetActive(true);
    }
    public void QuiButton()
    {
        menuBackSound.Play();
        Application.Quit();
        Debug.Log("quit");
    }
    public void SetFullScreen(bool isFullScreen)
    {
        Screen.fullScreen = isFullScreen;
    }
    void DetectGamepad()
    {
        if (InputManager.gamePadConnect == true)
        {
            eventsyst.GetComponent<UnityEngine.EventSystems.EventSystem>().SetSelectedGameObject(play);
            Cursor.visible = false;
            Debug.Log("true");
        }
        else
        {
            Debug.Log("false");
            Cursor.visible = true;
            eventsyst.GetComponent<UnityEngine.EventSystems.EventSystem>().SetSelectedGameObject(null);
        }
    }

    public void SoundHover()
    {
        menuHoverSound.Play();
    }
}
