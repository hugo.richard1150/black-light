﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CharacterController_1Person;

public class PutFlashLightDownLastVersion : MonoBehaviour
{
    public bool isOn;
    public Open unlockEvent;
    public Transform interactPoint;
    public GameObject flashLight;
    public GameObject support;


    private void Update(){
        gameObject.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, support.transform.position.z);
    
        isOn = interactPoint.childCount > 0;

        bool condition1 = support.transform.position.z > -5.7f && support.transform.position.z < -5.5f;

        if(isOn)
        {
            if(condition1)
            {
                unlockEvent.condition = true;
            }
            else
            {
                unlockEvent.condition = false;
            }
        }
        else
        {
            unlockEvent.condition = false;
        }
    }

    private void OnTriggerEnter(Collider other){
        if(other.CompareTag("Flashlights")){
        flashLight = other.gameObject;
        flashLight.transform.parent = interactPoint;
        flashLight.GetComponent<Rigidbody>().isKinematic = true;
        flashLight.transform.position = interactPoint.position;
        flashLight.transform.rotation = interactPoint.rotation;
        }
    }
}
