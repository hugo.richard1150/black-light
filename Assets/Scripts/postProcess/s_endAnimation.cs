﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CharacterController_1Person;

public class s_endAnimation : MonoBehaviour
{
    public GameObject player;
    public Camera mainCam;
    public Camera endingCam;
    public GameObject pointer;


    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            pointer.SetActive(false);
            player.SetActive(false);
            mainCam.gameObject.SetActive(false);
            endingCam.gameObject.SetActive(true);
        }
    }
}
