﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CharacterController_1Person
{
    public class Sound : Interactable
    {
        SaveSystem saveSystem;
        public bool trigger;
        public bool startCount;
        public float targetTime;
        public int countingInteract;
        public ActDialog actDialog;
        [Header("References")]
        private FMOD.Studio.EventInstance instance;

        [FMODUnity.EventRef]
        public string fmodEvent;

        private void Awake()
        {
            //Reset of some variable for each start of game
            //saveSystem = GameObject.Find("SaveMaster").GetComponent<SaveSystem>();
            actDialog = GetComponent<ActDialog>();
            actDialog.dialogData.index = 0;
        }

        public void Update()
        {
            //Instance for play the sound (FMOD)

            //Condition for use the bell a multiple time in no time
            if (startCount)
            {
                if(targetTime > 0)
                {
                    targetTime -= Time.deltaTime;

                    if(countingInteract == 6)
                    {
                        trigger = true;
                    }   

                    if(targetTime <= 0)
                    {
                        targetTime = 10;
                        startCount = false;
                        countingInteract = 0;
                        actDialog.dialogData.index = 0;
                    }
                }
            }

            //Start of the fonction Quit condition
            if (trigger)
            {
                Quit();
            }
        }

        //Fonction when the player is actually interacting with the target object
        public override void OnInteract()
        {
            base.OnInteract();
            if (actDialog != null  && !actDialog.dialogPanel.activeSelf)
            {
                actDialog.StartDialog();
                startCount = true;
                countingInteract += 1;
            }
            PlaySound();
        }

        //Fonction who play the bell sound when the player interact
        public void PlaySound()
        {
            if(GameObject.Find("CharacterController (1Person)").GetComponent<InputManager>().GetActionShort())
            {
                instance = FMODUnity.RuntimeManager.CreateInstance(fmodEvent);
                FMODUnity.RuntimeManager.AttachInstanceToGameObject(instance, GetComponent<Transform>(), GetComponent<Rigidbody>());
                instance.start();
            }
        }

        //Reset and Quitting fonction use when the boolean trigger is on
        public void Quit()
        {      
            targetTime = 10;
            startCount = false;
            countingInteract = 0;
            saveSystem.OnSave();
            Application.Quit();
            Debug.Log("Exit game ...");
            trigger = false;
        }
    }
}
