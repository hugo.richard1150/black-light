﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class S_Chest : MonoBehaviour
{
    private FMODUnity.StudioEventEmitter chestSound;
    
    void Start()
    {
        chestSound = gameObject.GetComponent<FMODUnity.StudioEventEmitter>();
    }

    public void ChestSound()
    {
        chestSound.Play();
    }
}
