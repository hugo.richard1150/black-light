﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CharacterController_1Person
{
    [CreateAssetMenu(fileName = "Dialog Data", menuName = "DialogSystem/DialogData")]
    public class DialogData : ScriptableObject
    {
        public string[] sentences;
        public string[] voiceName;
        public float[] typingSpeed;
        public float[] waitingSpeed;
        public string deblockText;
        public string needDeblock;
        public string[] NameADBusted;
        public int index;
    }
}
