﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddClimb : MonoBehaviour
{
    public GameObject target;
    public GameObject targetSecond;

    private void OnTriggerEnter(Collider other){
        if(other.CompareTag("Player")){
            target.layer = 10;
            targetSecond.layer = 10;
        }
    }
}
