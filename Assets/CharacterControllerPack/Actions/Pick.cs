﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CharacterController_1Person
{
    public class Pick : Interactable
    {
        [Header("Variables")]
        public GameObject[] doors;
        public GameObject door;
        public ActDialog actDialog;

        private FMODUnity.StudioEventEmitter keySound;

        void Awake()
        {
            actDialog = GetComponent<ActDialog>();
        }

        void Start()
        {
            keySound = gameObject.GetComponent<FMODUnity.StudioEventEmitter>();
            doors = GameObject.FindGameObjectsWithTag("Door");
        }

        public override void OnInteract()
        {
            base.OnInteract();
            ActionPick();
        }

        public void ActionPick()
        {
            if(actDialog != null && !actDialog.dialogPanel.activeSelf)
            {
                actDialog.StartDialog();
                condition = false;
                gameObject.layer = 0;
                foreach (Transform child in transform)
                {
                    child.transform.GetComponent<S_ObjectBlacklight>().enabled = false;
                    child.transform.GetComponent<MeshRenderer>().enabled = false;
                }

            }
            else
            {
                gameObject.SetActive(false);
            }

            keySound.Play();

            for (int i = 0; i < doors.Length; i++)
            {
                if (doors[i].GetComponent<Open>().id == id)
                {
                    Debug.Log("Let's go !");
                    door = doors[i];
                    doors[i].GetComponent<Open>().condition = true;
                }
            }
        } 
    }
}
