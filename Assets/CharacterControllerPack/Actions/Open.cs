﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CharacterController_1Person
{
    public class Open : Interactable
    {
        [Space]
        //references elements
        [Header("References")]
        public Animator anim;
        public ActDialog actDialog;
        private InputManager inputManager;
        [Space]
        [Header("Variables Settings")]
        //References variables
        public int doorState;
        public string Opened;
        public string EventOpened;
        public string Closed;

        private FMODUnity.StudioEventEmitter doorSound;

        private void Awake()
        {
            inputManager = GameObject.Find("CharacterController (1Person)").GetComponent<InputManager>();
            doorSound = gameObject.GetComponent<FMODUnity.StudioEventEmitter>();
        }

        public override void OnInteract()
        {
            base.OnInteract();
            ActionDoor();
        }

        void ActionDoor()
        {
            if(inputManager.interactionOneTap == true)
            {
                if (doorState == 0)
                {
                    doorSound.SetParameter("DoorState", 1);
                    DoorOpen();
                }
                else if (doorState == 1)
                {
                    doorSound.SetParameter("DoorState", 0);
                    DoorClose();
                }
            }
        }

        public void DoorClose()
        {
            if (string.IsNullOrEmpty(EventOpened))
            {
                anim.Play(Closed);
                doorSound.Play();
                doorState = 0;
            }
            else
            {
                DoorEvent();
            }
        }

        public void DoorOpen()
        {
            anim.Play(Opened);
            doorSound.Play();
            doorState = 1;

            if(!string.IsNullOrEmpty(EventOpened)){
                EventOpened = null;
            }
        }

        public void LaunchDialog()
        {
            if (actDialog != null && !actDialog.dialogPanel.activeSelf)
            {
                actDialog.StartDialog();    
            }
        }

        public void DoorEvent()
        {
            Debug.Log("l'interaction est bien présente");
            LaunchDialog();

            if (!string.IsNullOrEmpty(EventOpened))
            {
                anim.Play(EventOpened);
            }
        }
    }
}