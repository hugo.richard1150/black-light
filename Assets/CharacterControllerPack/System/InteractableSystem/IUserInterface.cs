﻿using UnityEngine.UI;
using TMPro;

public interface IUserInterface
{
    Image ProgressBar { get; }
    TextMeshProUGUI MessageText { get; }
    string ValidationMessage { get; }
    string ConfirmationMessage { get; }
}
