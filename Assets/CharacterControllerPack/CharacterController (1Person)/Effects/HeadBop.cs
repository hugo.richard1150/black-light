﻿using UnityEngine;

namespace CharacterController_1Person
{
    public class HeadBop : MonoBehaviour
    {
        private InputManager inputManager;

        private Transform headTransform;
        private Transform cameraTransform;

        public float bopFrequency = 5f;
        public float bopHorizontalAmplitude = 0.1f;
        public float bopVerticalAmplitude = 0.1f;
        [Range(0, 1)] public float headBopSmoothing = 0.1f;
        private float walkingTime;

        private Vector3 targetCameraPosition;

        private void Start()
        {
            //Récupération des valeurs
            inputManager = GetComponent<InputManager>();
            headTransform = GameObject.Find("Head").transform;
            cameraTransform = GameObject.Find("PointOfView").transform;
        }

        private void Update()
        {
            //Détecter si le joueur est en train de bouger
            if (!inputManager.isMoving)
            {
                walkingTime = 0;
            }
            else
            {
                walkingTime += Time.deltaTime;
            }

            //Fonction headbop
            targetCameraPosition = headTransform.position + CalculateHeadBopOffset(walkingTime);
            cameraTransform.position = Vector3.Lerp(cameraTransform.position, targetCameraPosition, headBopSmoothing);

            if ((cameraTransform.position - targetCameraPosition).magnitude <= 0.001)
            {
                cameraTransform.position = targetCameraPosition;
            }
        }

        private Vector3 CalculateHeadBopOffset(float t)
        {
            float horizontalOffset = 0;
            float verticalOffset = 0;
            Vector3 offset = Vector3.zero;

            if (t > 0)
            {
                horizontalOffset = Mathf.Cos(t * bopFrequency) * bopHorizontalAmplitude;
                verticalOffset = Mathf.Sin(t * bopFrequency * 2) * bopVerticalAmplitude;

                offset = headTransform.right * horizontalOffset + headTransform.up * verticalOffset;
            }

            return offset;
        }
    }
}