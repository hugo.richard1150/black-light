﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CharacterController_1Person
{
    [CreateAssetMenu(fileName = "InteractionInputData", menuName = "InteractionSystem/InputData")]
    public class InteractionInputData : ScriptableObject
    {
       private bool b_interactedClicked;
       private bool b_interactedReleased;

        public bool InteractedClicked
        {
            get => b_interactedClicked;
            set => b_interactedClicked = value;
        }

        public bool InteractedReleased
        {
            get => b_interactedReleased;
            set => b_interactedReleased = value;
        }

        public void ResetInput()
        {
            b_interactedClicked = false;
            b_interactedReleased = false;
        }
    }
}