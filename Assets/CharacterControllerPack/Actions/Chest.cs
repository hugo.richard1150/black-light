﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CharacterController_1Person;

public class Chest : Interactable
{
    public GameObject keyPen;
    public Animator anim1;
    public Animator anim2;

    private void Start()
    {
        keyPen = GameObject.Find("fbx_penKey");
    }

    public override void OnInteract()
    {
        base.OnInteract();
        StartCoroutine("OpenChest");
    }

    public void Update()
    {
        if (!keyPen.activeSelf)
        {
            condition = true;
        }
    }

    IEnumerator OpenChest()
    {
        if (!keyPen.activeSelf)
        {
            anim1.Play("Unlock");
            yield return new WaitForSeconds(3f);
            gameObject.transform.parent.gameObject.SetActive(false);
            gameObject.transform.parent.gameObject.transform.parent.gameObject.SetActive(false);
            this.gameObject.SetActive(false);
            anim2.Play("Open");
        }
    }
}
