﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CharacterController_1Person;

public class activescript : MonoBehaviour
{
    public GameObject targetCondition;

    private void Update()
    {
        ConditionConfirm(targetCondition);
    }

    private void ConditionConfirm(GameObject obj)
    {
        if (obj.GetComponent<MeshRenderer>().enabled == false)
        {
            gameObject.GetComponent<Push>().enabled = false;
        }
        else
        {
            gameObject.GetComponent<Push>().enabled = true;
        }
    }
}
