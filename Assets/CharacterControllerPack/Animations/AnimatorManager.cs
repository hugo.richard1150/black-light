﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorManager : MonoBehaviour
{
    public GameObject[] ClimbUps;

    public void Awake()
    {
        ClimbUps = GameObject.FindGameObjectsWithTag("ClimbUp");

        foreach (GameObject Ups in ClimbUps)
        {
            Ups.AddComponent<AnimTarget>();
        }
    }
}
