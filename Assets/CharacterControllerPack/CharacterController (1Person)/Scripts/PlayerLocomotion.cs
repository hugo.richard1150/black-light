﻿using System.Collections;
using UnityEngine;

namespace CharacterController_1Person
{
    [RequireComponent(typeof(FMODUnity.StudioEventEmitter))]
    public class PlayerLocomotion : MonoBehaviour
    {
        private InputManager inputManager;
        private CharacterController controller;
        private Climb climb;
        private Transform camObject;

        private float gravityValue = -19.62f;
        private float jumpHeight = 0.3f;
        [SerializeField, Range(1, 10)] private float movementSpeed;

        private Vector3 velocity;

        public bool grounded;

        [Header("Audio")]
        private bool soundIsPlaying = false;
        private bool climbingSound = false;
        public bool isJumping = false;
        private bool isClimbing = false;
        public bool landed = false;
        public FMODUnity.StudioEventEmitter stepsSound;
        public FMODUnity.StudioEventEmitter fallSound;
        public FMODUnity.StudioEventEmitter climbSound;
        public FMODUnity.StudioEventEmitter flashlightSound;

        private void Awake()
        {
            //Récupération des valeurs
            inputManager = GetComponent<InputManager>();
            controller = GetComponent<CharacterController>();
            climb = GetComponent<Climb>();
            camObject = Camera.main.transform;
        }

        private void Update()
        {
            //Bool de vérification pour savoir si le joueur est au sol
            grounded = controller.isGrounded;

            if (isJumping)
            {
                if (landed)
                {
                    fallSound.Play();
                    isJumping = false;
                    landed = false;
                }
            }
            
            if (climb.climbPhase == 1)
            {
                isClimbing = false;
                climbingSound = false;
            }
                

            if (isClimbing)
            {
                if (!climbingSound)
                {
                    stepsSound.Stop();
                    climbSound.Play();
                    climbingSound = true;
                }
            }
            else if (inputManager.isMoving)
            {
                if (grounded)
                {
                    if (!soundIsPlaying && !isClimbing)
                    {
                        stepsSound.Play();
                        soundIsPlaying = true;
                    }
                }
                else
                {
                    stepsSound.Stop();
                    soundIsPlaying = false;
                }
            }
                
        }

        public void HandleAllMovements()
        {
            //Fonction qui contient des fonctions private (movement, rotation, ...) mais qui est public pour être réutiliser dans d'autre scripts
            HandleMovement();
            HandleRotation();
            HandleVelocity();
        }
        public void HandleAllActions()
        {
            //Fonction qui contient des fonctions private (jump, climb, ...) mais qui est public pour être réutiliser dans d'autre scripts
            HandleJump();
            HandleClimb();
        }
        private void HandleMovement()
        {
            //Si la camera n'est pas (null) alors le joueur peut éxecuter la fonction de mouvement correctement
            if (camObject != null)
            {
                //Collect de la fonction (GetMovement) qui retourne un Vector2 et attribution de celle-ci à movement qui est temporaire le temps de l'execution de la fonction
                Vector2 movement = inputManager.GetMovement();
                Vector3 move = new Vector3(movement.x, 0f, movement.y);
                move = transform.forward * move.z + transform.right * move.x;
                move.y = 0;
                controller.Move(move * Time.deltaTime * movementSpeed);

                //Vérification (si le joueur bouge) alors isMoving passe à true 
                if(move != Vector3.zero)
                {
                    inputManager.isMoving = true;
                }
                else
                {
                    inputManager.isMoving = false;
                    stepsSound.Stop();
                    soundIsPlaying = false;
                }
            }
        }
        private void HandleRotation()
        {
            //Si la camera n'est pas (null) alors le joueur peut éxecuter la fonction de rotation correctement
            if (camObject != null)
            {
                transform.forward = new Vector3(camObject.forward.x, 0f, camObject.forward.z);
            }
        }
        private void HandleVelocity()
        {
            //Gestion de la gravité
            if (grounded && velocity.y < 0)
            {
                velocity.y = 0f;
            }

            velocity.y += gravityValue * Time.deltaTime;
            controller.Move(velocity * Time.deltaTime);
        }
        private void HandleJump()
        {
            //Si le joueur est au sol il peut executer la fonction de saut
            if (inputManager.GetJump() && grounded)
            {
                isJumping = true;
                velocity.y += Mathf.Sqrt(jumpHeight * -3.0f * gravityValue);
            }
        }
        private void HandleClimb()
        {
            //Si le joueur peut grimper et que la condition d'escalade est égale à 1, lance la fonction d'escalade
            if (inputManager.GetJump() && climb.climb && climb.climbPhase == 1)
            {
                StartCoroutine(Climb());
                climb.climbPhase = 0;
                isClimbing = true;
            }
        }
        public IEnumerator Climb()
        {
            //Stockage des variables (headBop et animName)
            string animName = GetComponent<Climb>().currentHitObject.GetComponent<AnimTarget>().animName;
            HeadBop headBopScript = GameObject.Find("CharacterController (1Person)").GetComponent<HeadBop>();
            Camera mainCam = GameObject.FindWithTag("MainCamera").GetComponent<Camera>();
            InteractionManager interactionManager = GetComponent<InteractionManager>();

            //Debut de la fonction d'escalade (mode : cinématique)
            headBopScript.enabled = false;
            CameraEnable(true);
            GameObject.Find("Main Camera").GetComponent<CameraController>().enabled = false;
            GameObject.Find("CameraCine").GetComponent<Animator>().Play(animName);
            controller.enabled = false;
            yield return new WaitForSeconds(2.45f);
            this.gameObject.transform.position = new Vector3(GameObject.Find("CameraCine").GetComponent<Transform>().position.x, GameObject.Find("CameraCine").GetComponent<Transform>().position.y - 0.8f, GameObject.Find("CameraCine").GetComponent<Transform>().position.z);
            CameraEnable(false);
            controller.enabled = true;
            GameObject.Find("Main Camera").GetComponent<CameraController>().enabled = true;
            climb.climbPhase = 1;
            if(mainCam.enabled && interactionManager._interactable != null){
                interactionManager.interactPanel.DisplayInteractAnim(interactionManager.animCross, "CrossInverse", 0);
                interactionManager.interactPanel.SetTooltip(interactionManager._interactable.messageText, "");
            }
            headBopScript.enabled = true;
            //Fin de la fonction d'escalade
        }

        //Fonction qui récupère les deux Caméra (la main et la cine) et qui les désactive/réactive par rapport au bool de celle-ci
        private void CameraEnable(bool protection)
        {
            Camera mainCamera = GameObject.Find("Main Camera").GetComponent<Camera>();
            Camera cameraCine = GameObject.Find("CameraCine").GetComponent<Camera>();

            if (protection)
            {
                mainCamera.enabled = false;
                cameraCine.enabled = true;
            }
            else
            {
                mainCamera.enabled = true;
                cameraCine.enabled = false;
            }
        }
    }
}
