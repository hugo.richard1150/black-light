﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CharacterController_1Person
{
    public class Push : Interactable
    {
        [Space]
        [Header("References")]
        private InputManager inputManager;
        private PlayerLocomotion playerLocomotion;
        private GameObject player;
        private BoxCollider collCheck;
        [SerializeField] private Controls controls;
        [SerializeField] private LayerMask interactLayer;

        private GameObject currentHitObject;
        private Vector3 boxExtensionFrontBack;
        private Vector3 boxExtensionRightLeft;
        private float currentDistanceFront, currentDistanceBack, currentDistanceRight, currentDistanceLeft;
        private float DistanceFrontBack;
        private float DistanceRightLeft;
        private float rFactor;
        private bool front, back, right, left;
        [Space]
        [Header("Variables Settings")]
        public bool activationRightLeft;
        public bool activationFrontBack;
        [HideInInspector] public bool move;
        public Transform[] crans;
        public float[] cransVar;
        public int actuelCran;
        public bool canUp = true;
        public float displacementSpeed;
        public bool moveSit;
        public bool verif = false;

        private FMODUnity.StudioEventEmitter pushSound;

        #region SetInputs
        private void Awake()
        {
            pushSound = gameObject.GetComponent<FMODUnity.StudioEventEmitter>();

            controls = new Controls();
            inputManager = GameObject.FindGameObjectWithTag("Player").GetComponent<InputManager>();
            playerLocomotion = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerLocomotion>();
            player = GameObject.FindGameObjectWithTag("Player");
            collCheck = transform.GetChild(0).GetComponent<BoxCollider>();

            cransVar = new float[crans.Length];

            for (int i = 0; i < cransVar.Length; i++)
            {
                cransVar[i] = crans[i].transform.position.z;
            }
        }

        private void OnEnable()
        {
            controls.Enable();
        }

        private void OnDisable()
        {
            controls.Disable();
        }

        public float GetHorizontalMovement()
        {
            return controls.Interaction.PushHorizontal.ReadValue<float>();
        }

        public float GetVerticalMovement()
        {
            return controls.Interaction.PushVertical.ReadValue<float>();
        }
        #endregion

        private void Update()
        {
            SetupAll();
            Detection();

            if (move)
            {
                playerLocomotion.stepsSound.Stop();
                pushSound.Play();
            }
            else
            {
                pushSound.Stop();
            }
        }

        public void PushWithTiming()
        {
            for (int i = 0; i < cransVar.Length; i++)
            {
                if (transform.position.z < cransVar[i] + 0.001f && transform.position.z > cransVar[i] - 0.001f)
                {
                    Debug.Log("Correct : " + crans[i].name);

                    if (verif)
                    {
                        StartCoroutine("Change");
                    }
                }
            }
        }

        IEnumerator Change()
        {
            displacementSpeed = 0f;
            yield return new WaitForSeconds(0.5f);
            Debug.Log("Can push !");
            displacementSpeed = 0.08f;
            verif = false;
            yield return new WaitForSeconds(0.8f);
        }

        private void FixedUpdate()
        {
            OnInteract();
        }

        public override void OnInteract()
        {
            CheckCondition();
        }

        private void SetupAll()
        {
            DistanceFrontBack = transform.localScale.z + 0.5f;
            DistanceRightLeft = transform.localScale.x + 0.5f;

            if (transform.localScale.z < 1.5f || transform.localScale.x < 1.5f)
            {
                rFactor = 0.75f;
            }
            else
            {
                rFactor = 1.5f;
            }

            boxExtensionFrontBack = new Vector3(transform.localScale.x - rFactor, transform.localScale.y, transform.localScale.z / 8);
            boxExtensionRightLeft = new Vector3(transform.localScale.x / 8, transform.localScale.y, transform.localScale.z - rFactor);
        }

        private void Detection()
        {
            if (activationFrontBack)
            {
                Debug.DrawRay(transform.position, Vector3.forward * currentDistanceFront, Color.red);
                RaycastHit boxHitFront;
                if (Physics.BoxCast(transform.position, boxExtensionFrontBack, Vector3.forward, out boxHitFront, Quaternion.identity, DistanceFrontBack, interactLayer, QueryTriggerInteraction.UseGlobal))
                {
                    currentHitObject = boxHitFront.transform.gameObject;
                    currentDistanceFront = boxHitFront.distance;
                    front = true;
                }
                else
                {
                    currentDistanceFront = DistanceFrontBack;
                    currentHitObject = null;
                    front = false;
                }

                Debug.DrawRay(transform.position, -Vector3.forward * currentDistanceBack, Color.red);
                RaycastHit boxHitBack;
                if (Physics.BoxCast(transform.position, boxExtensionFrontBack, -Vector3.forward, out boxHitBack, Quaternion.identity, DistanceFrontBack, interactLayer, QueryTriggerInteraction.UseGlobal))
                {
                    currentHitObject = boxHitBack.transform.gameObject;
                    currentDistanceBack = boxHitBack.distance;
                    back = true;
                }
                else
                {
                    currentDistanceBack = DistanceFrontBack;
                    back = false;
                }
            }
            if (activationRightLeft)
            {
                Debug.DrawRay(transform.position, Vector3.right * currentDistanceRight, Color.red);
                RaycastHit boxHitRight;
                if (Physics.BoxCast(transform.position, boxExtensionRightLeft, Vector3.right, out boxHitRight, Quaternion.identity, DistanceRightLeft, interactLayer, QueryTriggerInteraction.UseGlobal))
                {
                    currentHitObject = boxHitRight.transform.gameObject;
                    currentDistanceRight = boxHitRight.distance;
                    right = true;
                }
                else
                {
                    currentDistanceRight = DistanceRightLeft;
                    right = false;
                }

                Debug.DrawRay(transform.position, -Vector3.right * currentDistanceLeft, Color.red);
                RaycastHit boxHitLeft;
                if (Physics.BoxCast(transform.position, boxExtensionRightLeft, -Vector3.right, out boxHitLeft, Quaternion.identity, DistanceRightLeft, interactLayer, QueryTriggerInteraction.UseGlobal))
                {
                    currentHitObject = boxHitLeft.transform.gameObject;
                    currentDistanceLeft = boxHitLeft.distance;
                    left = true;
                }
                else
                {
                    currentDistanceLeft = DistanceRightLeft;
                    left = false;
                }
            }
        }

        private void CheckCondition()
        {
            #region Condition
            bool firstCondition = currentDistanceFront > (DistanceFrontBack / 4) && currentDistanceFront < (DistanceFrontBack / 3);
            bool secondCondition = currentDistanceBack > (DistanceFrontBack / 4) && currentDistanceBack < (DistanceFrontBack / 3);
            bool thirdCondition = currentDistanceRight > (DistanceRightLeft / 4) && currentDistanceRight < (DistanceRightLeft / 3);
            bool fourthCondition = currentDistanceLeft > (DistanceRightLeft / 4) && currentDistanceLeft < (DistanceRightLeft / 3);
            #endregion

            if (firstCondition || secondCondition || thirdCondition || fourthCondition)
            {
                if (inputManager.PushFlag && playerLocomotion.grounded)
                {
                    inputManager.isCarrying = false;
                    player.transform.parent = this.transform;
                    player.GetComponent<PlayerManager>().enabled = false;
                    move = true;
                    //PushWithTiming();
                    //verif = true;
                }
                else
                {
                    player.transform.parent = null;
                    player.GetComponent<PlayerManager>().enabled = true;
                    move = false;
                    verif = false;
                }
            }

            if (move)
            {
                gameObject.layer = 0;
                GameObject.Find("Flashlight (1)").layer = 0;
                PushWithTiming();
                verif = true;

                if (right)
                {
                    Displacement(right, collCheck, new Vector3(0, 0, 0.5f), -GetHorizontalMovement());
                }

                if (left)
                {
                    Displacement(left, collCheck, new Vector3(0, 0, -0.5f), GetHorizontalMovement());
                }

                if (front)
                {
                    Displacement(front, collCheck, new Vector3(0, 0, -0.5f), GetVerticalMovement());
                }

                if (back)
                {
                    Displacement(back, collCheck, new Vector3(0, 0, 0.5f), -GetVerticalMovement());
                }
            }
            else{
                gameObject.layer = 12;
                GameObject.Find("Flashlight (1)").layer = 12;
                verif = false;
                StopCoroutine("Change");
            }
        }

        void Displacement(bool direction, BoxCollider collCheck, Vector3 collSwap, float getInput)
        {
            collCheck.center = collSwap;
            float moving = getInput;

            if (direction == right)
            {
                transform.Translate(moving * displacementSpeed * Time.deltaTime, 0, 0);
            }

            if (direction == left)
            {
                transform.Translate(moving * displacementSpeed * Time.deltaTime, 0, 0);
            }

            if (direction == front)
            {
                transform.Translate(0, 0, moving * displacementSpeed * Time.deltaTime);
            }

            if (direction == back)
            {
                transform.Translate(0, 0, moving * displacementSpeed * Time.deltaTime);
            }
        }

        private void OnDrawGizmos()
        {
            if (activationFrontBack)
            {
                Gizmos.color = Color.red;
                Gizmos.DrawCube(transform.position + Vector3.forward * currentDistanceFront, boxExtensionFrontBack);

                Gizmos.color = Color.red;
                Gizmos.DrawCube(transform.position + -Vector3.forward * currentDistanceBack, boxExtensionFrontBack);
            }
            if (activationRightLeft)
            {
                Gizmos.color = Color.red;
                Gizmos.DrawCube(transform.position + Vector3.right * currentDistanceRight, boxExtensionRightLeft);

                Gizmos.color = Color.red;
                Gizmos.DrawCube(transform.position + -Vector3.right * currentDistanceLeft, boxExtensionRightLeft);
            }
        }
    }
}
