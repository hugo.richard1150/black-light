﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class followSupport : MonoBehaviour
{
    public GameObject support;
    public PutFlashLightDownLastVersion downLastVersion;

    private void Update(){
        gameObject.transform.position = new Vector3(transform.position.x, transform.position.y, support.transform.position.z);

        if(downLastVersion.isOn){
            gameObject.GetComponent<BoxCollider>().enabled = false;
        }
        else{
            gameObject.GetComponent<BoxCollider>().enabled = true;
        }
    }
}
