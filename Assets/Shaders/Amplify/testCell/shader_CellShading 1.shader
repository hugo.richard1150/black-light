// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "shader_CelShading"
{
	Properties
	{
		_Tint("Tint", Color) = (0.4339623,0.4339623,0.4339623,0)
		_Distance("Distance", Float) = 4
		_ToonRamp("ToonRamp", 2D) = "white" {}
		_TextureSample0("Texture Sample 0", 2D) = "white" {}
		_Normal("Normal", 2D) = "bump" {}
		_PoweEmession("Powe Emession", Range( 0 , 10)) = 0
		[Toggle(_NEONMATERIAL_ON)] _NeonMaterial("NeonMaterial", Float) = 1
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" "IsEmissive" = "true"  }
		Cull Back
		CGINCLUDE
		#include "UnityPBSLighting.cginc"
		#include "UnityShaderVariables.cginc"
		#include "UnityCG.cginc"
		#include "Lighting.cginc"
		#pragma target 3.0
		#pragma shader_feature_local _NEONMATERIAL_ON
		#ifdef UNITY_PASS_SHADOWCASTER
			#undef INTERNAL_DATA
			#undef WorldReflectionVector
			#undef WorldNormalVector
			#define INTERNAL_DATA half3 internalSurfaceTtoW0; half3 internalSurfaceTtoW1; half3 internalSurfaceTtoW2;
			#define WorldReflectionVector(data,normal) reflect (data.worldRefl, half3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal)))
			#define WorldNormalVector(data,normal) half3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal))
		#endif
		struct Input
		{
			float2 uv_texcoord;
			float3 worldNormal;
			INTERNAL_DATA
			float3 worldPos;
		};

		struct SurfaceOutputCustomLightingCustom
		{
			half3 Albedo;
			half3 Normal;
			half3 Emission;
			half Metallic;
			half Smoothness;
			half Occlusion;
			half Alpha;
			Input SurfInput;
			UnityGIInput GIData;
		};

		uniform sampler2D _TextureSample0;
		uniform float4 _TextureSample0_ST;
		uniform float _PoweEmession;
		uniform float4 _Tint;
		uniform sampler2D _Normal;
		uniform float4 _Normal_ST;
		uniform sampler2D _ToonRamp;
		uniform float4 EntrancePositionsArray[30];
		uniform float _Distance;


		float LoopArrayPositions218( float3 WorldPos, float3 ObjectPos )
		{
			float closest=10000;
			float now=0;
			for(int i=0; i<EntrancePositionsArray.Length;i++){
				now = distance(WorldPos,EntrancePositionsArray[i]);
				if(now < closest){
				closest = now;
				}
			}
			return closest;
		}


		inline half4 LightingStandardCustomLighting( inout SurfaceOutputCustomLightingCustom s, half3 viewDir, UnityGI gi )
		{
			UnityGIInput data = s.GIData;
			Input i = s.SurfInput;
			half4 c = 0;
			#ifdef UNITY_PASS_FORWARDBASE
			float ase_lightAtten = data.atten;
			if( _LightColor0.a == 0)
			ase_lightAtten = 0;
			#else
			float3 ase_lightAttenRGB = gi.light.color / ( ( _LightColor0.rgb ) + 0.000001 );
			float ase_lightAtten = max( max( ase_lightAttenRGB.r, ase_lightAttenRGB.g ), ase_lightAttenRGB.b );
			#endif
			#if defined(HANDLE_SHADOWS_BLENDING_IN_GI)
			half bakedAtten = UnitySampleBakedOcclusion(data.lightmapUV.xy, data.worldPos);
			float zDist = dot(_WorldSpaceCameraPos - data.worldPos, UNITY_MATRIX_V[2].xyz);
			float fadeDist = UnityComputeShadowFadeDistance(data.worldPos, zDist);
			ase_lightAtten = UnityMixRealtimeAndBakedShadows(data.atten, bakedAtten, UnityComputeShadowFade(fadeDist));
			#endif
			#if defined(LIGHTMAP_ON) && ( UNITY_VERSION < 560 || ( defined(LIGHTMAP_SHADOW_MIXING) && !defined(SHADOWS_SHADOWMASK) && defined(SHADOWS_SCREEN) ) )//aselc
			float4 ase_lightColor = 0;
			#else //aselc
			float4 ase_lightColor = _LightColor0;
			#endif //aselc
			float4 normalizeResult342 = normalize( ase_lightColor );
			float4 color333 = IsGammaSpace() ? float4(0.7843137,0,1,0) : float4(0.5775805,0,1,0);
			float4 normalizeResult341 = normalize( color333 );
			float dotResult350 = dot( normalizeResult342 , normalizeResult341 );
			float LightsStates319 = dotResult350;
			float2 uv_TextureSample0 = i.uv_texcoord * _TextureSample0_ST.xy + _TextureSample0_ST.zw;
			float4 tex2DNode143 = tex2D( _TextureSample0, uv_TextureSample0 );
			float4 Albedo146 = ( _Tint * tex2DNode143 );
			float4 color505 = IsGammaSpace() ? float4(0.8227772,0.5295479,0.9433962,0) : float4(0.6431562,0.2424167,0.8760344,0);
			float4 color504 = IsGammaSpace() ? float4(0.4962019,0,0.7075472,0) : float4(0.2105425,0,0.4588115,0);
			float4 blendOpSrc506 = color505;
			float4 blendOpDest506 = color504;
			float4 temp_output_506_0 = ( saturate(  (( blendOpSrc506 > 0.5 ) ? ( 1.0 - ( 1.0 - 2.0 * ( blendOpSrc506 - 0.5 ) ) * ( 1.0 - blendOpDest506 ) ) : ( 2.0 * blendOpSrc506 * blendOpDest506 ) ) ));
			float4 color507 = IsGammaSpace() ? float4(0.8745098,0.6313726,0.9607843,0) : float4(0.7379106,0.3564003,0.9130987,0);
			float4 blendOpSrc508 = temp_output_506_0;
			float4 blendOpDest508 = color507;
			float2 uv_Normal = i.uv_texcoord * _Normal_ST.xy + _Normal_ST.zw;
			float3 normalMap209 = UnpackNormal( tex2D( _Normal, uv_Normal ) );
			UnityGI gi533 = gi;
			float3 diffNorm533 = WorldNormalVector( i , normalMap209 );
			gi533 = UnityGI_Base( data, 1, diffNorm533 );
			float3 indirectDiffuse533 = gi533.indirect.diffuse + diffNorm533 * 0.0001;
			float3 temp_output_535_0 = ( ( indirectDiffuse533 * ase_lightColor.rgb ) + pow( ase_lightAtten , 0.1 ) );
			float4 color539 = IsGammaSpace() ? float4(1,1,1,0) : float4(1,1,1,0);
			#ifdef _NEONMATERIAL_ON
				float4 staticSwitch548 = Albedo146;
			#else
				float4 staticSwitch548 = ( ( Albedo146 * ( temp_output_506_0 * ( saturate( 2.0f*blendOpDest508*blendOpSrc508 + blendOpDest508*blendOpDest508*(1.0f - 2.0f*blendOpSrc508) )) ) ) * ( float4( temp_output_535_0 , 0.0 ) + ( float4( pow( ( temp_output_535_0 * ase_lightAtten ) , 4.0 ) , 0.0 ) * ( color539 * 2.6 ) ) ) );
			#endif
			float4 Blacklighting512 = staticSwitch548;
			float3 ase_worldPos = i.worldPos;
			#if defined(LIGHTMAP_ON) && UNITY_VERSION < 560 //aseld
			float3 ase_worldlightDir = 0;
			#else //aseld
			float3 ase_worldlightDir = normalize( UnityWorldSpaceLightDir( ase_worldPos ) );
			#endif //aseld
			float dotResult128 = dot( (WorldNormalVector( i , normalMap209 )) , ase_worldlightDir );
			float Normal_lightdir134 = dotResult128;
			float2 temp_cast_3 = ((Normal_lightdir134*0.0 + 0.0)).xx;
			float4 shadow138 = ( Albedo146 * tex2D( _ToonRamp, temp_cast_3 ) );
			UnityGI gi428 = gi;
			float3 diffNorm428 = WorldNormalVector( i , normalMap209 );
			gi428 = UnityGI_Base( data, 1, diffNorm428 );
			float3 indirectDiffuse428 = gi428.indirect.diffuse + diffNorm428 * 0.0001;
			float3 temp_output_490_0 = ( pow( ase_lightAtten , 0.1 ) + ( indirectDiffuse428 * ase_lightColor.rgb ) );
			float4 lighting152 = ( shadow138 * float4( ( temp_output_490_0 + float3( 0,0,0 ) ) , 0.0 ) );
			float3 WorldPos218 = ase_worldPos;
			float3 ObjectPos218 = EntrancePositionsArray[0].xyz;
			float localLoopArrayPositions218 = LoopArrayPositions218( WorldPos218 , ObjectPos218 );
			float clampResult561 = clamp( pow( ( localLoopArrayPositions218 / _Distance ) , ase_lightAtten ) , 0.0 , 1.0 );
			float PositionsLoop562 = clampResult561;
			float4 lerpResult275 = lerp( ( LightsStates319 == 1.0 ? Blacklighting512 : lighting152 ) , lighting152 , PositionsLoop562);
			c.rgb = lerpResult275.rgb;
			c.a = 1;
			return c;
		}

		inline void LightingStandardCustomLighting_GI( inout SurfaceOutputCustomLightingCustom s, UnityGIInput data, inout UnityGI gi )
		{
			s.GIData = data;
		}

		void surf( Input i , inout SurfaceOutputCustomLightingCustom o )
		{
			o.SurfInput = i;
			o.Normal = float3(0,0,1);
			float2 uv_TextureSample0 = i.uv_texcoord * _TextureSample0_ST.xy + _TextureSample0_ST.zw;
			float4 tex2DNode143 = tex2D( _TextureSample0, uv_TextureSample0 );
			#ifdef _NEONMATERIAL_ON
				float4 staticSwitch550 = ( tex2DNode143 * _PoweEmession );
			#else
				float4 staticSwitch550 = float4( 0,0,0,0 );
			#endif
			o.Emission = staticSwitch550.rgb;
		}

		ENDCG
		CGPROGRAM
		#pragma surface surf StandardCustomLighting keepalpha fullforwardshadows 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			#include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float2 customPack1 : TEXCOORD1;
				float4 tSpace0 : TEXCOORD2;
				float4 tSpace1 : TEXCOORD3;
				float4 tSpace2 : TEXCOORD4;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO( o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				Input customInputData;
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				half3 worldNormal = UnityObjectToWorldNormal( v.normal );
				half3 worldTangent = UnityObjectToWorldDir( v.tangent.xyz );
				half tangentSign = v.tangent.w * unity_WorldTransformParams.w;
				half3 worldBinormal = cross( worldNormal, worldTangent ) * tangentSign;
				o.tSpace0 = float4( worldTangent.x, worldBinormal.x, worldNormal.x, worldPos.x );
				o.tSpace1 = float4( worldTangent.y, worldBinormal.y, worldNormal.y, worldPos.y );
				o.tSpace2 = float4( worldTangent.z, worldBinormal.z, worldNormal.z, worldPos.z );
				o.customPack1.xy = customInputData.uv_texcoord;
				o.customPack1.xy = v.texcoord;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				return o;
			}
			half4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				surfIN.uv_texcoord = IN.customPack1.xy;
				float3 worldPos = float3( IN.tSpace0.w, IN.tSpace1.w, IN.tSpace2.w );
				half3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				surfIN.worldPos = worldPos;
				surfIN.worldNormal = float3( IN.tSpace0.z, IN.tSpace1.z, IN.tSpace2.z );
				surfIN.internalSurfaceTtoW0 = IN.tSpace0.xyz;
				surfIN.internalSurfaceTtoW1 = IN.tSpace1.xyz;
				surfIN.internalSurfaceTtoW2 = IN.tSpace2.xyz;
				SurfaceOutputCustomLightingCustom o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutputCustomLightingCustom, o )
				surf( surfIN, o );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=18800
196;292;1052;836;-1920.207;-2992.849;1;True;True
Node;AmplifyShaderEditor.SamplerNode;208;-488.4385,1104.758;Inherit;True;Property;_Normal;Normal;7;0;Create;True;0;0;0;False;0;False;-1;None;None;True;0;True;bump;Auto;True;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.CommentaryNode;545;2950.932,2884.174;Inherit;False;2131.798;1292.748;Black Lighting;10;504;505;507;506;508;509;510;544;512;546;Black Lighting;1,1,1,1;0;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;209;-112.282,1120.871;Inherit;False;normalMap;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.CommentaryNode;545;2950.932,2884.174;Inherit;False;2131.798;1292.748;Black Lighting;14;560;546;506;507;509;510;528;505;508;504;548;544;512;556;Black Lighting;1,1,1,1;0;0
Node;AmplifyShaderEditor.GetLocalVarNode;210;598.0093,969.0223;Inherit;False;209;normalMap;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.CommentaryNode;546;3232.271,3457.932;Inherit;False;1244.118;682.9761;Small lumière;14;537;541;542;534;538;539;536;533;531;543;530;540;535;532;Small lumière;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;207;780.4177,909.3609;Inherit;False;735.7573;448.1411;Comment;4;134;129;127;128;N dot L;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;529;1296.118,2900.062;Inherit;False;1666.029;786.7881;Comment;17;488;490;444;516;152;151;149;517;519;526;518;527;520;428;150;426;427;Lighting;1,1,1,1;0;0
Node;AmplifyShaderEditor.WorldNormalVector;127;831.1698,959.3609;Inherit;False;False;1;0;FLOAT3;0,0,1;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.WorldSpaceLightDirHlpNode;129;830.4177,1176.902;Inherit;False;False;1;0;FLOAT;0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.GetLocalVarNode;530;3282.271,3662.783;Inherit;False;209;normalMap;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.DotProductOpNode;128;1113.336,1148.309;Inherit;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LightAttenuation;531;3386.346,3507.932;Inherit;False;0;1;FLOAT;0
Node;AmplifyShaderEditor.LightColorNode;532;3524.504,3742.143;Inherit;False;0;3;COLOR;0;FLOAT3;1;FLOAT;2
Node;AmplifyShaderEditor.IndirectDiffuseLighting;533;3470.456,3659.458;Inherit;False;Tangent;1;0;FLOAT3;0,0,1;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ColorNode;144;1666.318,1161.365;Inherit;False;Property;_Tint;Tint;0;0;Create;True;0;0;0;False;0;False;0.4339623,0.4339623,0.4339623,0;1,1,1,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;143;1600.783,1355.401;Inherit;True;Property;_TextureSample0;Texture Sample 0;3;0;Create;True;0;0;0;False;0;False;-1;None;bf963d1f25cedbc4b80776316eb8b7fe;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.PowerNode;541;3649.692,3517.434;Inherit;False;False;2;0;FLOAT;0;False;1;FLOAT;0.1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;534;3731.241,3629.606;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;134;1288.175,1165.377;Inherit;False;Normal_lightdir;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;529;1296.118,2900.062;Inherit;False;1666.029;786.7881;Comment;17;488;490;444;516;152;151;149;517;519;526;518;527;520;428;150;426;427;Lighting;1,1,1,1;0;0
Node;AmplifyShaderEditor.SimpleAddOpNode;535;3890.694,3531.434;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;145;1929.318,1287.365;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;141;1636.565,2065.548;Inherit;False;Constant;_Float0;Float 0;1;0;Create;True;0;0;0;False;0;False;0;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;504;2993.936,3217.055;Inherit;False;Constant;_Color2;Color 2;8;0;Create;True;0;0;0;False;0;False;0.4962019,0,0.7075472,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GetLocalVarNode;136;1625.441,1881.597;Inherit;False;134;Normal_lightdir;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;505;3001.414,3014.498;Inherit;False;Constant;_Color3;Color 3;8;0;Create;True;0;0;0;False;0;False;0.8227772,0.5295479,0.9433962,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GetLocalVarNode;427;1346.118,3208.725;Inherit;False;209;normalMap;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;536;3903.578,3764.294;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ScaleAndOffsetNode;140;1810.74,2032.354;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;1;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.BlendOpsNode;506;3349.146,3010.662;Inherit;False;HardLight;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;1;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;146;2154.318,1324.365;Inherit;False;Albedo;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ScaleAndOffsetNode;140;1810.74,2032.354;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;1;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;507;3299.574,3251.155;Inherit;False;Constant;_Color4;Color 4;8;0;Create;True;0;0;0;False;0;False;0.8745098,0.6313726,0.9607843,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;539;3777.175,3931.908;Inherit;False;Constant;_Color1;Color 1;8;0;Create;True;0;0;0;False;0;False;1,1,1,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;507;3299.574,3251.155;Inherit;False;Constant;_Color4;Color 4;8;0;Create;True;0;0;0;False;0;False;0.8745098,0.6313726,0.9607843,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;542;4027.513,4005.791;Inherit;False;Constant;_Float3;Float 3;8;0;Create;True;0;0;0;False;0;False;2.6;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.IndirectDiffuseLighting;428;1534.303,3205.401;Inherit;False;Tangent;1;0;FLOAT3;0,0,1;False;1;FLOAT3;0
Node;AmplifyShaderEditor.LightAttenuation;426;1450.194,3053.874;Inherit;False;0;1;FLOAT;0
Node;AmplifyShaderEditor.LightColorNode;150;1588.352,3288.086;Inherit;False;0;3;COLOR;0;FLOAT3;1;FLOAT;2
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;543;4190.514,3896.958;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;137;2043.991,1933.665;Inherit;True;Property;_ToonRamp;ToonRamp;2;0;Create;True;0;0;0;False;0;False;-1;None;43293c38a102d404e8ad405969a29a1d;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.PowerNode;537;4093.153,3763.439;Inherit;False;False;2;0;FLOAT3;0,0,0;False;1;FLOAT;4;False;1;FLOAT3;0
Node;AmplifyShaderEditor.BlendOpsNode;508;3567.061,3179.325;Inherit;True;SoftLight;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;1;False;1;COLOR;0
Node;AmplifyShaderEditor.WorldPosInputsNode;215;1911.42,4641.577;Inherit;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.GlobalArrayNode;216;1865.634,4790.345;Inherit;False;EntrancePositionsArray;0;30;2;False;False;0;1;False;Object;147;4;0;INT;0;False;2;INT;0;False;1;INT;0;False;3;INT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.GetLocalVarNode;147;2088.192,1821.558;Inherit;False;146;Albedo;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;444;1806.089,3167.548;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;510;3056.266,2932.927;Inherit;False;146;Albedo;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.CommentaryNode;559;2950.932,2884.174;Inherit;False;2131.798;1292.748;Black Lighting;2;528;548;Black Lighting;1,1,1,1;0;0
Node;AmplifyShaderEditor.PowerNode;488;1728.917,3059.851;Inherit;False;False;2;0;FLOAT;0;False;1;FLOAT;0.1;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;318;3329.307,4576.237;Inherit;False;728.8997;365.3354;Lights states;7;555;331;333;341;342;350;319;Lights States;1,1,1,1;0;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;148;2379.277,1877.722;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;540;4294.396,3785.18;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.CustomExpressionNode;218;2123.022,4693.498;Float;False;float closest=10000@$float now=0@$for(int i=0@ i<EntrancePositionsArray.Length@i++){$	now = distance(WorldPos,EntrancePositionsArray[i])@$	if(now < closest){$	closest = now@$	}$}$return closest@;1;False;2;True;WorldPos;FLOAT3;0,0,0;In;;Inherit;False;True;ObjectPos;FLOAT3;0,0,0;In;;Inherit;False;Loop Array Positions;True;False;0;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;217;2152.653,4807.039;Inherit;False;Property;_Distance;Distance;1;0;Create;True;0;0;0;False;0;False;4;4;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;219;2339.861,4696.089;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LightColorNode;331;3409.253,4647.513;Inherit;False;0;3;COLOR;0;FLOAT3;1;FLOAT;2
Node;AmplifyShaderEditor.SimpleAddOpNode;490;1954.54,3077.377;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;528;4126.523,2901.203;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;333;3345.966,4765.577;Inherit;False;Constant;_ColorBlacklightState;Color Blacklight State;8;0;Create;True;0;0;0;False;0;False;0.7843137,0,1,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RegisterLocalVarNode;138;2556.364,1884.851;Inherit;False;shadow;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;538;4323.989,3557.762;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;149;1904.369,2950.062;Inherit;False;138;shadow;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.PowerNode;413;2525.397,4695.444;Inherit;False;False;2;0;FLOAT;0;False;1;FLOAT;4000;False;1;FLOAT;0
Node;AmplifyShaderEditor.NormalizeNode;341;3569.258,4767.249;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.NormalizeNode;342;3566.613,4647.06;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;544;4381.858,2913.572;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.LightAttenuation;408;2490.148,4811.726;Inherit;False;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;517;2387.836,3103.704;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.PowerNode;416;2727.635,4693.936;Inherit;False;False;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.StaticSwitch;548;4599.31,2945.524;Inherit;False;Property;_NeonMaterial;NeonMaterial;8;0;Create;True;0;0;0;False;0;False;0;1;1;True;;Toggle;2;Key0;Key1;Create;True;True;9;1;COLOR;0,0,0,0;False;0;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;4;COLOR;0,0,0,0;False;5;COLOR;0,0,0,0;False;6;COLOR;0,0,0,0;False;7;COLOR;0,0,0,0;False;8;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.CommentaryNode;311;1844.726,4584.387;Inherit;False;1440.589;364.5928;Positions lights loop;2;561;562;Lights Positions Loop;1,1,1,1;0;0
Node;AmplifyShaderEditor.DotProductOpNode;350;3729.287,4693.523;Inherit;False;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;151;2559.752,3000.55;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT3;0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ClampOpNode;561;2900.456,4694.311;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;319;3850.625,4689.145;Float;False;LightsStates;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;512;4879.669,2906.822;Inherit;False;Blacklighting;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;152;2737.346,3003.807;Inherit;False;lighting;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;563;4015.07,1640.984;Inherit;False;319;LightsStates;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;446;4070.738,1720.618;Inherit;False;Constant;_Float1;Float 1;8;0;Create;True;0;0;0;False;0;False;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;562;3068.456,4688.311;Inherit;False;PositionsLoop;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;564;4015.111,1790.959;Inherit;False;512;Blacklighting;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;139;4036.899,1870.304;Inherit;False;152;lighting;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;553;4302.734,1570.06;Inherit;False;Property;_PoweEmession;Powe Emession;9;0;Create;True;0;0;0;False;0;False;0;0;0;10;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;572;4572.514,1864.119;Inherit;False;152;lighting;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;166;-698.3795,2766.857;Inherit;False;rim;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.Compare;449;4270.715,1666.931;Inherit;False;0;4;0;FLOAT;0;False;1;FLOAT;0;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;313;4551.449,1965.819;Inherit;False;562;PositionsLoop;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;275;4806.229,1818.551;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;167;4796.126,1955.188;Inherit;False;166;rim;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;552;4569.44,1532.353;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;160;-2476.711,2763.512;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ComponentMaskNode;582;4709.814,2353.646;Inherit;False;True;True;True;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;177;5020.736,1909.737;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SaturateNode;162;-2316.754,2763.679;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LightColorNode;575;4359.838,2238.388;Inherit;False;0;3;COLOR;0;FLOAT3;1;FLOAT;2
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;557;4569.44,1532.353;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.PowerNode;164;-1934.553,2780.58;Inherit;False;False;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.ViewDirInputsCoordNode;133;848.1334,1677.349;Inherit;False;World;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RegisterLocalVarNode;555;3850.625,4689.145;Float;False;LightsStates;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ComponentMaskNode;581;4712.69,2244.398;Inherit;False;True;True;True;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;173;-2041.006,3056.701;Inherit;False;134;Normal_lightdir;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;558;4796.126,1955.188;Inherit;False;166;rim;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;169;-1287.863,3111.946;Inherit;False;Property;_rimColor;rimColor;6;0;Create;True;0;0;0;False;0;False;0,0,0,0;0.2413668,0.3709732,0.4528298,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GetLocalVarNode;159;-2775.633,2838.001;Inherit;False;135;normal_viewdir;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;161;-2695.628,2721.813;Inherit;False;Property;_rimOffset;rimOffset;4;0;Create;True;0;0;0;False;0;False;1;0.59;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.DotProductOpNode;578;4937.175,2277.21;Inherit;False;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;574;4296.551,2356.453;Inherit;False;Constant;_Color5;Color 5;8;0;Create;True;0;0;0;False;0;False;0.9960784,0.9529412,0.8352941,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.DotProductOpNode;132;1104.074,1539.429;Inherit;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;526;2254.361,3442.901;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;165;-2211.453,2887.18;Inherit;False;Property;_rimPower;rimPower;5;0;Create;True;0;0;0;False;0;False;0;0.027;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.WorldNormalVector;130;810.9969,1464.964;Inherit;False;False;1;0;FLOAT3;0,0,1;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.StaticSwitch;550;4743.563,1547.557;Inherit;False;Property;_NeonMaterial;NeonMaterial;9;0;Create;True;0;0;0;False;0;False;0;1;1;True;_NeonMaterial;Toggle;2;Key0;Key1;Create;True;True;9;1;COLOR;0,0,0,0;False;0;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;4;COLOR;0,0,0,0;False;5;COLOR;0,0,0,0;False;6;COLOR;0,0,0,0;False;7;COLOR;0,0,0,0;False;8;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;554;-1035.603,3013.02;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.NormalizeNode;577;4517.198,2237.935;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;175;-1570.254,2802.613;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;516;1997.424,3312.236;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ColorNode;520;1841.022,3477.85;Inherit;False;Constant;_Color0;Color 0;8;0;Create;True;0;0;0;False;0;False;0.8999152,0,1,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;527;2091.361,3551.734;Inherit;False;Constant;_Float2;Float 2;8;0;Create;True;0;0;0;False;0;False;2.6;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.NormalizeNode;576;4519.843,2358.125;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;170;-1035.603,3013.02;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SaturateNode;178;-1174.735,2799.599;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LightColorNode;168;-1286.605,2945.994;Inherit;False;0;3;COLOR;0;FLOAT3;1;FLOAT;2
Node;AmplifyShaderEditor.RegisterLocalVarNode;556;4879.669,2906.822;Inherit;False;Blacklighting;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;135;1300.005,1535.521;Inherit;False;normal_viewdir;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;519;2358.243,3331.122;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.OneMinusNode;163;-2150.353,2766.28;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.PowerNode;518;2157,3309.381;Inherit;False;False;2;0;FLOAT3;0,0,0;False;1;FLOAT;4;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;174;-1797.007,3112.701;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LightAttenuation;172;-2059.582,3181.035;Inherit;False;0;1;FLOAT;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;214;5248.871,1575.785;Float;False;True;-1;2;ASEMaterialInspector;0;0;CustomLighting;shader_CelShading;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Opaque;0.5;True;True;0;False;Opaque;;Geometry;All;14;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;False;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;4;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
Node;AmplifyShaderEditor.CommentaryNode;176;-2798.301,2685.479;Inherit;False;2352.054;649.1321;rimlight;0;rimlight;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;560;3232.271,3457.932;Inherit;False;1244.118;682.9761;Small lumière;0;Small lumière;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;204;1550.783,1111.365;Inherit;False;828.3352;474.0365;Albedo;0;;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;206;797.9973,1388.964;Inherit;False;730.0077;473.985;Comment;0;ViewDir;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;207;780.4177,909.3609;Inherit;False;735.7573;448.1411;Comment;0;N dot L;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;205;1575.441,1691.264;Inherit;False;1205.722;498.2904;Comment;0;Shadow;1,1,1,1;0;0
WireConnection;209;0;208;0
WireConnection;127;0;210;0
WireConnection;128;0;127;0
WireConnection;128;1;129;0
WireConnection;533;0;530;0
WireConnection;541;0;531;0
WireConnection;534;0;533;0
WireConnection;534;1;532;1
WireConnection;134;0;128;0
WireConnection;535;0;534;0
WireConnection;535;1;541;0
WireConnection;145;0;144;0
WireConnection;145;1;143;0
WireConnection;536;0;535;0
WireConnection;536;1;531;0
WireConnection;140;0;136;0
WireConnection;140;1;141;0
WireConnection;140;2;141;0
WireConnection;506;0;505;0
WireConnection;506;1;504;0
WireConnection;146;0;145;0
WireConnection;428;0;427;0
WireConnection;543;0;539;0
WireConnection;543;1;542;0
WireConnection;137;1;140;0
WireConnection;543;0;539;0
WireConnection;543;1;542;0
WireConnection;508;0;506;0
WireConnection;508;1;507;0
WireConnection;444;0;428;0
WireConnection;444;1;150;1
WireConnection;488;0;426;0
WireConnection;509;0;506;0
WireConnection;509;1;508;0
WireConnection;148;0;147;0
WireConnection;148;1;137;0
WireConnection;540;0;537;0
WireConnection;540;1;543;0
WireConnection;218;0;215;0
WireConnection;218;1;216;0
WireConnection;219;0;218;0
WireConnection;219;1;217;0
WireConnection;490;0;488;0
WireConnection;490;1;444;0
WireConnection;528;0;510;0
WireConnection;528;1;509;0
WireConnection;138;0;148;0
WireConnection;538;0;535;0
WireConnection;538;1;540;0
WireConnection;413;0;219;0
WireConnection;341;0;333;0
WireConnection;342;0;331;0
WireConnection;544;0;528;0
WireConnection;544;1;538;0
WireConnection;219;0;218;0
WireConnection;219;1;217;0
WireConnection;517;0;490;0
WireConnection;416;0;413;0
WireConnection;416;1;408;0
WireConnection;548;1;544;0
WireConnection;548;0;510;0
WireConnection;151;0;149;0
WireConnection;151;1;517;0
WireConnection;561;0;416;0
WireConnection;319;0;350;0
WireConnection;512;0;548;0
WireConnection;152;0;151;0
WireConnection;562;0;561;0
WireConnection;449;0;563;0
WireConnection;449;1;446;0
WireConnection;449;2;564;0
WireConnection;449;3;572;0
WireConnection;552;0;143;0
WireConnection;552;1;553;0
WireConnection;160;0;161;0
WireConnection;160;1;159;0
WireConnection;582;0;576;0
WireConnection;177;0;275;0
WireConnection;177;1;167;0
WireConnection;162;0;160;0
WireConnection;164;0;163;0
WireConnection;164;1;165;0
WireConnection;581;0;577;0
WireConnection;578;0;581;0
WireConnection;578;1;582;0
WireConnection;132;0;130;0
WireConnection;132;1;133;0
WireConnection;526;0;520;0
WireConnection;526;1;527;0
WireConnection;550;0;552;0
WireConnection;577;0;575;0
WireConnection;175;0;164;0
WireConnection;175;1;174;0
WireConnection;516;0;490;0
WireConnection;516;1;426;0
WireConnection;576;0;574;0
WireConnection;170;0;168;0
WireConnection;170;1;169;0
WireConnection;135;0;132;0
WireConnection;519;0;518;0
WireConnection;519;1;526;0
WireConnection;163;0;162;0
WireConnection;518;0;516;0
WireConnection;174;0;173;0
WireConnection;174;1;172;0
WireConnection;214;2;550;0
WireConnection;214;13;275;0
ASEEND*/
//CHKSM=1B9F2EC015225784150E718334F5BC08BC366173