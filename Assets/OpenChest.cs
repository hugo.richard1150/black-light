﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CharacterController_1Person;

public class OpenChest : Interactable
{
    public Animator animator;

    public override void OnInteract()
    {
        base.OnInteract();

        OpeningChest();
    }

    public void OpeningChest(){
        animator.Play("Open");
        gameObject.layer = 0;
    }
}
