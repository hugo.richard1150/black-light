﻿using UnityEngine;

namespace CharacterController_1Person
{
    public class InteractionManager : MonoBehaviour
    {
        [Space]
        [Header("Data")]
        public InteractionInputData interactionInputData;
        public InteractionData interactionData;
        public Animator animCross;
        private InteractionPanel interactionPanel;
        public InteractionPanel interactPanel => interactionPanel;
        [Space]
        [Header("Ray settings")]
        public float rayDistanceShort;
        public float raySphereRadiusShort;
        public float rayDistanceLong;
        public float raySphereRadiusLong;
        public LayerMask interactableLayer;

        public int keepTasking => keepTask;
        public float timeRemaining;
        public Interactable _interactable;
        private ActDialog _actDialog;
        public S_bustedDiag _bustedDiag;
        private float startTimeRemaining;
        private Camera b_cam;
        private bool b_interacting;
        private float b_holdTimer = 0f;
        private int keepTask;
        private bool timerCountFinish = false;
        private bool unlockTimer = false;

        private void Awake()
        {
            //Remise à zéro du compteur de focus
            startTimeRemaining = timeRemaining;
            //Récupération des variables et références (private)
            b_cam = GameObject.Find("Main Camera").GetComponent<Camera>();
            interactionPanel = GameObject.Find("InteractionPanel").transform.GetComponent<InteractionPanel>();
            animCross = GameObject.Find("FlexInterface").transform.GetChild(0).GetComponent<Animator>();
        }

        private void Update()
        {
            CheckForInteractable();
            CheckForInteractableInput();
            if (unlockTimer)
            {
                interactionPanel.UpdateProgressBar(_actDialog.progressBar, timeRemaining);
                timeRemaining += Time.deltaTime;
                b_cam.fieldOfView -= Time.deltaTime * 1.5f;
                if (timeRemaining >= 3)
                {
                    timerCountFinish = true;
                }
            }
            else
            {
                timeRemaining = startTimeRemaining;
                b_cam.fieldOfView = 60;
            }
        }

        //Fonction de détection des interactions
        void CheckForInteractable()
        {
            //Raycast pour les interactions globales
            #region Raycast interactions globales
            Ray _rayshort = new Ray(b_cam.transform.position, b_cam.transform.forward);
            RaycastHit _hitInfoShort;

            bool _hitSomethingShort = Physics.SphereCast(_rayshort, raySphereRadiusShort, out _hitInfoShort, rayDistanceShort, interactableLayer);

            if (_hitSomethingShort)
            {
                _interactable = _hitInfoShort.transform.GetComponent<Interactable>();
                _bustedDiag = _hitInfoShort.transform.GetComponent<S_bustedDiag>();

                if (_interactable != null)
                {
                    if(interactionData.IsEmpty())
                    {
                        interactionData.Interaction = _interactable;
                        interactionPanel.interactable = interactionData.Interaction;
                        interactionPanel.SetTooltip(_interactable.messageText, _interactable.validationMessage);
                        interactionPanel.DisplayInteractAnim(animCross, "Cross", 1);
                        keepTask = 1;
                        //keepTask = _interactable.task;
                    }
                    else
                    {
                        if (!interactionData.Interaction.condition)
                        {
                            interactionPanel.SetTooltip(_interactable.messageText, _interactable.confirmationMessage);
                            keepTask = _interactable.task;
                            interactionPanel.DisplayInteractAnim(animCross, "None", 0);
                        }

                        if (!interactionData.IsSameInteractable(_interactable))
                        {
                            interactionPanel.ResetUI(_interactable.progressBar, _interactable.messageText);
                            interactionData.Interaction = _interactable;
                            interactionPanel.interactable = interactionData.Interaction;
                            interactionPanel.SetTooltip(_interactable.messageText, _interactable.validationMessage);
                            interactionPanel.DisplayInteractAnim(animCross, "Cross", 1);
                            keepTask = 1;
                            //keepTask = _interactable.task;
                        }
                    }
                }
            }
            else
            {
                if (_interactable != null)
                {
                    interactionPanel.ResetUI(_interactable.progressBar, _interactable.messageText);
                    if(interactionPanel.actualAnim == 0)
                    {
                        interactionPanel.DisplayInteractAnim(animCross, "None", 0);
                    }
                    else
                    {
                        interactionPanel.DisplayInteractAnim(animCross, "CrossInverse", 3);
                    }
                }
                else
                {
                    interactionPanel.DisplayInteractAnim(animCross, "CrossInverse", 0);
                }
                _interactable = null;
                interactionData.ResetData();
                keepTask = 0;
            }
            Debug.DrawRay(_rayshort.origin, _rayshort.direction * rayDistanceShort, _hitSomethingShort ? Color.green : Color.red);
#endregion

            //Raycast pour les interactions porter sur la vision (fixe)
            #region Raycast visée
            Ray _rayLong = new Ray(b_cam.transform.position, b_cam.transform.forward);
            RaycastHit _hitInfoLong;

            bool _hitSomethingLong = Physics.SphereCast(_rayLong, raySphereRadiusLong, out _hitInfoLong, rayDistanceLong);

            if (_hitSomethingLong)
            {
                _actDialog = _hitInfoLong.transform.GetComponent<ActDialog>();

                if (_actDialog != null)
                {
                    interactionPanel.actDialog = _actDialog;

                    if (_actDialog.mustBeSeen == true)
                    {
                        #region Dialogue comparaison condition
                        bool actdialogCompare = !string.IsNullOrEmpty(_actDialog.dialogData.needDeblock) && _actDialog.dialogDataCompare.deblockText == _actDialog.dialogData.needDeblock && _actDialog.needCompare == true;
                        #endregion

                        if (actdialogCompare)
                        {
                            if (timerCountFinish)
                            {
                                unlockTimer = false;
                                b_cam.fieldOfView = 60;
                                interactionPanel.ResetUI(_actDialog.progressBar, _interactable.messageText);

                                if (!_actDialog.dialogPanel.activeSelf)
                                {
                                    _actDialog.StartDialog();
                                }
                            }
                            else
                            {
                                if (!_actDialog.dialogPanel.activeSelf)
                                {
                                    //interactionPanel.DisplayIcon(_actDialog.iconTimer);
                                    unlockTimer = true;
                                }
                            }
                        }
                        else if(_actDialog.needCompare == false)
                        {
                            if (timerCountFinish)
                            {
                                unlockTimer = false;
                                b_cam.fieldOfView = 60;
                                interactionPanel.ResetUI(_actDialog.progressBar, _interactable.messageText);

                                if (!_actDialog.dialogPanel.activeSelf)
                                {
                                    _actDialog.StartDialog();
                                }
                            }
                            else
                            {
                                if (!_actDialog.dialogPanel.activeSelf)
                                {
                                    //interactionPanel.DisplayIcon(_actDialog.iconTimer);
                                    unlockTimer = true;
                                }
                            }
                        }
                    }
                }
                else
                {
                    interactionPanel.actDialog = null;
                }
            }
            #endregion
        }

        //Fonction de détection des inputs (interactions)
        void CheckForInteractableInput()
        {
            if (interactionData.IsEmpty())
            {
                return;
            }

            if (keepTask >= 1 && interactionData.Interaction.condition)
            {
                if (interactionInputData.InteractedClicked)
                {
                    b_interacting = true;
                    //b_holdTimer = 0f;
                    if (_bustedDiag != null)
                    {
                        _bustedDiag.nameBusted();
                    }
                }
                else
                {
                    b_interacting = false;
                    b_holdTimer = 0f;
                }
            }

            if(keepTask >= 1 && !interactionData.Interaction.condition)
            {
                Debug.Log(keepTask);
                if (interactionInputData.InteractedClicked)
                {
                    interactionData.Interaction.GetComponent<Open>().DoorEvent();
                }
            }

            if (interactionInputData.InteractedReleased)
            {
                b_interacting = false;
                b_holdTimer = 0f;
                interactionPanel.UpdateProgressBar(_interactable.progressBar, 0f);
            }

            if (b_interacting)
            {
                if (!interactionData.Interaction.IsInteractable)
                {
                    return;
                }

                if (interactionData.Interaction.HoldInteract)
                {
                    b_holdTimer += Time.deltaTime;

                    float holdPercent = b_holdTimer / interactionData.Interaction.HoldDuration;
                    interactionPanel.UpdateProgressBar(_interactable.progressBar, holdPercent);

                    if (holdPercent > 1f)
                    {
                        interactionData.Interact();
                        b_interacting = false;
                    }
                }
                else
                {
                    interactionData.Interact();
                    b_holdTimer = 0f;
                }
            }
        }
    }
}
