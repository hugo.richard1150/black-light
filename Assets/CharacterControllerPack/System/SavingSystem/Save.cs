﻿using CharacterController_1Person;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Save : MonoBehaviour
{
    public Transform[] savingElements;

    private void OnEnable()
    {
        CreateSave();
    }

    void Start()
    {
        /*if(SceneManager.GetActiveScene().buildIndex == 0)
        {
            PlayerPrefs.SetInt("load", 0);
        }
        else
        {
        }*/

        if (PlayerPrefs.GetInt("load") == 1)
        {
            foreach (var element in savingElements)
            {
                for (int i = 0; i < savingElements.Length; i++)
                {
                    float x = PlayerPrefs.GetFloat("x");
                    float y = PlayerPrefs.GetFloat("y");
                    float z = PlayerPrefs.GetFloat("z");

                    element.transform.position = new Vector3(x, y, z);
                }
            }
        }
    }

    private void Update()
    {
        if (GameObject.Find("CharacterController (1Person)").GetComponent<InputManager>().GetJump())
        {
            LoadSave();
        }
    }

    public void CreateSave()
    {
        PlayerPrefs.SetString("Scene", SceneManager.GetActiveScene().name);

        foreach (var elements in savingElements)
        {
            PlayerPrefs.SetFloat("x" + elements.name, elements.transform.position.x);
            PlayerPrefs.SetFloat("y" + elements.name, elements.transform.position.y);
            PlayerPrefs.SetFloat("z" + elements.name, elements.transform.position.z);

            Debug.Log("x" + elements.name);
            Debug.Log("y" + elements.name);
            Debug.Log("z" + elements.name);
        }
    }

    public void LoadSave()
    {
        if (PlayerPrefs.HasKey("Scene"))
        {
            PlayerPrefs.SetInt("load", 1);
            SceneManager.LoadScene(PlayerPrefs.GetString("Scene"));
        }
    }
}
