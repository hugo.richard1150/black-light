﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeLayer : MonoBehaviour
{
    private void Update(){
        if(this.gameObject.GetComponent<MeshRenderer>().enabled == false){
            gameObject.layer = 4;
        }
        else{
            gameObject.layer = 0;
        }
    }
}
