﻿using CharacterController_1Person;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class S_SwitchMode : MonoBehaviour
{
    private RaycastHit hit;
    private Ray ray;
    public Camera b_camera;

    private void Update()
    {
        if (GameObject.Find("CharacterController (1Person)").GetComponent<InputManager>().GetActionShort())
        {
            SwitchLampMode();
        }
    }

    public void SwitchLampMode()
    {
        S_LampBehaviour hitLamp = hit.collider.gameObject.GetComponent<S_LampBehaviour>();

        if (hitLamp.lightType == S_LampBehaviour.LightType.WhiteLight)
        {
            hitLamp.lightType = S_LampBehaviour.LightType.BlackLight;
        }
        else if (hitLamp.lightType == S_LampBehaviour.LightType.BlackLight)
        {
            hitLamp.lightType = S_LampBehaviour.LightType.WhiteLight;
        }
    }
}
