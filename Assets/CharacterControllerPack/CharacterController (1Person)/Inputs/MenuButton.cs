﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuButton : MonoBehaviour
{
    public MenuController menuController;
    public int thisIndex;

    private void Update()
    {
        if(menuController.index == thisIndex)
        {
            this.gameObject.SetActive(false);
        }
    }

    public void Attack()
    {

    }
}
