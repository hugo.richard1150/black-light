﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class S_LampsManager : MonoBehaviour
{
    private Renderer myRenderer;
    private Transform[] positions;
    private Vector4[] vectorPositions;
    public S_LampBehaviour[] lights;
    public MaterialPropertyBlock materialProperty;

    void Start()
    {
        myRenderer = GetComponent<Renderer>();
        materialProperty = new MaterialPropertyBlock();
        positions = new Transform[lights.Length];
        vectorPositions = new Vector4[lights.Length];
        for (int i = 0; i < lights.Length; i++)
        {
            positions[i] = lights[i].transform;
        }
    }

    private void Update()
    {
        for (int i = 0; i < lights.Length; i++)
        {
            vectorPositions[i] = new Vector4(positions[i].position.x, positions[i].position.y, positions[i].position.z, 0);
        }
        materialProperty.SetVectorArray("EntrancePositionsArray", vectorPositions);
        myRenderer.SetPropertyBlock(materialProperty);
    }
}
