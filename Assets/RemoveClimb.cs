﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemoveClimb : MonoBehaviour
{
    public GameObject target;

    private void OnTriggerEnter(Collider other){
        if(other.CompareTag("Player")){
            target.layer = 0;
        }
    }
}
