﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class S_MusicManager : MonoBehaviour
{
    public FMODUnity.StudioEventEmitter music;
    public FMODUnity.StudioEventEmitter menuAmbiance;

    public bool firstFloor = false;
    public bool secondFloor = false;
    public bool thirdFloor = false;
    public bool underground = false;

    private void Start()
    {
        DontDestroyOnLoad(gameObject);
    }

    private void Update()
    {
        if (underground)
        {
            music.SetParameter("1erEtage", 0);
            music.SetParameter("2emeEtage", 0);
            music.SetParameter("3emeEtage", 0);
        }
        else
        {
            if (firstFloor)
            {
                menuAmbiance.SetParameter("Active", 0);
                music.SetParameter("1erEtage", 1);
            }
            if (secondFloor)
                music.SetParameter("2emeEtage", 1);
            if (thirdFloor)
                music.SetParameter("3emeEtage", 1);
        }
    }

    public void FirstFloorMusic()
    {
        firstFloor = true;
        music.Play();
    }

    public void SecondFloorMusic()
    {
        secondFloor = true;
    }

    public void ThirdFloorMusic()
    {
        thirdFloor = true;
    }

    public void UndergroundMusic()
    {
        underground = true;
    }
}
