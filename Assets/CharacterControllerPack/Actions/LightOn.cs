﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CharacterController_1Person
{
    public class LightOn : Interactable
    {
        public ActDialog actDialog;
        public GameObject lamp;

        private FMODUnity.StudioEventEmitter lightSound;

        public void Awake()
        {
            actDialog = GetComponent<ActDialog>();
            lightSound = gameObject.GetComponent<FMODUnity.StudioEventEmitter>();
        }

        public override void OnInteract()
        {
            base.OnInteract();
            if(actDialog != null && actDialog.read != 0 && !actDialog.dialogPanel.activeSelf)
            {
                Debug.Log("je passe");
                actDialog.StartDialog();
            }
            if (GameObject.Find("CharacterController (1Person)").GetComponent<InputManager>().GetActionShort())
            {
                SwitchLampMode();
            }
        }

        public void SwitchLampMode()
        {
            if(lamp != null)
            {
                if (lamp.GetComponent<S_LampBehaviour>())
                {
                    S_LampBehaviour hitLamp = lamp.GetComponent<S_LampBehaviour>();

                    if (hitLamp.lightType == S_LampBehaviour.LightType.WhiteLight)
                    {
                        hitLamp.lightType = S_LampBehaviour.LightType.BlackLight;
                        lightSound.SetParameter("LightState", 1);
                    }
                    else if (hitLamp.lightType == S_LampBehaviour.LightType.BlackLight)
                    {
                        hitLamp.lightType = S_LampBehaviour.LightType.WhiteLight;
                        lightSound.SetParameter("LightState", 0);
                    }

                    lightSound.Play();
                }
            }
            else
            {
                if (gameObject.GetComponent<S_LampBehaviour>())
                {
                    S_LampBehaviour hitLamp = gameObject.GetComponent<S_LampBehaviour>();

                    if (hitLamp.lightType == S_LampBehaviour.LightType.WhiteLight)
                    {
                        hitLamp.lightType = S_LampBehaviour.LightType.BlackLight;
                        lightSound.SetParameter("LightState", 1);
                    }
                    else if (hitLamp.lightType == S_LampBehaviour.LightType.BlackLight)
                    {
                        hitLamp.lightType = S_LampBehaviour.LightType.WhiteLight;
                        lightSound.SetParameter("LightState", 0);
                    }

                    lightSound.Play();
                }
            }
        }
    }
}
