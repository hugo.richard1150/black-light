﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class S_MaterialPropertyBlock : MonoBehaviour
{
    public S_LampBehaviour[] lights;
    public Renderer[] renderers;

    void Start()
    {
        renderers = (Renderer[])Object.FindObjectsOfType(typeof(Renderer));
        lights = (S_LampBehaviour[])Object.FindObjectsOfType(typeof(S_LampBehaviour));

        for (int i = 0; i < renderers.Length; i++)
        {
            renderers[i].gameObject.AddComponent<S_LampsManager>();
            S_LampsManager lampsManager = renderers[i].GetComponent<S_LampsManager>();
            lampsManager.lights = new S_LampBehaviour[lights.Length];
            lampsManager.lights = lights;
        }
    }
}
