// Upgrade NOTE: upgraded instancing buffer 'shader_InvisibleWall' to new syntax.

// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "shader_InvisibleWall"
{
	Properties
	{
		[Toggle(_SUBSTRACT_ON)] _Substract("Substract", Float) = 0
		_Tint("Tint", Color) = (0.4339623,0.4339623,0.4339623,0)
		_Texture("Texture ", 2D) = "white" {}
		_DistanceFeedbackLight("DistanceFeedbackLight", Range( 0 , 20)) = 10
		_PowerColorFeedback("PowerColorFeedback", Range( 0 , 10)) = 0
		_ColorWhiteLightFeedback("ColorWhiteLightFeedback", Color) = (1,0.4662507,0.2877358,0)
		_ColorBlackLightFeedback("ColorBlackLightFeedback", Color) = (1,0,0.3363633,0)
		[Toggle(_NEONMATERIAL_ON)] _NeonMaterial("NeonMaterial", Float) = 1
		_PoweEmession("Powe Emession", Range( 0 , 10)) = 0
		_OpacityMap("OpacityMap", 2D) = "white" {}
		_Cutoff( "Mask Clip Value", Float ) = 0.5
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "AlphaTest+0" "IgnoreProjector" = "True" "IsEmissive" = "true"  }
		Cull Back
		Blend One OneMinusSrcAlpha
		
		CGINCLUDE
		#include "UnityPBSLighting.cginc"
		#include "UnityShaderVariables.cginc"
		#include "Lighting.cginc"
		#pragma target 3.0
		#pragma multi_compile_instancing
		#pragma shader_feature_local _SUBSTRACT_ON
		#pragma shader_feature_local _NEONMATERIAL_ON
		#ifdef UNITY_PASS_SHADOWCASTER
			#undef INTERNAL_DATA
			#undef WorldReflectionVector
			#undef WorldNormalVector
			#define INTERNAL_DATA half3 internalSurfaceTtoW0; half3 internalSurfaceTtoW1; half3 internalSurfaceTtoW2;
			#define WorldReflectionVector(data,normal) reflect (data.worldRefl, half3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal)))
			#define WorldNormalVector(data,normal) half3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal))
		#endif
		struct Input
		{
			float2 uv_texcoord;
			float3 worldPos;
			float3 worldNormal;
			INTERNAL_DATA
		};

		struct SurfaceOutputCustomLightingCustom
		{
			half3 Albedo;
			half3 Normal;
			half3 Emission;
			half Metallic;
			half Smoothness;
			half Occlusion;
			half Alpha;
			Input SurfInput;
			UnityGIInput GIData;
		};

		uniform float4 _Tint;
		uniform sampler2D _Texture;
		uniform float _DistanceFeedbackLight;
		uniform float4 _ColorWhiteLightFeedback;
		uniform float _PowerColorFeedback;
		uniform float4 EntrancePositionsArray[30];
		uniform float3 FlashlightPos;
		uniform float RadiusFlashlight;
		uniform float _PoweEmession;
		uniform sampler2D _OpacityMap;
		uniform float _Cutoff = 0.5;

		UNITY_INSTANCING_BUFFER_START(shader_InvisibleWall)
			UNITY_DEFINE_INSTANCED_PROP(float4, _Texture_ST)
#define _Texture_ST_arr shader_InvisibleWall
			UNITY_DEFINE_INSTANCED_PROP(float4, _ColorBlackLightFeedback)
#define _ColorBlackLightFeedback_arr shader_InvisibleWall
			UNITY_DEFINE_INSTANCED_PROP(float4, _OpacityMap_ST)
#define _OpacityMap_ST_arr shader_InvisibleWall
		UNITY_INSTANCING_BUFFER_END(shader_InvisibleWall)


		float LoopArrayPositions55( float3 WorldPos, float3 ObjectPos )
		{
			float closest=10000;
			float now=0;
			for(int i=0; i<EntrancePositionsArray.Length;i++){
				now = distance(WorldPos,EntrancePositionsArray[i]);
				if(now < closest){
				closest = now;
				}
			}
			return closest;
		}


		inline half4 LightingStandardCustomLighting( inout SurfaceOutputCustomLightingCustom s, half3 viewDir, UnityGI gi )
		{
			UnityGIInput data = s.GIData;
			Input i = s.SurfInput;
			half4 c = 0;
			#ifdef UNITY_PASS_FORWARDBASE
			float ase_lightAtten = data.atten;
			if( _LightColor0.a == 0)
			ase_lightAtten = 0;
			#else
			float3 ase_lightAttenRGB = gi.light.color / ( ( _LightColor0.rgb ) + 0.000001 );
			float ase_lightAtten = max( max( ase_lightAttenRGB.r, ase_lightAttenRGB.g ), ase_lightAttenRGB.b );
			#endif
			#if defined(HANDLE_SHADOWS_BLENDING_IN_GI)
			half bakedAtten = UnitySampleBakedOcclusion(data.lightmapUV.xy, data.worldPos);
			float zDist = dot(_WorldSpaceCameraPos - data.worldPos, UNITY_MATRIX_V[2].xyz);
			float fadeDist = UnityComputeShadowFadeDistance(data.worldPos, zDist);
			ase_lightAtten = UnityMixRealtimeAndBakedShadows(data.atten, bakedAtten, UnityComputeShadowFade(fadeDist));
			#endif
			#if defined(LIGHTMAP_ON) && ( UNITY_VERSION < 560 || ( defined(LIGHTMAP_SHADOW_MIXING) && !defined(SHADOWS_SHADOWMASK) && defined(SHADOWS_SCREEN) ) )//aselc
			float4 ase_lightColor = 0;
			#else //aselc
			float4 ase_lightColor = _LightColor0;
			#endif //aselc
			float4 normalizeResult65 = normalize( ase_lightColor );
			float4 color59 = IsGammaSpace() ? float4(0.3882353,0,0.9607843,0) : float4(0.1247718,0,0.9130987,0);
			float4 normalizeResult68 = normalize( color59 );
			float dotResult72 = dot( normalizeResult65 , normalizeResult68 );
			float LightsStates77 = dotResult72;
			float4 _Texture_ST_Instance = UNITY_ACCESS_INSTANCED_PROP(_Texture_ST_arr, _Texture_ST);
			float2 uv_Texture = i.uv_texcoord * _Texture_ST_Instance.xy + _Texture_ST_Instance.zw;
			float4 tex2DNode16 = tex2D( _Texture, uv_Texture );
			float4 Albedo31 = ( _Tint * tex2DNode16 );
			float3 ase_worldNormal = WorldNormalVector( i, float3( 0, 0, 1 ) );
			UnityGI gi251 = gi;
			float3 diffNorm251 = ase_worldNormal;
			gi251 = UnityGI_Base( data, 1, diffNorm251 );
			float3 indirectDiffuse251 = gi251.indirect.diffuse + diffNorm251 * 0.0001;
			float4 color246 = IsGammaSpace() ? float4(0.8235294,0.5294118,0.945098,0) : float4(0.6444798,0.2422812,0.8796223,0);
			float4 color247 = IsGammaSpace() ? float4(0.4980392,0,0.7058824,0) : float4(0.2122307,0,0.4564111,0);
			float4 blendOpSrc250 = color246;
			float4 blendOpDest250 = color247;
			float4 temp_output_250_0 = ( saturate( ( blendOpSrc250 + blendOpDest250 ) ));
			float4 color248 = IsGammaSpace() ? float4(0.522742,0,0.735849,0) : float4(0.2356978,0,0.5007474,0);
			float4 blendOpSrc253 = temp_output_250_0;
			float4 blendOpDest253 = color248;
			float4 _ColorBlackLightFeedback_Instance = UNITY_ACCESS_INSTANCED_PROP(_ColorBlackLightFeedback_arr, _ColorBlackLightFeedback);
			float4 BlackLighting268 = ( ( Albedo31 * ( float4( ( ( indirectDiffuse251 + pow( ase_lightAtten , 0.1 ) ) * ase_lightColor.rgb ) , 0.0 ) * ( temp_output_250_0 * ( saturate( 2.0f*blendOpDest253*blendOpSrc253 + blendOpDest253*blendOpDest253*(1.0f - 2.0f*blendOpSrc253) )) ) ) ) + ( pow( ase_lightAtten , 10.0 ) * ( _ColorBlackLightFeedback_Instance * 10.0 ) ) );
			UnityGI gi230 = gi;
			float3 diffNorm230 = ase_worldNormal;
			gi230 = UnityGI_Base( data, 1, diffNorm230 );
			float3 indirectDiffuse230 = gi230.indirect.diffuse + diffNorm230 * 0.0001;
			float4 lighting243 = ( Albedo31 * ( float4( ( ( indirectDiffuse230 + pow( ase_lightAtten , 0.1 ) ) * ase_lightColor.rgb ) , 0.0 ) + ( pow( ase_lightAtten , _DistanceFeedbackLight ) * ( _ColorWhiteLightFeedback * _PowerColorFeedback ) ) ) );
			float3 ase_worldPos = i.worldPos;
			float3 WorldPos55 = ase_worldPos;
			float3 ObjectPos55 = EntrancePositionsArray[0].xyz;
			float localLoopArrayPositions55 = LoopArrayPositions55( WorldPos55 , ObjectPos55 );
			float clampResult78 = clamp( pow( pow( ( localLoopArrayPositions55 / 4.0 ) , 4000.0 ) , ase_lightAtten ) , 0.0 , 1.0 );
			float PositionsLoop82 = clampResult78;
			float4 lerpResult91 = lerp( ( LightsStates77 == 1.0 ? BlackLighting268 : lighting243 ) , lighting243 , PositionsLoop82);
			float4 Result224 = lerpResult91;
			float FlashlightDirection208 = distance( ase_worldPos , FlashlightPos );
			float RadiusFlashlight205 = ( RadiusFlashlight / 2.0 );
			float clampResult201 = clamp( ( ( FlashlightDirection208 - RadiusFlashlight205 ) / 0.1 ) , 0.0 , 1.0 );
			float4 lerpResult215 = lerp( Result224 , float4( 0,0,0,0 ) , clampResult201);
			float4 OpacityFlashlightAddition202 = lerpResult215;
			float clampResult177 = clamp( ( ( FlashlightDirection208 - RadiusFlashlight205 ) / 0.1 ) , 0.0 , 1.0 );
			float OpacityFlashlightSubtract181 = clampResult177;
			float4 temp_cast_8 = (OpacityFlashlightSubtract181).xxxx;
			#ifdef _SUBSTRACT_ON
				float4 staticSwitch191 = temp_cast_8;
			#else
				float4 staticSwitch191 = OpacityFlashlightAddition202;
			#endif
			float4 _OpacityMap_ST_Instance = UNITY_ACCESS_INSTANCED_PROP(_OpacityMap_ST_arr, _OpacityMap_ST);
			float2 uv_OpacityMap = i.uv_texcoord * _OpacityMap_ST_Instance.xy + _OpacityMap_ST_Instance.zw;
			float4 tex2DNode269 = tex2D( _OpacityMap, uv_OpacityMap );
			c.rgb = Result224.rgb;
			c.a = staticSwitch191.r;
			c.rgb *= c.a;
			clip( ( tex2DNode269.r * tex2DNode269.g * tex2DNode269.b ) - _Cutoff );
			return c;
		}

		inline void LightingStandardCustomLighting_GI( inout SurfaceOutputCustomLightingCustom s, UnityGIInput data, inout UnityGI gi )
		{
			s.GIData = data;
		}

		void surf( Input i , inout SurfaceOutputCustomLightingCustom o )
		{
			o.SurfInput = i;
			o.Normal = float3(0,0,1);
			#if defined(LIGHTMAP_ON) && ( UNITY_VERSION < 560 || ( defined(LIGHTMAP_SHADOW_MIXING) && !defined(SHADOWS_SHADOWMASK) && defined(SHADOWS_SCREEN) ) )//aselc
			float4 ase_lightColor = 0;
			#else //aselc
			float4 ase_lightColor = _LightColor0;
			#endif //aselc
			float4 normalizeResult65 = normalize( ase_lightColor );
			float4 color59 = IsGammaSpace() ? float4(0.3882353,0,0.9607843,0) : float4(0.1247718,0,0.9130987,0);
			float4 normalizeResult68 = normalize( color59 );
			float dotResult72 = dot( normalizeResult65 , normalizeResult68 );
			float LightsStates77 = dotResult72;
			float4 _Texture_ST_Instance = UNITY_ACCESS_INSTANCED_PROP(_Texture_ST_arr, _Texture_ST);
			float2 uv_Texture = i.uv_texcoord * _Texture_ST_Instance.xy + _Texture_ST_Instance.zw;
			float4 tex2DNode16 = tex2D( _Texture, uv_Texture );
			float4 Albedo31 = ( _Tint * tex2DNode16 );
			float4 color246 = IsGammaSpace() ? float4(0.8235294,0.5294118,0.945098,0) : float4(0.6444798,0.2422812,0.8796223,0);
			float4 color247 = IsGammaSpace() ? float4(0.4980392,0,0.7058824,0) : float4(0.2122307,0,0.4564111,0);
			float4 blendOpSrc250 = color246;
			float4 blendOpDest250 = color247;
			float4 temp_output_250_0 = ( saturate( ( blendOpSrc250 + blendOpDest250 ) ));
			float4 color248 = IsGammaSpace() ? float4(0.522742,0,0.735849,0) : float4(0.2356978,0,0.5007474,0);
			float4 blendOpSrc253 = temp_output_250_0;
			float4 blendOpDest253 = color248;
			float4 _ColorBlackLightFeedback_Instance = UNITY_ACCESS_INSTANCED_PROP(_ColorBlackLightFeedback_arr, _ColorBlackLightFeedback);
			float4 BlackLighting268 = ( ( Albedo31 * ( float4( ( ( float3(0,0,0) + pow( 1 , 0.1 ) ) * ase_lightColor.rgb ) , 0.0 ) * ( temp_output_250_0 * ( saturate( 2.0f*blendOpDest253*blendOpSrc253 + blendOpDest253*blendOpDest253*(1.0f - 2.0f*blendOpSrc253) )) ) ) ) + ( pow( 1 , 10.0 ) * ( _ColorBlackLightFeedback_Instance * 10.0 ) ) );
			float4 lighting243 = ( Albedo31 * ( float4( ( ( float3(0,0,0) + pow( 1 , 0.1 ) ) * ase_lightColor.rgb ) , 0.0 ) + ( pow( 1 , _DistanceFeedbackLight ) * ( _ColorWhiteLightFeedback * _PowerColorFeedback ) ) ) );
			float3 ase_worldPos = i.worldPos;
			float3 WorldPos55 = ase_worldPos;
			float3 ObjectPos55 = EntrancePositionsArray[0].xyz;
			float localLoopArrayPositions55 = LoopArrayPositions55( WorldPos55 , ObjectPos55 );
			float clampResult78 = clamp( pow( pow( ( localLoopArrayPositions55 / 4.0 ) , 4000.0 ) , 1 ) , 0.0 , 1.0 );
			float PositionsLoop82 = clampResult78;
			float4 lerpResult91 = lerp( ( LightsStates77 == 1.0 ? BlackLighting268 : lighting243 ) , lighting243 , PositionsLoop82);
			float4 Result224 = lerpResult91;
			float FlashlightDirection208 = distance( ase_worldPos , FlashlightPos );
			float RadiusFlashlight205 = ( RadiusFlashlight / 2.0 );
			float clampResult201 = clamp( ( ( FlashlightDirection208 - RadiusFlashlight205 ) / 0.1 ) , 0.0 , 1.0 );
			float4 lerpResult215 = lerp( Result224 , float4( 0,0,0,0 ) , clampResult201);
			float4 OpacityFlashlightAddition202 = lerpResult215;
			float clampResult177 = clamp( ( ( FlashlightDirection208 - RadiusFlashlight205 ) / 0.1 ) , 0.0 , 1.0 );
			float OpacityFlashlightSubtract181 = clampResult177;
			float4 temp_cast_3 = (OpacityFlashlightSubtract181).xxxx;
			#ifdef _SUBSTRACT_ON
				float4 staticSwitch191 = temp_cast_3;
			#else
				float4 staticSwitch191 = OpacityFlashlightAddition202;
			#endif
			#ifdef _NEONMATERIAL_ON
				float4 staticSwitch109 = ( tex2DNode16 * _PoweEmession );
			#else
				float4 staticSwitch109 = float4( 0,0,0,0 );
			#endif
			o.Emission = ( staticSwitch191 * staticSwitch109 ).rgb;
		}

		ENDCG
		CGPROGRAM
		#pragma surface surf StandardCustomLighting keepalpha fullforwardshadows 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			#include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			sampler3D _DitherMaskLOD;
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float2 customPack1 : TEXCOORD1;
				float4 tSpace0 : TEXCOORD2;
				float4 tSpace1 : TEXCOORD3;
				float4 tSpace2 : TEXCOORD4;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO( o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				Input customInputData;
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				half3 worldNormal = UnityObjectToWorldNormal( v.normal );
				half3 worldTangent = UnityObjectToWorldDir( v.tangent.xyz );
				half tangentSign = v.tangent.w * unity_WorldTransformParams.w;
				half3 worldBinormal = cross( worldNormal, worldTangent ) * tangentSign;
				o.tSpace0 = float4( worldTangent.x, worldBinormal.x, worldNormal.x, worldPos.x );
				o.tSpace1 = float4( worldTangent.y, worldBinormal.y, worldNormal.y, worldPos.y );
				o.tSpace2 = float4( worldTangent.z, worldBinormal.z, worldNormal.z, worldPos.z );
				o.customPack1.xy = customInputData.uv_texcoord;
				o.customPack1.xy = v.texcoord;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				return o;
			}
			half4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				surfIN.uv_texcoord = IN.customPack1.xy;
				float3 worldPos = float3( IN.tSpace0.w, IN.tSpace1.w, IN.tSpace2.w );
				half3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				surfIN.worldPos = worldPos;
				surfIN.worldNormal = float3( IN.tSpace0.z, IN.tSpace1.z, IN.tSpace2.z );
				surfIN.internalSurfaceTtoW0 = IN.tSpace0.xyz;
				surfIN.internalSurfaceTtoW1 = IN.tSpace1.xyz;
				surfIN.internalSurfaceTtoW2 = IN.tSpace2.xyz;
				SurfaceOutputCustomLightingCustom o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutputCustomLightingCustom, o )
				surf( surfIN, o );
				UnityGI gi;
				UNITY_INITIALIZE_OUTPUT( UnityGI, gi );
				o.Alpha = LightingStandardCustomLighting( o, worldViewDir, gi ).a;
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				half alphaRef = tex3D( _DitherMaskLOD, float3( vpos.xy * 0.25, o.Alpha * 0.9375 ) ).a;
				clip( alphaRef - 0.01 );
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=18900
0;73;1180;648;3347.512;-3816.169;1.102742;True;False
Node;AmplifyShaderEditor.CommentaryNode;244;-2545.006,1103.539;Inherit;False;2480.969;1508.055;Black Lighting;18;268;267;266;262;261;259;257;255;254;253;252;251;250;249;248;247;246;245;Black Lighting;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;226;-4482.002,1227.973;Inherit;False;1794.299;1372.021;Comment;11;243;242;241;240;239;236;234;233;230;228;227;Lighting;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;11;-1730.162,-664.2731;Inherit;False;828.3352;474.0365;Albedo;4;31;27;16;15;;1,1,1,1;0;0
Node;AmplifyShaderEditor.ColorNode;247;-2133.933,1919.51;Inherit;False;Constant;_Color2;Color 2;8;0;Create;True;0;0;0;False;0;False;0.4980392,0,0.7058824,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;246;-2126.455,1716.954;Inherit;False;Constant;_Color1;Color 1;8;0;Create;True;0;0;0;False;0;False;0.8235294,0.5294118,0.945098,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LightAttenuation;245;-2357.857,1497.04;Inherit;False;0;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;30;-3403.288,2984.291;Inherit;False;1440.589;364.5928;Positions lights loop;10;82;78;74;70;58;55;53;44;43;162;Lights Positions Loop;1,1,1,1;0;0
Node;AmplifyShaderEditor.LightAttenuation;227;-4244.396,1398.155;Inherit;False;0;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;228;-4309.623,1766.057;Inherit;False;1011.394;439.1484;Petite Light;6;238;237;235;232;231;229;;0.9811321,0.6390607,0.2915628,1;0;0
Node;AmplifyShaderEditor.BlendOpsNode;250;-1838.714,1713.157;Inherit;False;LinearDodge;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;1;False;1;COLOR;0
Node;AmplifyShaderEditor.PowerNode;249;-2097.641,1491.287;Inherit;False;False;2;0;FLOAT;0;False;1;FLOAT;0.1;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;248;-1882.566,1944.566;Inherit;False;Constant;_Color0;Color 0;8;0;Create;True;0;0;0;False;0;False;0.522742,0,0.735849,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.IndirectDiffuseLighting;251;-2300.806,1360.515;Inherit;False;Tangent;1;0;FLOAT3;0,0,1;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ColorNode;15;-1614.628,-614.2732;Inherit;False;Property;_Tint;Tint;1;0;Create;True;0;0;0;False;0;False;0.4339623,0.4339623,0.4339623,0;1,1,1,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;16;-1680.162,-420.2374;Inherit;True;Property;_Texture;Texture ;2;0;Create;True;0;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.CommentaryNode;252;-2391.063,2132.65;Inherit;False;1011.394;439.1484;Petite Light;6;265;264;263;258;256;272;;0.9803922,0.2901962,0.7915436,1;0;0
Node;AmplifyShaderEditor.LightColorNode;255;-1812.184,1546.561;Inherit;False;0;3;COLOR;0;FLOAT3;1;FLOAT;2
Node;AmplifyShaderEditor.GlobalArrayNode;44;-3382.379,3190.249;Inherit;False;EntrancePositionsArray;0;30;2;False;False;0;1;False;Object;147;4;0;INT;0;False;2;INT;0;False;1;INT;0;False;3;INT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.WorldPosInputsNode;43;-3336.594,3041.48;Inherit;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RangedFloatNode;232;-4262.855,1831.137;Inherit;False;Property;_DistanceFeedbackLight;DistanceFeedbackLight;3;0;Create;True;0;0;0;False;0;False;10;10;0;20;0;1;FLOAT;0
Node;AmplifyShaderEditor.PowerNode;233;-3917.426,1413.238;Inherit;False;False;2;0;FLOAT;0;False;1;FLOAT;0.1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;254;-1933.325,1385.306;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;229;-4006.702,2088.288;Inherit;False;Property;_PowerColorFeedback;PowerColorFeedback;4;0;Create;True;0;0;0;False;0;False;0;1;0;10;0;1;FLOAT;0
Node;AmplifyShaderEditor.BlendOpsNode;253;-1615.078,1872.735;Inherit;True;SoftLight;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;1;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;27;-1351.628,-488.2733;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.IndirectDiffuseLighting;230;-4129.97,1294.305;Inherit;False;Tangent;1;0;FLOAT3;0,0,1;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ColorNode;231;-3948.067,1914.755;Inherit;False;Property;_ColorWhiteLightFeedback;ColorWhiteLightFeedback;5;0;Create;True;0;0;0;False;0;False;1,0.4662507,0.2877358,0;1,0.4662507,0.2877357,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;236;-3675.418,1350.8;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ColorNode;258;-2061.518,2280.43;Inherit;False;InstancedProperty;_ColorBlackLightFeedback;ColorBlackLightFeedback;6;0;Create;True;0;0;0;False;0;False;1,0,0.3363633,0;0.3396226,0.1778212,0.333151,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;237;-3708.077,1920.477;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;272;-2284.849,2197.935;Inherit;False;Constant;_Float1;Float 1;3;0;Create;True;0;0;0;False;0;False;10;10;0;20;0;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;56;-1918.707,2976.14;Inherit;False;728.8997;365.3354;Lights states;7;103;77;72;68;65;63;59;Lights States;1,1,1,1;0;0
Node;AmplifyShaderEditor.CustomExpressionNode;55;-3124.992,3093.401;Float;False;float closest=10000@$float now=0@$for(int i=0@ i<EntrancePositionsArray.Length@i++){$	now = distance(WorldPos,EntrancePositionsArray[i])@$	if(now < closest){$	closest = now@$	}$}$return closest@;1;False;2;True;WorldPos;FLOAT3;0,0,0;In;;Inherit;False;True;ObjectPos;FLOAT3;0,0,0;In;;Inherit;False;Loop Array Positions;True;False;0;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;259;-1600.738,1398.259;Inherit;True;2;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;53;-3095.36,3206.942;Inherit;False;Constant;_Distance;Distance;2;0;Create;True;0;0;0;False;0;False;4;4;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;31;-1126.626,-451.2733;Inherit;False;Albedo;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;257;-1428.017,1695.319;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;256;-1773.274,2445.537;Inherit;False;Constant;_Float0;Float 0;8;0;Create;True;0;0;0;False;0;False;10;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.LightColorNode;234;-3863.524,1569.544;Inherit;False;0;3;COLOR;0;FLOAT3;1;FLOAT;2
Node;AmplifyShaderEditor.PowerNode;235;-3895.26,1816.057;Inherit;False;False;2;0;FLOAT;0;False;1;FLOAT;0.1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;58;-2908.153,3095.992;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.PowerNode;264;-1976.699,2182.65;Inherit;False;False;2;0;FLOAT;0;False;1;FLOAT;0.1;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;59;-1902.047,3165.48;Inherit;False;Constant;_ColorBlacklightState;Color Blacklight State;8;0;Create;True;0;0;0;False;0;False;0.3882353,0,0.9607843,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;263;-1739.415,2301.369;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.LightColorNode;63;-1838.76,3047.417;Inherit;False;0;3;COLOR;0;FLOAT3;1;FLOAT;2
Node;AmplifyShaderEditor.GetLocalVarNode;262;-2382.886,1148.31;Inherit;False;31;Albedo;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;261;-1105.99,1328.491;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;239;-3554.893,1486.314;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;238;-3460.627,1818.188;Inherit;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.CommentaryNode;211;-1926.813,3521.821;Inherit;False;720.5781;380.2058;Flashlight direction;4;208;196;195;193;Flashlight direction;1,1,1,1;0;0
Node;AmplifyShaderEditor.NormalizeNode;68;-1678.755,3167.152;Inherit;False;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;241;-3395.344,1269.431;Inherit;False;31;Albedo;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;265;-1535.419,2259.317;Inherit;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;240;-3240.634,1456.352;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.PowerNode;70;-2722.617,3095.347;Inherit;False;False;2;0;FLOAT;0;False;1;FLOAT;4000;False;1;FLOAT;0
Node;AmplifyShaderEditor.LightAttenuation;162;-2737.723,3242.032;Inherit;False;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;266;-995.6144,1168.461;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.NormalizeNode;65;-1681.4,3046.963;Inherit;False;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;267;-752.9505,1199.976;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WorldPosInputsNode;193;-1826.439,3568.721;Inherit;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RangedFloatNode;203;-1773.232,4073.252;Inherit;False;Global;RadiusFlashlight;RadiusFlashlight;12;0;Create;True;0;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.PowerNode;74;-2520.379,3093.839;Inherit;False;False;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;242;-3122.538,1274.667;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.CommentaryNode;212;-1814.86,4004.512;Inherit;False;612.4364;206.3147;Flashlight radius;2;205;204;Flashlight radius;1,1,1,1;0;0
Node;AmplifyShaderEditor.DotProductOpNode;72;-1518.726,3093.426;Inherit;False;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.Vector3Node;195;-1843.517,3729.232;Inherit;False;Global;FlashlightPos;FlashlightPos;11;0;Create;True;0;0;0;False;0;False;0,0,0;0,0,0;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RegisterLocalVarNode;268;-577.1677,1198.536;Inherit;False;BlackLighting;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ClampOpNode;78;-2347.558,3094.214;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.DistanceOpNode;196;-1600.475,3660.747;Inherit;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;243;-2937.43,1276.878;Inherit;False;lighting;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;204;-1564.669,4073.771;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;2;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;77;-1397.388,3089.048;Float;False;LightsStates;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;80;-1232.943,40.88698;Inherit;False;77;LightsStates;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;83;-1177.275,120.521;Inherit;False;Constant;_Float2;Float 2;8;0;Create;True;0;0;0;False;0;False;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;192;-1121.189,4007.059;Inherit;False;1570.247;334.7673;Flashlight addition;9;202;201;200;206;209;215;221;223;225;Flashlight addition;1,1,1,1;0;0
Node;AmplifyShaderEditor.GetLocalVarNode;79;-1254.902,192.862;Inherit;False;268;BlackLighting;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;82;-2179.558,3088.214;Inherit;False;PositionsLoop;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;208;-1451.521,3656.64;Inherit;False;FlashlightDirection;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;205;-1426.967,4057.461;Inherit;False;RadiusFlashlight;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;81;-1247.115,270.2069;Inherit;False;243;lighting;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.Compare;87;-977.2988,66.83388;Inherit;False;0;4;0;FLOAT;0;False;1;FLOAT;0;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;85;-675.4996,264.022;Inherit;False;243;lighting;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;206;-1080.283,4195.504;Inherit;False;205;RadiusFlashlight;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;209;-1094.879,4110.46;Inherit;False;208;FlashlightDirection;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;183;-1121.292,3526.313;Inherit;False;1172.066;385.3833;Flashlight subtract;7;207;210;181;177;178;176;179;Flashlight subtract;1,1,1,1;0;0
Node;AmplifyShaderEditor.GetLocalVarNode;86;-699.5645,366.7219;Inherit;False;82;PositionsLoop;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;210;-1102.3,3642.741;Inherit;False;208;FlashlightDirection;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;223;-780.2959,4119.499;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;207;-1088.981,3728.779;Inherit;False;205;RadiusFlashlight;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;91;-450.7846,237.454;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;221;-823.8755,4243.884;Inherit;False;Constant;_Float4;Float 4;12;0;Create;True;0;0;0;False;0;False;0.1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;224;-234.4155,237.8209;Inherit;False;Result;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;176;-732.2543,3668.277;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;179;-817.5153,3780.989;Inherit;False;Constant;_SmoothnessFlashlight;SmoothnessFlashlight;12;0;Create;True;0;0;0;False;0;False;0.1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;200;-562.8997,4206.062;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ClampOpNode;201;-400.0076,4205.292;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;225;-219.0598,4097.774;Inherit;False;224;Result;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;178;-566.6157,3673.288;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ClampOpNode;177;-398.0546,3680.077;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;215;-5.962037,4125.532;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;181;-230.3326,3677.077;Inherit;False;OpacityFlashlightSubtract;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;88;-804.9412,-275.0639;Inherit;False;Property;_PoweEmession;Powe Emession;8;0;Create;True;0;0;0;False;0;False;0;0;0;10;0;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;202;192.9158,4121.753;Inherit;False;OpacityFlashlightAddition;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;182;-591.2633,-90.42954;Inherit;False;181;OpacityFlashlightSubtract;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;92;-502.2358,-337.7709;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;213;-595.3426,-170.3851;Inherit;False;202;OpacityFlashlightAddition;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.StaticSwitch;109;-335.1131,-307.5671;Inherit;False;Property;_NeonMaterial;NeonMaterial;7;0;Create;True;0;0;0;False;0;False;0;1;1;True;_NeonMaterial;Toggle;2;Key0;Key1;Create;True;True;9;1;COLOR;0,0,0,0;False;0;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;4;COLOR;0,0,0,0;False;5;COLOR;0,0,0,0;False;6;COLOR;0,0,0,0;False;7;COLOR;0,0,0,0;False;8;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;269;-471.644,18.49655;Inherit;True;Property;_OpacityMap;OpacityMap;9;0;Create;True;0;0;0;False;0;False;-1;None;04a05161ecd0a0744836402c96f82c0e;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.StaticSwitch;191;-304.6639,-140.5879;Inherit;False;Property;_Substract;Substract;0;0;Create;True;0;0;0;False;0;False;0;0;0;True;;Toggle;2;Key0;Key1;Create;True;True;9;1;COLOR;0,0,0,0;False;0;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;4;COLOR;0,0,0,0;False;5;COLOR;0,0,0,0;False;6;COLOR;0,0,0,0;False;7;COLOR;0,0,0,0;False;8;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.CustomExpressionNode;280;-2919.334,3637.007;Float;False;float now@$for(int i=0@ i<30@i++){$	now = EntranceFlashlightRadiusArray[i]@$}$return now@;1;False;1;True;radius;FLOAT;0;In;;Inherit;False;Loop Array Radius Flashlight;True;False;0;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;279;-86.40596,-229.4715;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;290;-2689.175,4416.063;Inherit;False;Property;_Float3;Float 3;11;0;Create;True;0;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;270;-161.9982,98.18754;Inherit;False;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GlobalArrayNode;283;-2579.41,3709.617;Inherit;False;EntranceFlashlightRadiusArray;0;30;0;False;False;0;1;False;Object;-1;4;0;INT;0;False;2;INT;0;False;1;INT;0;False;3;INT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CustomExpressionNode;287;-2472.437,4072.21;Float;False;float now@$for(int i=0@ i<30@i++){$	now = EntranceSpheresRadiusArray[i]@$}$return now@;1;False;1;True;radius;FLOAT;0;In;;Inherit;False;Loop Array Radius Flashlight;True;False;0;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GlobalArrayNode;288;-2759.737,4063.112;Inherit;False;MyGlobalArray;0;30;0;False;False;0;1;False;Object;-1;4;0;INT;0;False;2;INT;0;False;1;INT;0;False;3;INT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WorldPosInputsNode;281;-2501.316,3457.493;Inherit;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RegisterLocalVarNode;103;-1397.388,3089.048;Float;False;LightsStates;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CustomExpressionNode;282;-2281.694,3519.254;Float;False;float closest=10000@$float now=0@$for(int i=0@ i<EntranceFlashlightPositionsArray.Length@i++){$	now = distance(WorldPos,EntranceFlashlightPositionsArray[i])@$	if(now < closest){$	closest = now - EntranceFlashlightRadiusArray[i]@$	}$}$return closest@;3;False;3;True;WorldPos;FLOAT3;0,0,0;In;;Inherit;False;True;ObjectPos;FLOAT3;0,0,0;In;;Inherit;False;True;Radius;FLOAT;0;In;;Inherit;False;Loop Array Positions Flashlight;True;False;0;3;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.CustomExpressionNode;285;-2923.03,3538.748;Float;False;float closest=10000@$float now=0@$for(int i=0@ i<EntrancePositionsArray.Length@i++){$	now = distance(WorldPos,EntrancePositionsArray[i])@$	if(now < closest){$	closest = now@$	}$}$return closest@;1;False;2;True;WorldPos;FLOAT3;0,0,0;In;;Inherit;False;True;ObjectPos;FLOAT3;0,0,0;In;;Inherit;False;Loop Array Radius Flashlight;True;False;0;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GlobalArrayNode;284;-2592.738,3606.528;Inherit;False;EntranceFlashlightPositionsArray;0;30;2;False;False;0;1;False;Object;147;4;0;INT;0;False;2;INT;0;False;1;INT;0;False;3;INT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;77.7,-7.8;Float;False;True;-1;2;ASEMaterialInspector;0;0;CustomLighting;shader_InvisibleWall;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Custom;0.5;True;True;0;True;Opaque;;AlphaTest;All;14;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;3;1;False;-1;10;False;-1;0;1;False;-1;1;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;10;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;False;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;4;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;250;0;246;0
WireConnection;250;1;247;0
WireConnection;249;0;245;0
WireConnection;233;0;227;0
WireConnection;254;0;251;0
WireConnection;254;1;249;0
WireConnection;253;0;250;0
WireConnection;253;1;248;0
WireConnection;27;0;15;0
WireConnection;27;1;16;0
WireConnection;236;0;230;0
WireConnection;236;1;233;0
WireConnection;237;0;231;0
WireConnection;237;1;229;0
WireConnection;55;0;43;0
WireConnection;55;1;44;0
WireConnection;259;0;254;0
WireConnection;259;1;255;1
WireConnection;31;0;27;0
WireConnection;257;0;250;0
WireConnection;257;1;253;0
WireConnection;235;0;227;0
WireConnection;235;1;232;0
WireConnection;58;0;55;0
WireConnection;58;1;53;0
WireConnection;264;0;245;0
WireConnection;264;1;272;0
WireConnection;263;0;258;0
WireConnection;263;1;256;0
WireConnection;261;0;259;0
WireConnection;261;1;257;0
WireConnection;239;0;236;0
WireConnection;239;1;234;1
WireConnection;238;0;235;0
WireConnection;238;1;237;0
WireConnection;68;0;59;0
WireConnection;265;0;264;0
WireConnection;265;1;263;0
WireConnection;240;0;239;0
WireConnection;240;1;238;0
WireConnection;70;0;58;0
WireConnection;266;0;262;0
WireConnection;266;1;261;0
WireConnection;65;0;63;0
WireConnection;267;0;266;0
WireConnection;267;1;265;0
WireConnection;74;0;70;0
WireConnection;74;1;162;0
WireConnection;242;0;241;0
WireConnection;242;1;240;0
WireConnection;72;0;65;0
WireConnection;72;1;68;0
WireConnection;268;0;267;0
WireConnection;78;0;74;0
WireConnection;196;0;193;0
WireConnection;196;1;195;0
WireConnection;243;0;242;0
WireConnection;204;0;203;0
WireConnection;77;0;72;0
WireConnection;82;0;78;0
WireConnection;208;0;196;0
WireConnection;205;0;204;0
WireConnection;87;0;80;0
WireConnection;87;1;83;0
WireConnection;87;2;79;0
WireConnection;87;3;81;0
WireConnection;223;0;209;0
WireConnection;223;1;206;0
WireConnection;91;0;87;0
WireConnection;91;1;85;0
WireConnection;91;2;86;0
WireConnection;224;0;91;0
WireConnection;176;0;210;0
WireConnection;176;1;207;0
WireConnection;200;0;223;0
WireConnection;200;1;221;0
WireConnection;201;0;200;0
WireConnection;178;0;176;0
WireConnection;178;1;179;0
WireConnection;177;0;178;0
WireConnection;215;0;225;0
WireConnection;215;2;201;0
WireConnection;181;0;177;0
WireConnection;202;0;215;0
WireConnection;92;0;16;0
WireConnection;92;1;88;0
WireConnection;109;0;92;0
WireConnection;191;1;213;0
WireConnection;191;0;182;0
WireConnection;279;0;191;0
WireConnection;279;1;109;0
WireConnection;270;0;269;1
WireConnection;270;1;269;2
WireConnection;270;2;269;3
WireConnection;287;0;288;0
WireConnection;282;0;281;0
WireConnection;282;1;284;0
WireConnection;282;2;283;0
WireConnection;0;2;279;0
WireConnection;0;9;191;0
WireConnection;0;10;270;0
WireConnection;0;13;224;0
ASEEND*/
//CHKSM=FFA5B8394804ECBD688B23F0A69F0AF9B0ED87CA