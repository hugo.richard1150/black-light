﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class S_CylinderFlashlight : MonoBehaviour
{
    public int sphereIndex;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "InvisibleWall")
        {
            other.gameObject.GetComponent<BoxCollider>().isTrigger = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "InvisibleWall")
        {
            other.gameObject.GetComponent<BoxCollider>().isTrigger = false;
        }
    }
}
