﻿using CharacterController_1Person;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveSystem : MonoBehaviour
{
    public Open[] elements;

    private void Awake()
    {
        if(PlayerPrefs.HasKey("DoorState"))
        {
            OnLoad();
        }
    }

    public void OnSave()
    {
        foreach (Open element in elements)
        {
            SaveObject(element);
        }
    }

    public void OnLoad()
    {
        foreach (Open element in elements)
        {
            LoadObject(element);
        }
    }

    void SaveObject(Open element)
    {
        foreach (Open script in elements)
        {
            // L'animation de la porte
            PlayerPrefs.SetInt("DoorState", element.doorState);
            // La condition d'ouverture
            PlayerPrefs.SetInt("Condition", element.condition ? 1 : 0);
        }
        Debug.Log("Game saving ...");
    }

    void LoadObject(Open element)
    {
        // L'animation de la porte
        element.doorState = PlayerPrefs.GetInt("DoorState");
        // La condition d'ouverture
        element.condition = (PlayerPrefs.GetInt("Condition") != 0);
        Debug.Log("Game Loading ...");
    }
}
