﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CharacterController_1Person;

public class S_FlashlightsBehaviour : MonoBehaviour
{
    public GameObject WhitelightObject;
    public GameObject UVlightObject;

    private RaycastHit hit;
    public LayerMask layerMaskRaycast;

    //Cylinder
    private GameObject cylinder;
    private GameObject[] spheresFlashlight;
    private Vector3 hitPos;

    //Var light
    private float range;
    private float spotAngle;

    private void Start()
    {
        //Var light
        range = WhitelightObject.GetComponent<Light>().range;
        spotAngle = WhitelightObject.GetComponent<Light>().spotAngle;

        //Sphere
        cylinder = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        cylinder.layer = 14;
        cylinder.GetComponent<MeshRenderer>().enabled = false;
        cylinder.GetComponent<SphereCollider>().isTrigger = true;
        cylinder.AddComponent<S_CylinderFlashlight>();
        Rigidbody rbCylinder = cylinder.AddComponent<Rigidbody>();
        rbCylinder.useGravity = false;

        //CreateSpheres();
    }

    private void Update()
    {
        if (GameObject.Find("CharacterController (1Person)").GetComponent<InputManager>().GetFlashLight())
        {
            UvFlashlight();
        }

        //Rendu shader
        Shader.SetGlobalFloat("RadiusFlashlight", cylinder.transform.localScale.x);
        Shader.SetGlobalVector("FlashlightPos", hitPos);

        /*Shader.SetGlobalFloatArray("EntranceSpheresRadiusArray", );*/
    }

    private void FixedUpdate()
    {
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.right), out hit, Mathf.Infinity, layerMaskRaycast))
        {
            Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.right) * hit.distance, Color.yellow);

            if (hit.transform != cylinder.transform)
            {
                hitPos = hit.point;

                if (Vector3.Distance(transform.position, hitPos) <= range)
                {
                    hitPos = hit.point;
                    //cylinder.SetActive(true);
                }
                else
                {
                    //cylinder.SetActive(false);
                }
            }
        }

        cylinder.transform.position = hitPos;
        cylinder.transform.localScale = new Vector3(Mathf.Sin(spotAngle * Mathf.Deg2Rad) * hit.distance,
                                                    Mathf.Sin(spotAngle * Mathf.Deg2Rad) * hit.distance,
                                                    Mathf.Sin(spotAngle * Mathf.Deg2Rad) * hit.distance);

        /*for (int i = 0; i < spheresFlashlight.Length; i++)
        {
            if (i == 0)
            {
                spheresFlashlight[0].transform.position = hitPos;
                spheresFlashlight[0].transform.localScale = new Vector3(Mathf.Sin(spotAngle * Mathf.Deg2Rad) * hit.distance,
                                                                        Mathf.Sin(spotAngle * Mathf.Deg2Rad) * hit.distance,
                                                                        Mathf.Sin(spotAngle * Mathf.Deg2Rad) * hit.distance);
            }
            else if (i == 5)
            {
                spheresFlashlight[5].transform.position = WhitelightObject.transform.position;

                spheresFlashlight[5].transform.localScale = new Vector3(Mathf.Sin(spotAngle * Mathf.Deg2Rad) * hit.distance / i / i,
                                                                        Mathf.Sin(spotAngle * Mathf.Deg2Rad) * hit.distance / i / i,
                                                                        Mathf.Sin(spotAngle * Mathf.Deg2Rad) * hit.distance / i / i);
            }
            else
            {
                spheresFlashlight[i].transform.position = hitPos - transform.TransformDirection(Vector3.right);

                spheresFlashlight[i].transform.localScale = new Vector3(Mathf.Sin(spotAngle * Mathf.Deg2Rad) * hit.distance / i / i,
                                                                        Mathf.Sin(spotAngle * Mathf.Deg2Rad) * hit.distance / i / i,
                                                                        Mathf.Sin(spotAngle * Mathf.Deg2Rad) * hit.distance / i / i);
            }
        }*/
    }

    private void CreateSpheres()
    {
        spheresFlashlight = new GameObject[6];

        for (int i = 0; i < 6; i++)
        {
            spheresFlashlight[i] = GameObject.CreatePrimitive(PrimitiveType.Sphere);

            spheresFlashlight[i].layer = 14;
            spheresFlashlight[i].GetComponent<MeshRenderer>().enabled = false;
            spheresFlashlight[i].GetComponent<SphereCollider>().isTrigger = true;

            S_CylinderFlashlight cylinderFlashlight = spheresFlashlight[i].AddComponent<S_CylinderFlashlight>();
            cylinderFlashlight.sphereIndex = i;

            Rigidbody rbSphere = spheresFlashlight[i].AddComponent<Rigidbody>();
            rbSphere.useGravity = false;

            spheresFlashlight[i].name = "FlashlightSphere" + i;
        }
    }

    public void UvFlashlight()
    {
        if (UVlightObject.activeInHierarchy)
        {
            UVlightObject.SetActive(false);
            WhitelightObject.SetActive(true);
        }
        else
        {
            UVlightObject.SetActive(true);
            WhitelightObject.SetActive(false);
        }
    }
}
