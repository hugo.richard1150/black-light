﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class S_SphereLight : MonoBehaviour
{
    public bool whitelight;

    public void OnTriggerEnter(Collider other)
    {
        if (whitelight && other.gameObject.GetComponent<S_ObjectBlacklight>())
        {
            other.gameObject.GetComponent<S_ObjectBlacklight>().whiteLightCollide = true;
            other.gameObject.GetComponent<S_ObjectBlacklight>().lampWhitelightDetection.Add(gameObject);
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (other.gameObject.GetComponent<S_ObjectBlacklight>())
        {
            other.gameObject.GetComponent<S_ObjectBlacklight>().whiteLightCollide = false;
            other.gameObject.GetComponent<S_ObjectBlacklight>().lampWhitelightDetection.Remove(gameObject);
            Debug.Log(other.gameObject.name);
        }
    }

    /*public void OnCollisionEnter(Collision collision)
    {
        if (whitelight && collision.gameObject.GetComponent<S_ObjectBlacklight>())
        {
            collision.gameObject.GetComponent<S_ObjectBlacklight>().whiteLightCollide = true;
            Debug.Log(collision.gameObject.name);
        }
    }*/

    /*public void OnTriggerExit(Collider other)
    {
        if (!whitelight && other.gameObject.GetComponent<S_ObjectBlacklight>())
        {
            other.gameObject.GetComponent<S_ObjectBlacklight>().whiteLightCollide = false;
        }
    }*/
}
