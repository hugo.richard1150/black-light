﻿using UnityEngine;
using CharacterController_1Person;

public class CameraController : MonoBehaviour
{
    public Transform playerHead;
    public InputManager inputManager;
    public float rotationX = 0;
    public float rotationY = 0;

    public float angleYmin = -80;
    public float angleYmax = 80;

    public float sensitivity;

    public float smoothRotX;
    public float smoothRotY;

    public float smoothCoefX = 0.005f;
    public float smoothCoefY = 0.005f;

    private void LateUpdate()
    {
        transform.position = playerHead.position;
    }

    public void AdjustSensitivity(float newSensitivity)
    {
        sensitivity = newSensitivity;
    }

    void Update()
    {
        Vector2 deltaInput = inputManager.GetMouse() * sensitivity * Time.deltaTime;

        //Smoothing de l'arriver de la camera sur le point de vue du joueur
        smoothRotX = Mathf.Lerp(smoothRotX, deltaInput.x, smoothCoefX);
        smoothRotY = Mathf.Lerp(smoothRotY, deltaInput.y, smoothCoefY);

        rotationX += smoothRotX;
        rotationY += smoothRotY;

        rotationY = Mathf.Clamp(rotationY, angleYmin, angleYmax);

        //Rotation du joueur
        transform.localEulerAngles = new Vector3(-rotationY, rotationX, 0f);
    }
}
