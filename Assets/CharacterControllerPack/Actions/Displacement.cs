﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CharacterController_1Person;

public class Displacement : MonoBehaviour
{
    InputManager inputManager;
    public Transform[] crans;
    public int actuelCran;

    public void Awake()
    {
        inputManager = GameObject.Find("CharacterController (1Person)").GetComponent<InputManager>();
        actuelCran = 0;
    }

    public void Update()
    {
        if (inputManager.GetJump())
        {
            if(actuelCran < 3)
            {
                actuelCran += 1;
            }
        }

        if (inputManager.GetActionShort())
        {
            if(actuelCran > -3)
            {
                actuelCran -= 1;
            }
        }

        for (int i = 0; i < crans.Length; i++)
        {
            if (actuelCran == int.Parse(crans[i].name))
            {
                transform.position = new Vector3(transform.position.x, transform.position.y, crans[i].transform.position.z);
            }
        }
    }
}
