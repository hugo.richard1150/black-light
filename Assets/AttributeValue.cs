﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AttributeValue : MonoBehaviour
{
    public Toggle toggle;
    public Slider sliderSensiCam;
    public Slider sliderSoundMaster;
    public Slider sliderSoundMusic;
    public Slider sliderSoundEffect;
    public Slider sliderSoundNarrator;

    public void Start(){
        toggle.isOn = S_Menu.headBobbing;
        sliderSensiCam.value = S_Menu.sensitivityCam;
        sliderSoundMaster.value = S_Menu.soundMaster;
        sliderSoundMusic.value = S_Menu.soundMusic;
        sliderSoundEffect.value = S_Menu.soundEffect;
        sliderSoundNarrator.value = S_Menu.soundNarrator;
    }
}
