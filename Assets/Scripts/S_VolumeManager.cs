﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class S_VolumeManager : MonoBehaviour
{
    FMOD.Studio.VCA Music;
    FMOD.Studio.VCA SFX;
    FMOD.Studio.VCA Master;
    FMOD.Studio.VCA Narrator;
    FMOD.Studio.EventInstance SFXVolumeTest;

    private Slider slider;
    void Awake()
    {
        Master = FMODUnity.RuntimeManager.GetVCA("vca:/Master");
        Music = FMODUnity.RuntimeManager.GetVCA("vca:/Music");
        SFX = FMODUnity.RuntimeManager.GetVCA("vca:/SFX");
        Narrator = FMODUnity.RuntimeManager.GetVCA("vca:/Narrator");
        SFXVolumeTest = FMODUnity.RuntimeManager.CreateInstance("event:/bellTest");

        slider = GetComponent<Slider>();
        Cursor.visible = enabled;
    }


    public void SetVolumeMaster(float volume)
    {
        Master.setVolume(DecibelToLinear(volume));
    }
    public void SetVolumeMusic(float volume)
    {
        Music.setVolume(DecibelToLinear(volume));
    }

    public void SetVolumeSFX(float volume)
    {
        SFX.setVolume(DecibelToLinear(volume));

        FMOD.Studio.PLAYBACK_STATE pbState;
        SFXVolumeTest.getPlaybackState(out pbState);
        if(pbState != FMOD.Studio.PLAYBACK_STATE.PLAYING)
        {
            SFXVolumeTest.start();
        }
    }
    public void SetVolumeNarrator(float volume)
    {
        Narrator.setVolume(DecibelToLinear(volume));
    }
    private float DecibelToLinear(float dB)
    {
        float linear = Mathf.Pow(10.0f, dB / 20f);
        return linear;
    }
}