﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace CharacterController_1Person
{
    public class Interactable : MonoBehaviour, IInteractable, IUserInterface
    {
        [Space]
        //Interface : IInteractable
        [Header("Interaction Settings")]
        public float holdDuration;
        public int task;
        public int id;
        public bool holdInteract;
        public bool isInteractable;
        public bool condition;
        public float HoldDuration => holdDuration;
        public int Task => task;
        public int ID => id;
        public bool HoldInteract => holdInteract;
        public bool IsInteractable => isInteractable;
        public bool Condition => condition;

        [Space]
        //Interface : IUserInterface
        [Header("UserInterface Settings")]
        public TextMeshProUGUI messageText;
        public Image progressBar;
        public string validationMessage = "Utiliser";
        public string confirmationMessage = "Inutilisable";
        public TextMeshProUGUI MessageText => messageText;
        public Image ProgressBar => progressBar;
        public string ValidationMessage => validationMessage;
        public string ConfirmationMessage => confirmationMessage;

        public virtual void OnInteract()
        {
            Debug.Log("Interaction réussie !");
        }
    }
}
