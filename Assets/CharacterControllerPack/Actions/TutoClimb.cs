﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TutoClimb : MonoBehaviour
{
    public TextMeshProUGUI tutoText;
    public GameObject tutoPanel;
    public string tutoMessage;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            tutoPanel.SetActive(true);
            SetTutoText(tutoMessage);
        }
    }

    void SetTutoText(string message)
    {
        if (tutoPanel.activeSelf)
        {
            tutoText.SetText(message);
        }
        else
        {
            tutoText.SetText("");
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            tutoPanel.gameObject.SetActive(false);
            this.gameObject.GetComponent<BoxCollider>().enabled = false;
        }
    }
}
