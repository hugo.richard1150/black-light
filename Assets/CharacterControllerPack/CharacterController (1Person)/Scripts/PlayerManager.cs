﻿using UnityEngine;

namespace CharacterController_1Person
{
    public class PlayerManager : MonoBehaviour
    {
        private PlayerLocomotion playerLocomotion;

        [HideInInspector] public int item = 0;

        private void Awake()
        {
            //Récupération des valeurs
            playerLocomotion = GetComponent<PlayerLocomotion>();
        }
        private void Update()
        {
            //Fonction (HandleAllAction) du script PlayerLocomotion
            playerLocomotion.HandleAllActions();
        }
        private void FixedUpdate()
        {
            //Fonction (HandleAllMovement) du script PlayerLocomotion
            playerLocomotion.HandleAllMovements();
        }
    }
}
