﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using CharacterController_1Person;

public class Debug_Cheats : MonoBehaviour
{
    public Controls controls;
    private S_MusicManager music;
    public Transform floor1Position;
    public Transform floor2Position;
    public Transform floor3Position;
    public Transform endingPosition;
    private GameObject player;
    public GameObject block204;
    public GameObject door204;
    public Open[] doors;

    void Awake()
    {
        controls = new Controls();
    }

    private void OnEnable()
    {
        controls.Enable();
    }

    private void OnDisable()
    {
        controls.Disable();
    }

    void Start()
    {
        controls.Cheats.Teleport1stFloor.performed += _ => Teleport1stFloor();
        controls.Cheats.Teleport2ndFloor.performed += _ => Teleport2ndFloor();
        controls.Cheats.Teleport3rdFloor.performed += _ => Teleport3rdFloor();
        controls.Cheats.Ending.performed += _ => TeleportToEnding();
        controls.Cheats.ResetGame.performed += _ => ResetGame();
        controls.Cheats.Skip204.performed += _ => Skip204();
        controls.Cheats.OpenDoors.performed += _ => OpenDoors();

        music = GameObject.Find("MusicManager").GetComponent<S_MusicManager>();
        player = GameObject.FindGameObjectWithTag("Player");
    }

    public void Teleport1stFloor()
    {
        Debug.Log("TP 1st Floor");
        player.GetComponent<CharacterController>().enabled = false;
        player.transform.position = floor1Position.position;
        player.GetComponent<CharacterController>().enabled = true;
    }

    public void Teleport2ndFloor()
    {
        Debug.Log("TP 2nd Floor");
        player.GetComponent<CharacterController>().enabled = false;
        player.transform.position = floor2Position.position;
        player.GetComponent<CharacterController>().enabled = true;
    }

    public void Teleport3rdFloor()
    {
        Debug.Log("TP 3rd Floor");
        player.GetComponent<CharacterController>().enabled = false;
        player.transform.position = floor3Position.position;
        music.ThirdFloorMusic();
        player.GetComponent<CharacterController>().enabled = true;
    }

    public void TeleportToEnding()
    {
        Debug.Log("TP Ending");
        player.GetComponent<CharacterController>().enabled = false;
        player.transform.position = endingPosition.position;
        player.GetComponent<CharacterController>().enabled = true;
    }

    public void ResetGame()
    {
        SceneManager.LoadScene(0);
    }

    public void Skip204()
    {
        block204.SetActive(false);
        door204.SetActive(false);
    }

    public void OpenDoors()
    {
        Skip204();
        foreach (var door in doors)
        {
            door.DoorOpen();
        }
    }
}
