﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectionFloor : MonoBehaviour
{

    public float hauteur;
    public GameObject[] rdc;
    public GameObject[] firstFloor;
    public GameObject[] secondFloor;
    public GameObject[] thirdFloor;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        hauteur = gameObject.transform.position.y;
        if (gameObject.transform.position.y > -1.5f && gameObject.transform.position.y < 1.2f)
        {
            Debug.Log("RDC");
            foreach (GameObject gameObject in rdc)
            {
                gameObject.SetActive(true);
            }
            foreach (GameObject gameObject in firstFloor)
            {
                gameObject.SetActive(false);
            }
            foreach (GameObject gameObject in secondFloor)
            {
                gameObject.SetActive(false);
            }
            foreach (GameObject gameObject in thirdFloor)
            {
                gameObject.SetActive(false);
            }
        }
        else if (gameObject.transform.position.y > 1.5f && gameObject.transform.position.y < 2.3f)
        {
            Debug.Log("1erEtage");
            foreach (GameObject gameObject in rdc)
            {
                gameObject.SetActive(false);
            }
            foreach (GameObject gameObject in firstFloor)
            {
                gameObject.SetActive(true);
            }
            foreach (GameObject gameObject in secondFloor)
            {
                gameObject.SetActive(false);
            }
            foreach (GameObject gameObject in thirdFloor)
            {
                gameObject.SetActive(false);
            }
            
        }
        else if (gameObject.transform.position.y > 4.6f && gameObject.transform.position.y < 5.3f)
        {
            Debug.Log("2emeEtage");
            foreach (GameObject gameObject in rdc)
            {
                gameObject.SetActive(false);
            }
            foreach (GameObject gameObject in firstFloor)
            {
                gameObject.SetActive(false);
            }
            foreach (GameObject gameObject in secondFloor)
            {
                gameObject.SetActive(true);
            }
            foreach (GameObject gameObject in thirdFloor)
            {
                gameObject.SetActive(false);
            }
        }
        else if(gameObject.transform.position.y > 5.5f)
        {
            Debug.Log("3emeEtage");
            foreach (GameObject gameObject in rdc)
            {
                gameObject.SetActive(false);
            }
            foreach (GameObject gameObject in firstFloor)
            {
                gameObject.SetActive(false);
            }
            foreach (GameObject gameObject in secondFloor)
            {
                gameObject.SetActive(false);
            }
            foreach (GameObject gameObject in thirdFloor)
            {
                gameObject.SetActive(true);
            }
        }
    }
}
