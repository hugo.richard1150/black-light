﻿public interface IInteractable
{
    int Task { get; }
    float HoldDuration { get; }

    int ID { get; }

    bool HoldInteract { get; }

    bool IsInteractable { get; }

    bool Condition { get; }

    void OnInteract();
}
