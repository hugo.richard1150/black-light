// Upgrade NOTE: upgraded instancing buffer 'shader_CelShading3' to new syntax.

// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "shader_CelShading3"
{
	Properties
	{
		_Tint("Tint", Color) = (1,1,1,0)
		_DistanceLight("DistanceLight", Range( 0 , 20)) = 10
		_ColorWhiteLight("ColorWhiteLight", Color) = (1,0.4662507,0.2877358,0)
		_PowerColor("PowerColor", Range( 0 , 10)) = 0
		_Float0("Float 0", Range( 0 , 10)) = 0
		_TextureSample0("Texture Sample 0", 2D) = "white" {}
		_ColorBlackLight("ColorBlackLight", Color) = (1,0,0.3363633,0)
		_BlacklightPower("BlacklightPower", Range( -1 , 5)) = 0
		_Brightness("Brightness", Range( 1 , 10)) = 1
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" }
		Cull Back
		CGINCLUDE
		#include "UnityPBSLighting.cginc"
		#include "UnityShaderVariables.cginc"
		#include "Lighting.cginc"
		#pragma target 3.0
		#pragma multi_compile_instancing
		#ifdef UNITY_PASS_SHADOWCASTER
			#undef INTERNAL_DATA
			#undef WorldReflectionVector
			#undef WorldNormalVector
			#define INTERNAL_DATA half3 internalSurfaceTtoW0; half3 internalSurfaceTtoW1; half3 internalSurfaceTtoW2;
			#define WorldReflectionVector(data,normal) reflect (data.worldRefl, half3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal)))
			#define WorldNormalVector(data,normal) half3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal))
		#endif
		struct Input
		{
			float2 uv_texcoord;
			float3 worldNormal;
			INTERNAL_DATA
			float3 worldPos;
		};

		struct SurfaceOutputCustomLightingCustom
		{
			half3 Albedo;
			half3 Normal;
			half3 Emission;
			half Metallic;
			half Smoothness;
			half Occlusion;
			half Alpha;
			Input SurfInput;
			UnityGIInput GIData;
		};

		uniform float4 _Tint;
		uniform sampler2D _TextureSample0;
		uniform float _BlacklightPower;
		uniform float _Float0;
		uniform float _DistanceLight;
		uniform float4 _ColorWhiteLight;
		uniform float _PowerColor;
		uniform float4 EntrancePositionsArray[30];
		uniform float _Brightness;

		UNITY_INSTANCING_BUFFER_START(shader_CelShading3)
			UNITY_DEFINE_INSTANCED_PROP(float4, _TextureSample0_ST)
#define _TextureSample0_ST_arr shader_CelShading3
			UNITY_DEFINE_INSTANCED_PROP(float4, _ColorBlackLight)
#define _ColorBlackLight_arr shader_CelShading3
		UNITY_INSTANCING_BUFFER_END(shader_CelShading3)


		float LoopArrayPositions218( float3 WorldPos, float3 ObjectPos )
		{
			float closest=10000;
			float now=0;
			for(int i=0; i<EntrancePositionsArray.Length;i++){
				now = distance(WorldPos,EntrancePositionsArray[i]);
				if(now < closest){
				closest = now;
				}
			}
			return closest;
		}


		inline half4 LightingStandardCustomLighting( inout SurfaceOutputCustomLightingCustom s, half3 viewDir, UnityGI gi )
		{
			UnityGIInput data = s.GIData;
			Input i = s.SurfInput;
			half4 c = 0;
			#ifdef UNITY_PASS_FORWARDBASE
			float ase_lightAtten = data.atten;
			if( _LightColor0.a == 0)
			ase_lightAtten = 0;
			#else
			float3 ase_lightAttenRGB = gi.light.color / ( ( _LightColor0.rgb ) + 0.000001 );
			float ase_lightAtten = max( max( ase_lightAttenRGB.r, ase_lightAttenRGB.g ), ase_lightAttenRGB.b );
			#endif
			#if defined(HANDLE_SHADOWS_BLENDING_IN_GI)
			half bakedAtten = UnitySampleBakedOcclusion(data.lightmapUV.xy, data.worldPos);
			float zDist = dot(_WorldSpaceCameraPos - data.worldPos, UNITY_MATRIX_V[2].xyz);
			float fadeDist = UnityComputeShadowFadeDistance(data.worldPos, zDist);
			ase_lightAtten = UnityMixRealtimeAndBakedShadows(data.atten, bakedAtten, UnityComputeShadowFade(fadeDist));
			#endif
			#if defined(LIGHTMAP_ON) && ( UNITY_VERSION < 560 || ( defined(LIGHTMAP_SHADOW_MIXING) && !defined(SHADOWS_SHADOWMASK) && defined(SHADOWS_SCREEN) ) )//aselc
			float4 ase_lightColor = 0;
			#else //aselc
			float4 ase_lightColor = _LightColor0;
			#endif //aselc
			float4 normalizeResult342 = normalize( ase_lightColor );
			float4 color333 = IsGammaSpace() ? float4(0.3882353,0,0.9607843,0) : float4(0.1247718,0,0.9130987,0);
			float4 normalizeResult341 = normalize( color333 );
			float dotResult350 = dot( normalizeResult342 , normalizeResult341 );
			float LightsStates319 = dotResult350;
			float4 _TextureSample0_ST_Instance = UNITY_ACCESS_INSTANCED_PROP(_TextureSample0_ST_arr, _TextureSample0_ST);
			float2 uv_TextureSample0 = i.uv_texcoord * _TextureSample0_ST_Instance.xy + _TextureSample0_ST_Instance.zw;
			float4 Albedo146 = ( _Tint * tex2D( _TextureSample0, uv_TextureSample0 ) );
			float4 color770 = IsGammaSpace() ? float4(0.8235294,0.5294118,0.945098,0) : float4(0.6444798,0.2422812,0.8796223,0);
			float4 color769 = IsGammaSpace() ? float4(0.4980392,0,0.7058824,0) : float4(0.2122307,0,0.4564111,0);
			float4 blendOpSrc772 = color770;
			float4 blendOpDest772 = color769;
			float4 lerpBlendMode772 = lerp(blendOpDest772,( blendOpSrc772 + blendOpDest772 ),0.05);
			float4 temp_output_772_0 = lerpBlendMode772;
			float4 color798 = IsGammaSpace() ? float4(0.522742,0,0.735849,0) : float4(0.2356978,0,0.5007474,0);
			float4 blendOpSrc806 = temp_output_772_0;
			float4 blendOpDest806 = color798;
			float4 _ColorBlackLight_Instance = UNITY_ACCESS_INSTANCED_PROP(_ColorBlackLight_arr, _ColorBlackLight);
			float4 BlackLighting788 = ( ( saturate( ( ( Albedo146 + temp_output_772_0 ) * ( saturate( min( blendOpSrc806 , blendOpDest806 ) )) ) ) * _BlacklightPower ) + ( pow( ase_lightAtten , 10.0 ) * ( _ColorBlackLight_Instance * _Float0 ) ) );
			float3 ase_worldNormal = WorldNormalVector( i, float3( 0, 0, 1 ) );
			UnityGI gi805 = gi;
			float3 diffNorm805 = ase_worldNormal;
			gi805 = UnityGI_Base( data, 1, diffNorm805 );
			float3 indirectDiffuse805 = gi805.indirect.diffuse + diffNorm805 * 0.0001;
			float4 lighting152 = ( Albedo146 * ( ( float4( ( indirectDiffuse805 + pow( ase_lightAtten , 0.1 ) ) , 0.0 ) * ase_lightColor ) + ( pow( ase_lightAtten , _DistanceLight ) * ( _ColorWhiteLight * _PowerColor ) ) ) );
			float3 ase_worldPos = i.worldPos;
			float3 WorldPos218 = ase_worldPos;
			float3 ObjectPos218 = EntrancePositionsArray[0].xyz;
			float localLoopArrayPositions218 = LoopArrayPositions218( WorldPos218 , ObjectPos218 );
			float PositionsLoop562 = pow( localLoopArrayPositions218 , ase_lightAtten );
			float4 lerpResult275 = lerp( ( LightsStates319 == 1.0 ? BlackLighting788 : lighting152 ) , lighting152 , PositionsLoop562);
			c.rgb = ( lerpResult275 * _Brightness ).rgb;
			c.a = 1;
			return c;
		}

		inline void LightingStandardCustomLighting_GI( inout SurfaceOutputCustomLightingCustom s, UnityGIInput data, inout UnityGI gi )
		{
			s.GIData = data;
		}

		void surf( Input i , inout SurfaceOutputCustomLightingCustom o )
		{
			o.SurfInput = i;
			o.Normal = float3(0,0,1);
		}

		ENDCG
		CGPROGRAM
		#pragma surface surf StandardCustomLighting keepalpha fullforwardshadows 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			#include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float2 customPack1 : TEXCOORD1;
				float4 tSpace0 : TEXCOORD2;
				float4 tSpace1 : TEXCOORD3;
				float4 tSpace2 : TEXCOORD4;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO( o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				Input customInputData;
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				half3 worldNormal = UnityObjectToWorldNormal( v.normal );
				half3 worldTangent = UnityObjectToWorldDir( v.tangent.xyz );
				half tangentSign = v.tangent.w * unity_WorldTransformParams.w;
				half3 worldBinormal = cross( worldNormal, worldTangent ) * tangentSign;
				o.tSpace0 = float4( worldTangent.x, worldBinormal.x, worldNormal.x, worldPos.x );
				o.tSpace1 = float4( worldTangent.y, worldBinormal.y, worldNormal.y, worldPos.y );
				o.tSpace2 = float4( worldTangent.z, worldBinormal.z, worldNormal.z, worldPos.z );
				o.customPack1.xy = customInputData.uv_texcoord;
				o.customPack1.xy = v.texcoord;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				return o;
			}
			half4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				surfIN.uv_texcoord = IN.customPack1.xy;
				float3 worldPos = float3( IN.tSpace0.w, IN.tSpace1.w, IN.tSpace2.w );
				half3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				surfIN.worldPos = worldPos;
				surfIN.worldNormal = float3( IN.tSpace0.z, IN.tSpace1.z, IN.tSpace2.z );
				surfIN.internalSurfaceTtoW0 = IN.tSpace0.xyz;
				surfIN.internalSurfaceTtoW1 = IN.tSpace1.xyz;
				surfIN.internalSurfaceTtoW2 = IN.tSpace2.xyz;
				SurfaceOutputCustomLightingCustom o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutputCustomLightingCustom, o )
				surf( surfIN, o );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=18900
1004;280;908;549;-4616.417;-1406.717;1;True;False
Node;AmplifyShaderEditor.CommentaryNode;204;1724.074,1397.366;Inherit;False;828.3352;474.0365;Albedo;4;145;143;144;146;;1,1,1,1;0;0
Node;AmplifyShaderEditor.ColorNode;144;1841.609,1447.366;Inherit;False;Property;_Tint;Tint;0;0;Create;True;0;0;0;False;0;False;1,1,1,0;0.8584906,0.8584906,0.8584906,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;143;1778.592,1641.402;Inherit;True;Property;_TextureSample0;Texture Sample 0;5;0;Create;True;0;0;0;False;0;False;-1;None;df9bf997dd797704b9d0643012b9578f;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.CommentaryNode;768;3157.183,1977.833;Inherit;False;2480.969;1508.055;Black Lighting;15;790;788;774;772;771;770;769;787;798;806;813;817;818;819;820;Black Lighting;1,1,1,1;0;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;145;2096.946,1543.918;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;769;3571.844,2467.173;Inherit;False;Constant;_Color2;Color 2;8;0;Create;True;0;0;0;False;0;False;0.4980392,0,0.7058824,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.CommentaryNode;529;1251.229,2014.249;Inherit;False;1794.299;1372.021;Comment;11;444;149;150;618;488;426;152;151;661;705;805;Lighting;1,1,1,1;0;0
Node;AmplifyShaderEditor.ColorNode;770;3579.322,2264.617;Inherit;False;Constant;_Color1;Color 1;8;0;Create;True;0;0;0;False;0;False;0.8235294,0.5294118,0.945098,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RegisterLocalVarNode;146;2301.148,1540.507;Float;False;Albedo;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.CommentaryNode;661;1423.608,2552.333;Inherit;False;1011.394;439.1484;Petite Light;6;647;627;624;645;628;623;;0.9811321,0.6390607,0.2915628,1;0;0
Node;AmplifyShaderEditor.LightAttenuation;426;1488.835,2184.43;Inherit;False;0;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;798;3823.21,2492.229;Inherit;False;Constant;_Color3;Color 3;8;0;Create;True;0;0;0;False;0;False;0.522742,0,0.735849,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GetLocalVarNode;790;3937.312,2126.713;Inherit;False;146;Albedo;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.BlendOpsNode;772;3821.561,2265.092;Inherit;True;LinearDodge;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0.05;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;628;1785.161,2701.03;Inherit;False;Property;_ColorWhiteLight;ColorWhiteLight;2;0;Create;True;0;0;0;False;0;False;1,0.4662507,0.2877358,0;1,0.4662507,0.2877356,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;623;1473.608,2617.412;Inherit;False;Property;_DistanceLight;DistanceLight;1;0;Create;True;0;0;0;False;0;False;10;12;0;20;0;1;FLOAT;0
Node;AmplifyShaderEditor.PowerNode;488;1881.729,2207.753;Inherit;False;False;2;0;FLOAT;0;False;1;FLOAT;0.1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;647;1726.527,2874.563;Inherit;False;Property;_PowerColor;PowerColor;3;0;Create;True;0;0;0;False;0;False;0;1;0;10;0;1;FLOAT;0
Node;AmplifyShaderEditor.BlendOpsNode;806;4127.157,2396.708;Inherit;True;Darken;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;1;False;1;COLOR;0
Node;AmplifyShaderEditor.IndirectDiffuseLighting;805;1633.538,2046.175;Inherit;False;Tangent;1;0;FLOAT3;0,0,1;False;1;FLOAT3;0
Node;AmplifyShaderEditor.CommentaryNode;774;3314.715,2680.313;Inherit;False;1011.394;439.1484;Petite Light;6;785;783;781;779;807;808;;0.9803922,0.2901962,0.7915436,1;0;0
Node;AmplifyShaderEditor.SimpleAddOpNode;818;4115.738,2224.509;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.PowerNode;624;1837.969,2602.333;Inherit;False;False;2;0;FLOAT;0;False;1;FLOAT;0.1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;645;2025.153,2706.752;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.LightColorNode;150;1883.952,2411.234;Inherit;False;0;3;COLOR;0;FLOAT3;1;FLOAT;2
Node;AmplifyShaderEditor.SimpleAddOpNode;618;2057.812,2137.075;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;813;4368.251,2263.785;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;781;3454.336,2760.445;Inherit;False;Constant;_Float2;Float 2;10;0;Create;True;0;0;0;False;0;False;10;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;318;3656.286,3567.101;Inherit;False;728.8997;365.3354;Lights states;6;331;333;341;342;350;319;Lights States;1,1,1,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;807;3648.058,3010.377;Inherit;False;Property;_Float0;Float 0;4;0;Create;True;0;0;0;False;0;False;0;2.4;0;10;0;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;779;3644.259,2828.093;Inherit;False;InstancedProperty;_ColorBlackLight;ColorBlackLight;6;0;Create;True;0;0;0;False;0;False;1,0,0.3363633,0;0.3396225,0.1778212,0.3331509,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;820;4526.799,2217.84;Inherit;False;Property;_BlacklightPower;BlacklightPower;7;0;Create;True;0;0;0;False;0;False;0;0.7;-1;5;0;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;817;4518.059,2123.467;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;444;2178.336,2272.59;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.PowerNode;783;3729.077,2730.313;Inherit;False;False;2;0;FLOAT;0;False;1;FLOAT;0.1;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;333;3672.945,3756.44;Inherit;False;Constant;_ColorBlacklightState;Color Blacklight State;8;0;Create;True;0;0;0;False;0;False;0.3882353,0,0.9607843,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LightColorNode;331;3736.232,3638.377;Inherit;False;0;3;COLOR;0;FLOAT3;1;FLOAT;2
Node;AmplifyShaderEditor.CommentaryNode;311;2171.705,3575.251;Inherit;False;1439.415;353.8765;Positions lights loop;7;562;416;408;218;216;215;801;Lights Positions Loop;1,1,1,1;0;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;808;3968.486,2842.232;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;627;2272.603,2604.463;Inherit;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WorldPosInputsNode;215;2238.399,3632.441;Inherit;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SimpleAddOpNode;705;2492.596,2242.627;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;819;4750.698,2063.14;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;149;2337.886,2055.707;Inherit;False;146;Albedo;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.NormalizeNode;341;3896.237,3758.112;Inherit;False;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GlobalArrayNode;216;2192.613,3781.208;Inherit;False;EntrancePositionsArray;0;30;2;False;False;0;1;False;Object;147;4;0;INT;0;False;2;INT;0;False;1;INT;0;False;3;INT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;785;4170.358,2806.98;Inherit;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.NormalizeNode;342;3893.592,3637.924;Inherit;False;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.DotProductOpNode;350;4056.266,3684.386;Inherit;False;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CustomExpressionNode;218;2455.831,3686.304;Float;False;float closest=10000@$float now=0@$for(int i=0@ i<EntrancePositionsArray.Length@i++){$	now = distance(WorldPos,EntrancePositionsArray[i])@$	if(now < closest){$	closest = now@$	}$}$return closest@;1;False;2;True;WorldPos;FLOAT3;0,0,0;In;;Inherit;False;True;ObjectPos;FLOAT3;0,0,0;In;;Inherit;False;Loop Array Positions;True;False;0;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;787;4932.237,2066.27;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.LightAttenuation;408;2816.131,3768.933;Inherit;False;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;151;2610.692,2060.942;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;152;2795.8,2063.154;Inherit;False;lighting;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.PowerNode;416;3034.513,3691.274;Inherit;False;False;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;319;4177.604,3680.009;Float;False;LightsStates;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;788;5442.43,2064.844;Inherit;False;BlackLighting;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;139;4229.902,1724.037;Inherit;False;152;lighting;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;446;4257.278,1557.134;Inherit;False;Constant;_Float1;Float 1;8;0;Create;True;0;0;0;False;0;False;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;564;4206.589,1634.711;Inherit;False;788;BlackLighting;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;562;3418.801,3684.217;Inherit;False;PositionsLoop;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;563;4208.072,1491.718;Inherit;False;319;LightsStates;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;803;4229.902,1724.037;Inherit;False;152;lighting;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;313;4694.05,1721.068;Inherit;False;562;PositionsLoop;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.Compare;449;4460.106,1506.832;Inherit;False;0;4;0;FLOAT;0;False;1;FLOAT;0;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;275;4971.529,1600.799;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;1;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;838;4957.417,1747.717;Inherit;False;Property;_Brightness;Brightness;8;0;Create;True;0;0;0;False;0;False;1;1;1;10;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;802;4257.278,1557.134;Inherit;False;Constant;_Float4;Float 4;8;0;Create;True;0;0;0;False;0;False;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ComponentMaskNode;834;4812.537,1183.147;Inherit;False;True;True;True;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;801;3418.801,3684.217;Inherit;False;PositionsLoop;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;771;3823.21,2492.229;Inherit;False;Constant;_Color0;Color 0;8;0;Create;True;0;0;0;False;0;False;0.522742,0,0.735849,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;836;5189.417,1651.717;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;804;4208.072,1491.718;Inherit;False;319;LightsStates;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;835;4586.682,1254.267;Inherit;False;788;BlackLighting;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;214;5381.313,1421.803;Float;False;True;-1;2;ASEMaterialInspector;0;0;CustomLighting;shader_CelShading3;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Opaque;0.5;True;True;0;False;Opaque;;Geometry;All;14;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;1;False;-1;3;False;-1;0;0;False;-1;0;False;-1;2;False;-1;1;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;False;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;4;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;145;0;144;0
WireConnection;145;1;143;0
WireConnection;146;0;145;0
WireConnection;772;0;770;0
WireConnection;772;1;769;0
WireConnection;488;0;426;0
WireConnection;806;0;772;0
WireConnection;806;1;798;0
WireConnection;818;0;790;0
WireConnection;818;1;772;0
WireConnection;624;0;426;0
WireConnection;624;1;623;0
WireConnection;645;0;628;0
WireConnection;645;1;647;0
WireConnection;618;0;805;0
WireConnection;618;1;488;0
WireConnection;813;0;818;0
WireConnection;813;1;806;0
WireConnection;817;0;813;0
WireConnection;444;0;618;0
WireConnection;444;1;150;0
WireConnection;783;0;426;0
WireConnection;783;1;781;0
WireConnection;808;0;779;0
WireConnection;808;1;807;0
WireConnection;627;0;624;0
WireConnection;627;1;645;0
WireConnection;705;0;444;0
WireConnection;705;1;627;0
WireConnection;819;0;817;0
WireConnection;819;1;820;0
WireConnection;341;0;333;0
WireConnection;785;0;783;0
WireConnection;785;1;808;0
WireConnection;342;0;331;0
WireConnection;350;0;342;0
WireConnection;350;1;341;0
WireConnection;218;0;215;0
WireConnection;218;1;216;0
WireConnection;787;0;819;0
WireConnection;787;1;785;0
WireConnection;151;0;149;0
WireConnection;151;1;705;0
WireConnection;152;0;151;0
WireConnection;416;0;218;0
WireConnection;416;1;408;0
WireConnection;319;0;350;0
WireConnection;788;0;787;0
WireConnection;562;0;416;0
WireConnection;449;0;563;0
WireConnection;449;1;446;0
WireConnection;449;2;564;0
WireConnection;449;3;139;0
WireConnection;275;0;449;0
WireConnection;275;1;803;0
WireConnection;275;2;313;0
WireConnection;834;0;835;0
WireConnection;836;0;275;0
WireConnection;836;1;838;0
WireConnection;214;13;836;0
ASEEND*/
//CHKSM=D3517BE405AE1F33E8A93B0D11625C7E209DDFD9