﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class S_ObjectBlacklight : MonoBehaviour
{
    public enum BlacklightActive { Appear, Disappear }
    public BlacklightActive blacklightActive;

    public GameObject[] lampsInfluences;
    private GameObject[] sphereLamps;
    //private S_LampBehaviour[] lampBehaviour;

    public bool whiteLightCollide;
    public List<GameObject> lampWhitelightDetection;

    private MeshRenderer meshRenderer;
    private MeshCollider meshCollider;
    private BoxCollider boxCollider;

    private void Start()
    {
        meshRenderer = GetComponent<MeshRenderer>();
        if (gameObject.GetComponent<MeshCollider>())
        {
            meshCollider = GetComponent<MeshCollider>();
        }
        if (gameObject.GetComponent<BoxCollider>())
        {
            boxCollider = GetComponent<BoxCollider>();
        }

        sphereLamps = new GameObject[lampsInfluences.Length];
        //lampBehaviour = new S_LampBehaviour[sphereLamps.Length];

        for (int i = 0; i < lampsInfluences.Length; i++)
        {
            sphereLamps[i] = lampsInfluences[i].gameObject.GetComponent<S_LampBehaviour>().radiusLightCollider;

            //lampBehaviour[i] = sphereLamps[i].GetComponentInParent<S_LampBehaviour>();
        }
    }

    private void Update()
    {
        if (lampWhitelightDetection.Count > 0)
        {
            if (meshCollider != null)
            {
                if (blacklightActive == BlacklightActive.Appear)
                {
                    meshRenderer.enabled = false;
                    meshCollider.isTrigger = true;
                }
                else if (blacklightActive == BlacklightActive.Disappear)
                {
                    meshRenderer.enabled = true;
                    meshCollider.isTrigger = false;
                }
            }
            else if (boxCollider != null)
            {
                if (blacklightActive == BlacklightActive.Appear)
                {
                    meshRenderer.enabled = false;
                    boxCollider.isTrigger = true;
                }
                else if (blacklightActive == BlacklightActive.Disappear)
                {
                    meshRenderer.enabled = true;
                    boxCollider.isTrigger = false;
                }
            }
        }
        else
        {
            if (meshCollider != null)
            {
                if (blacklightActive == BlacklightActive.Appear)
                {
                    meshRenderer.enabled = true;
                    meshCollider.isTrigger = false;
                }
                else if (blacklightActive == BlacklightActive.Disappear)
                {
                    meshRenderer.enabled = false;
                    meshCollider.isTrigger = true;
                }
            }
            else if (boxCollider != null)
            {
                if (blacklightActive == BlacklightActive.Appear)
                {
                    meshRenderer.enabled = true;
                    boxCollider.isTrigger = false;
                }
                else if (blacklightActive == BlacklightActive.Disappear)
                {
                    meshRenderer.enabled = false;
                    boxCollider.isTrigger = true;
                }
            }
        }
    }
}
