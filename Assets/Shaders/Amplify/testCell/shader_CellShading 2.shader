// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "shader_CelShadingV2"
{
	Properties
	{
		_Tint1("Tint", Color) = (0.4339623,0.4339623,0.4339623,0)
		_Distance("Distance", Float) = 4
		_ToonRamp1("ToonRamp", 2D) = "white" {}
		_TextureSample1("Texture Sample 0", 2D) = "white" {}
		_Normal("Normal", 2D) = "bump" {}
		[Toggle(_NEONMATERIAL1_ON)] _NeonMaterial1("NeonMaterial", Float) = 1
		_PoweEmession1("Powe Emession", Range( 0 , 10)) = 0
		[Toggle(_NEONMATERIAL2_ON)] _NeonMaterial2("NeonMaterial", Float) = 1
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" "IsEmissive" = "true"  }
		Cull Back
		CGINCLUDE
		#include "UnityPBSLighting.cginc"
		#include "UnityShaderVariables.cginc"
		#include "UnityCG.cginc"
		#include "Lighting.cginc"
		#pragma target 3.0
		#pragma shader_feature_local _NEONMATERIAL2_ON
		#pragma shader_feature_local _NEONMATERIAL1_ON
		#ifdef UNITY_PASS_SHADOWCASTER
			#undef INTERNAL_DATA
			#undef WorldReflectionVector
			#undef WorldNormalVector
			#define INTERNAL_DATA half3 internalSurfaceTtoW0; half3 internalSurfaceTtoW1; half3 internalSurfaceTtoW2;
			#define WorldReflectionVector(data,normal) reflect (data.worldRefl, half3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal)))
			#define WorldNormalVector(data,normal) half3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal))
		#endif
		struct Input
		{
			float2 uv_texcoord;
			float3 worldNormal;
			INTERNAL_DATA
			float3 worldPos;
		};

		struct SurfaceOutputCustomLightingCustom
		{
			half3 Albedo;
			half3 Normal;
			half3 Emission;
			half Metallic;
			half Smoothness;
			half Occlusion;
			half Alpha;
			Input SurfInput;
			UnityGIInput GIData;
		};

		uniform sampler2D _TextureSample1;
		uniform float4 _TextureSample1_ST;
		uniform float _PoweEmession1;
		uniform float4 _Tint1;
		uniform sampler2D _Normal;
		uniform float4 _Normal_ST;
		uniform sampler2D _ToonRamp1;
		uniform float4 EntrancePositionsArray[30];
		uniform float _Distance;


		float LoopArrayPositions218( float3 WorldPos, float3 ObjectPos )
		{
			float closest=10000;
			float now=0;
			for(int i=0; i<EntrancePositionsArray.Length;i++){
				now = distance(WorldPos,EntrancePositionsArray[i]);
				if(now < closest){
				closest = now;
				}
			}
			return closest;
		}


		inline half4 LightingStandardCustomLighting( inout SurfaceOutputCustomLightingCustom s, half3 viewDir, UnityGI gi )
		{
			UnityGIInput data = s.GIData;
			Input i = s.SurfInput;
			half4 c = 0;
			#ifdef UNITY_PASS_FORWARDBASE
			float ase_lightAtten = data.atten;
			if( _LightColor0.a == 0)
			ase_lightAtten = 0;
			#else
			float3 ase_lightAttenRGB = gi.light.color / ( ( _LightColor0.rgb ) + 0.000001 );
			float ase_lightAtten = max( max( ase_lightAttenRGB.r, ase_lightAttenRGB.g ), ase_lightAttenRGB.b );
			#endif
			#if defined(HANDLE_SHADOWS_BLENDING_IN_GI)
			half bakedAtten = UnitySampleBakedOcclusion(data.lightmapUV.xy, data.worldPos);
			float zDist = dot(_WorldSpaceCameraPos - data.worldPos, UNITY_MATRIX_V[2].xyz);
			float fadeDist = UnityComputeShadowFadeDistance(data.worldPos, zDist);
			ase_lightAtten = UnityMixRealtimeAndBakedShadows(data.atten, bakedAtten, UnityComputeShadowFade(fadeDist));
			#endif
			#if defined(LIGHTMAP_ON) && ( UNITY_VERSION < 560 || ( defined(LIGHTMAP_SHADOW_MIXING) && !defined(SHADOWS_SHADOWMASK) && defined(SHADOWS_SCREEN) ) )//aselc
			float4 ase_lightColor = 0;
			#else //aselc
			float4 ase_lightColor = _LightColor0;
			#endif //aselc
			float4 normalizeResult342 = normalize( ase_lightColor );
			float4 color333 = IsGammaSpace() ? float4(0.7843137,0,1,0) : float4(0.5775805,0,1,0);
			float4 normalizeResult341 = normalize( color333 );
			float dotResult350 = dot( normalizeResult342 , normalizeResult341 );
			float LightsStates319 = dotResult350;
			float2 uv_TextureSample1 = i.uv_texcoord * _TextureSample1_ST.xy + _TextureSample1_ST.zw;
			float4 tex2DNode597 = tex2D( _TextureSample1, uv_TextureSample1 );
			float4 Albedo613 = ( _Tint1 * tex2DNode597 );
			float4 color604 = IsGammaSpace() ? float4(0.8227772,0.5295479,0.9433962,0) : float4(0.6431562,0.2424167,0.8760344,0);
			float4 color606 = IsGammaSpace() ? float4(0.4962019,0,0.7075472,0) : float4(0.2105425,0,0.4588115,0);
			float4 blendOpSrc610 = color604;
			float4 blendOpDest610 = color606;
			float4 temp_output_610_0 = ( saturate(  (( blendOpSrc610 > 0.5 ) ? ( 1.0 - ( 1.0 - 2.0 * ( blendOpSrc610 - 0.5 ) ) * ( 1.0 - blendOpDest610 ) ) : ( 2.0 * blendOpSrc610 * blendOpDest610 ) ) ));
			float4 color609 = IsGammaSpace() ? float4(0.8745098,0.6313726,0.9607843,0) : float4(0.7379106,0.3564003,0.9130987,0);
			float4 blendOpSrc621 = temp_output_610_0;
			float4 blendOpDest621 = color609;
			float2 uv_Normal = i.uv_texcoord * _Normal_ST.xy + _Normal_ST.zw;
			float3 normalMap209 = UnpackNormal( tex2D( _Normal, uv_Normal ) );
			UnityGI gi589 = gi;
			float3 diffNorm589 = WorldNormalVector( i , normalMap209 );
			gi589 = UnityGI_Base( data, 1, diffNorm589 );
			float3 indirectDiffuse589 = gi589.indirect.diffuse + diffNorm589 * 0.0001;
			float3 temp_output_601_0 = ( ( indirectDiffuse589 * ase_lightColor.rgb ) + pow( ase_lightAtten , 0.1 ) );
			float4 color614 = IsGammaSpace() ? float4(1,1,1,0) : float4(1,1,1,0);
			#ifdef _NEONMATERIAL1_ON
				float4 staticSwitch636 = Albedo613;
			#else
				float4 staticSwitch636 = ( ( Albedo613 * ( temp_output_610_0 * ( saturate( 2.0f*blendOpDest621*blendOpSrc621 + blendOpDest621*blendOpDest621*(1.0f - 2.0f*blendOpSrc621) )) ) ) * ( float4( temp_output_601_0 , 0.0 ) + ( float4( pow( ( temp_output_601_0 * ase_lightAtten ) , 4.0 ) , 0.0 ) * ( color614 * 2.6 ) ) ) );
			#endif
			float4 Blacklighting638 = staticSwitch636;
			float3 ase_worldPos = i.worldPos;
			#if defined(LIGHTMAP_ON) && UNITY_VERSION < 560 //aseld
			float3 ase_worldlightDir = 0;
			#else //aseld
			float3 ase_worldlightDir = normalize( UnityWorldSpaceLightDir( ase_worldPos ) );
			#endif //aseld
			float dotResult586 = dot( (WorldNormalVector( i , normalMap209 )) , ase_worldlightDir );
			float Normal_lightdir592 = dotResult586;
			float2 temp_cast_3 = ((Normal_lightdir592*0.0 + 0.0)).xx;
			float4 shadow631 = ( Albedo613 * tex2D( _ToonRamp1, temp_cast_3 ) );
			UnityGI gi596 = gi;
			float3 diffNorm596 = WorldNormalVector( i , normalMap209 );
			gi596 = UnityGI_Base( data, 1, diffNorm596 );
			float3 indirectDiffuse596 = gi596.indirect.diffuse + diffNorm596 * 0.0001;
			float3 temp_output_607_0 = ( pow( ase_lightAtten , 0.1 ) + ( indirectDiffuse596 * ase_lightColor.rgb ) );
			float4 color617 = IsGammaSpace() ? float4(0.8999152,0,1,0) : float4(0.7872446,0,1,0);
			float4 lighting639 = ( shadow631 * ( float4( temp_output_607_0 , 0.0 ) + ( float4( pow( ( temp_output_607_0 * ase_lightAtten ) , 4.0 ) , 0.0 ) * ( color617 * 2.6 ) ) ) );
			float3 WorldPos218 = ase_worldPos;
			float3 ObjectPos218 = EntrancePositionsArray[0].xyz;
			float localLoopArrayPositions218 = LoopArrayPositions218( WorldPos218 , ObjectPos218 );
			float clampResult561 = clamp( pow( pow( ( localLoopArrayPositions218 / _Distance ) , 4000.0 ) , ase_lightAtten ) , 0.0 , 1.0 );
			float PositionsLoop562 = clampResult561;
			float4 lerpResult648 = lerp( ( LightsStates319 == 1.0 ? Blacklighting638 : lighting639 ) , lighting639 , PositionsLoop562);
			c.rgb = lerpResult648.rgb;
			c.a = 1;
			return c;
		}

		inline void LightingStandardCustomLighting_GI( inout SurfaceOutputCustomLightingCustom s, UnityGIInput data, inout UnityGI gi )
		{
			s.GIData = data;
		}

		void surf( Input i , inout SurfaceOutputCustomLightingCustom o )
		{
			o.SurfInput = i;
			o.Normal = float3(0,0,1);
			float2 uv_TextureSample1 = i.uv_texcoord * _TextureSample1_ST.xy + _TextureSample1_ST.zw;
			float4 tex2DNode597 = tex2D( _TextureSample1, uv_TextureSample1 );
			#ifdef _NEONMATERIAL2_ON
				float4 staticSwitch665 = ( tex2DNode597 * _PoweEmession1 );
			#else
				float4 staticSwitch665 = float4( 0,0,0,0 );
			#endif
			o.Emission = staticSwitch665.rgb;
		}

		ENDCG
		CGPROGRAM
		#pragma surface surf StandardCustomLighting keepalpha fullforwardshadows 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			#include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float2 customPack1 : TEXCOORD1;
				float4 tSpace0 : TEXCOORD2;
				float4 tSpace1 : TEXCOORD3;
				float4 tSpace2 : TEXCOORD4;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO( o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				Input customInputData;
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				half3 worldNormal = UnityObjectToWorldNormal( v.normal );
				half3 worldTangent = UnityObjectToWorldDir( v.tangent.xyz );
				half tangentSign = v.tangent.w * unity_WorldTransformParams.w;
				half3 worldBinormal = cross( worldNormal, worldTangent ) * tangentSign;
				o.tSpace0 = float4( worldTangent.x, worldBinormal.x, worldNormal.x, worldPos.x );
				o.tSpace1 = float4( worldTangent.y, worldBinormal.y, worldNormal.y, worldPos.y );
				o.tSpace2 = float4( worldTangent.z, worldBinormal.z, worldNormal.z, worldPos.z );
				o.customPack1.xy = customInputData.uv_texcoord;
				o.customPack1.xy = v.texcoord;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				return o;
			}
			half4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				surfIN.uv_texcoord = IN.customPack1.xy;
				float3 worldPos = float3( IN.tSpace0.w, IN.tSpace1.w, IN.tSpace2.w );
				half3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				surfIN.worldPos = worldPos;
				surfIN.worldNormal = float3( IN.tSpace0.z, IN.tSpace1.z, IN.tSpace2.z );
				surfIN.internalSurfaceTtoW0 = IN.tSpace0.xyz;
				surfIN.internalSurfaceTtoW1 = IN.tSpace1.xyz;
				surfIN.internalSurfaceTtoW2 = IN.tSpace2.xyz;
				SurfaceOutputCustomLightingCustom o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutputCustomLightingCustom, o )
				surf( surfIN, o );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=18800
290.4;0.8;976;781;83.78952;-831.5351;4.984941;True;False
Node;AmplifyShaderEditor.SamplerNode;208;-488.4385,1104.758;Inherit;True;Property;_Normal;Normal;4;0;Create;True;0;0;0;False;0;False;-1;None;None;True;0;True;bump;Auto;True;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.CommentaryNode;578;2979.507,2792.091;Inherit;False;2131.798;1292.748;Black Lighting;14;574;656;638;636;633;630;625;623;621;610;609;606;604;581;Black Lighting;1,1,1,1;0;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;209;-112.282,1120.871;Inherit;False;normalMap;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.CommentaryNode;579;808.9927,817.2774;Inherit;False;735.7573;448.1411;Comment;4;592;586;584;583;N dot L;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;581;3260.846,3365.848;Inherit;False;1244.118;682.9761;Small lumière;14;629;626;619;615;614;612;608;601;594;591;589;588;587;585;Small lumière;1,1,1,1;0;0
Node;AmplifyShaderEditor.GetLocalVarNode;582;626.5842,876.9388;Inherit;False;209;normalMap;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.CommentaryNode;575;1324.693,2807.979;Inherit;False;1666.029;786.7881;Comment;17;639;637;635;634;632;628;627;622;617;616;607;602;599;596;595;593;590;Lighting;1,1,1,1;0;0
Node;AmplifyShaderEditor.WorldNormalVector;583;859.7448,867.2774;Inherit;False;False;1;0;FLOAT3;0,0,1;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.WorldSpaceLightDirHlpNode;584;858.9927,1084.818;Inherit;False;False;1;0;FLOAT;0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.GetLocalVarNode;585;3310.846,3570.699;Inherit;False;209;normalMap;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.LightAttenuation;588;3414.921,3415.848;Inherit;False;0;1;FLOAT;0
Node;AmplifyShaderEditor.DotProductOpNode;586;1141.911,1056.225;Inherit;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;577;1579.358,1019.281;Inherit;False;828.3352;474.0365;Albedo;4;613;600;598;597;;1,1,1,1;0;0
Node;AmplifyShaderEditor.IndirectDiffuseLighting;589;3499.031,3567.375;Inherit;False;Tangent;1;0;FLOAT3;0,0,1;False;1;FLOAT3;0
Node;AmplifyShaderEditor.LightColorNode;587;3553.079,3650.06;Inherit;False;0;3;COLOR;0;FLOAT3;1;FLOAT;2
Node;AmplifyShaderEditor.GetLocalVarNode;590;1374.693,3116.642;Inherit;False;209;normalMap;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ColorNode;598;1694.893,1069.281;Inherit;False;Property;_Tint1;Tint;0;0;Create;True;0;0;0;False;0;False;0.4339623,0.4339623,0.4339623,0;1,1,1,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;591;3759.816,3537.522;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;592;1316.75,1073.293;Inherit;False;Normal_lightdir;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LightColorNode;593;1616.927,3196.002;Inherit;False;0;3;COLOR;0;FLOAT3;1;FLOAT;2
Node;AmplifyShaderEditor.PowerNode;594;3678.267,3425.351;Inherit;False;False;2;0;FLOAT;0;False;1;FLOAT;0.1;False;1;FLOAT;0
Node;AmplifyShaderEditor.LightAttenuation;595;1478.769,2961.791;Inherit;False;0;1;FLOAT;0
Node;AmplifyShaderEditor.IndirectDiffuseLighting;596;1562.878,3113.317;Inherit;False;Tangent;1;0;FLOAT3;0,0,1;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SamplerNode;597;1629.358,1263.318;Inherit;True;Property;_TextureSample1;Texture Sample 0;3;0;Create;True;0;0;0;False;0;False;-1;None;bf963d1f25cedbc4b80776316eb8b7fe;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.CommentaryNode;576;1604.016,1599.181;Inherit;False;1205.722;498.2904;Comment;7;631;624;620;618;611;605;603;Shadow;1,1,1,1;0;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;599;1834.664,3075.465;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;603;1665.14,1973.465;Inherit;False;Constant;_Float1;Float 0;1;0;Create;True;0;0;0;False;0;False;0;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;600;1957.893,1195.281;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.PowerNode;602;1757.492,2967.768;Inherit;False;False;2;0;FLOAT;0;False;1;FLOAT;0.1;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;606;3022.511,3124.971;Inherit;False;Constant;_Color3;Color 2;8;0;Create;True;0;0;0;False;0;False;0.4962019,0,0.7075472,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;601;3919.269,3439.351;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;605;1654.016,1789.514;Inherit;False;592;Normal_lightdir;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;604;3029.989,2922.415;Inherit;False;Constant;_Color4;Color 3;8;0;Create;True;0;0;0;False;0;False;0.8227772,0.5295479,0.9433962,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ScaleAndOffsetNode;611;1839.315,1940.271;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;1;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;609;3328.149,3159.072;Inherit;False;Constant;_Color5;Color 4;8;0;Create;True;0;0;0;False;0;False;0.8745098,0.6313726,0.9607843,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;614;3805.75,3839.824;Inherit;False;Constant;_Color2;Color 1;8;0;Create;True;0;0;0;False;0;False;1,1,1,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;612;3932.153,3672.21;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;613;2182.893,1232.281;Inherit;False;Albedo;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.BlendOpsNode;610;3377.721,2918.579;Inherit;False;HardLight;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;1;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;608;4056.088,3913.708;Inherit;False;Constant;_Float4;Float 3;8;0;Create;True;0;0;0;False;0;False;2.6;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;607;1983.115,2985.293;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WorldPosInputsNode;215;1911.42,4641.577;Inherit;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;615;4219.089,3804.875;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;617;1869.597,3385.767;Inherit;False;Constant;_Color1;Color 0;8;0;Create;True;0;0;0;False;0;False;0.8999152,0,1,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;618;2072.566,1841.582;Inherit;True;Property;_ToonRamp1;ToonRamp;2;0;Create;True;0;0;0;False;0;False;-1;None;43293c38a102d404e8ad405969a29a1d;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;622;2119.936,3459.65;Inherit;False;Constant;_Float3;Float 2;8;0;Create;True;0;0;0;False;0;False;2.6;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;620;2116.767,1729.474;Inherit;False;613;Albedo;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.BlendOpsNode;621;3595.636,3087.241;Inherit;True;SoftLight;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;1;False;1;COLOR;0
Node;AmplifyShaderEditor.GlobalArrayNode;216;1865.634,4790.345;Inherit;False;EntrancePositionsArray;0;30;2;False;False;0;1;False;Object;147;4;0;INT;0;False;2;INT;0;False;1;INT;0;False;3;INT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.PowerNode;619;4121.728,3671.355;Inherit;False;False;2;0;FLOAT3;0,0,0;False;1;FLOAT;4;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;616;2025.999,3220.153;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;628;2282.936,3350.817;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.CustomExpressionNode;218;2123.022,4693.498;Float;False;float closest=10000@$float now=0@$for(int i=0@ i<EntrancePositionsArray.Length@i++){$	now = distance(WorldPos,EntrancePositionsArray[i])@$	if(now < closest){$	closest = now@$	}$}$return closest@;1;False;2;True;WorldPos;FLOAT3;0,0,0;In;;Inherit;False;True;ObjectPos;FLOAT3;0,0,0;In;;Inherit;False;Loop Array Positions;True;False;0;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;623;3853.2,2923.391;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;626;4322.971,3693.096;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.CommentaryNode;318;3329.307,4576.237;Inherit;False;728.8997;365.3354;Lights states;7;555;331;333;341;342;350;319;Lights States;1,1,1,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;217;2152.653,4807.039;Inherit;False;Property;_Distance;Distance;1;0;Create;True;0;0;0;False;0;False;4;4;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;625;3084.841,2840.844;Inherit;False;613;Albedo;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.PowerNode;627;2185.575,3217.298;Inherit;False;False;2;0;FLOAT3;0,0,0;False;1;FLOAT;4;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;624;2407.852,1785.639;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;629;4352.563,3465.678;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;219;2339.861,4696.089;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;630;4156.463,2820.04;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;631;2584.939,1792.767;Inherit;False;shadow;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;632;2386.818,3239.039;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;333;3345.966,4765.577;Inherit;False;Constant;_ColorBlacklightState;Color Blacklight State;8;0;Create;True;0;0;0;False;0;False;0.7843137,0,1,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LightColorNode;331;3409.253,4647.513;Inherit;False;0;3;COLOR;0;FLOAT3;1;FLOAT;2
Node;AmplifyShaderEditor.LightAttenuation;408;2490.148,4811.726;Inherit;False;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;633;4410.433,2821.489;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;635;1932.944,2857.979;Inherit;False;631;shadow;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;634;2416.411,3011.621;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.NormalizeNode;341;3569.258,4767.249;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.PowerNode;413;2525.397,4695.444;Inherit;False;False;2;0;FLOAT;0;False;1;FLOAT;4000;False;1;FLOAT;0
Node;AmplifyShaderEditor.NormalizeNode;342;3566.613,4647.06;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.CommentaryNode;311;1844.726,4584.387;Inherit;False;1440.589;364.5928;Positions lights loop;2;561;562;Lights Positions Loop;1,1,1,1;0;0
Node;AmplifyShaderEditor.PowerNode;416;2727.635,4693.936;Inherit;False;False;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.DotProductOpNode;350;3729.287,4693.523;Inherit;False;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;637;2588.327,2908.467;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.StaticSwitch;636;4655.885,2825.44;Inherit;False;Property;_NeonMaterial1;NeonMaterial;5;0;Create;True;0;0;0;False;0;False;0;1;1;True;;Toggle;2;Key0;Key1;Create;True;True;9;1;COLOR;0,0,0,0;False;0;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;4;COLOR;0,0,0,0;False;5;COLOR;0,0,0,0;False;6;COLOR;0,0,0,0;False;7;COLOR;0,0,0,0;False;8;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;639;2765.921,2911.723;Inherit;False;lighting;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;638;4908.244,2814.739;Inherit;False;Blacklighting;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;319;3850.625,4689.145;Float;False;LightsStates;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ClampOpNode;561;2900.456,4694.311;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;646;4331.309,1477.977;Inherit;False;Property;_PoweEmession1;Powe Emession;6;0;Create;True;0;0;0;False;0;False;0;0;0;10;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;641;4065.474,1778.22;Inherit;False;639;lighting;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;642;4099.313,1628.535;Inherit;False;Constant;_Float2;Float 1;8;0;Create;True;0;0;0;False;0;False;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;562;3068.456,4688.311;Inherit;False;PositionsLoop;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;640;4043.686,1698.875;Inherit;False;638;Blacklighting;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;643;4043.645,1548.901;Inherit;False;319;LightsStates;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;644;4580.024,1873.735;Inherit;False;562;PositionsLoop;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;645;4601.089,1772.036;Inherit;False;639;lighting;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.Compare;647;4299.29,1574.848;Inherit;False;0;4;0;FLOAT;0;False;1;FLOAT;0;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;650;4598.015,1440.27;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.CommentaryNode;580;826.5723,1296.88;Inherit;False;730.0077;473.985;Comment;4;666;662;655;653;ViewDir;1,1,1,1;0;0
Node;AmplifyShaderEditor.DotProductOpNode;657;4965.75,2185.126;Inherit;False;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;648;4834.804,1726.468;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;661;4598.015,1440.27;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.ComponentMaskNode;660;4738.389,2261.563;Inherit;False;True;True;True;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ViewDirInputsCoordNode;662;876.7084,1585.266;Inherit;False;World;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.LightColorNode;659;4388.413,2146.304;Inherit;False;0;3;COLOR;0;FLOAT3;1;FLOAT;2
Node;AmplifyShaderEditor.StaticSwitch;665;4731.138,1485.474;Inherit;False;Property;_NeonMaterial2;NeonMaterial;7;0;Create;True;0;0;0;False;0;False;0;1;1;True;_NeonMaterial;Toggle;2;Key0;Key1;Create;True;True;9;1;COLOR;0,0,0,0;False;0;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;4;COLOR;0,0,0,0;False;5;COLOR;0,0,0,0;False;6;COLOR;0,0,0,0;False;7;COLOR;0,0,0,0;False;8;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.DotProductOpNode;666;1132.649,1447.345;Inherit;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WorldNormalVector;655;839.5718,1372.88;Inherit;False;False;1;0;FLOAT3;0,0,1;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RegisterLocalVarNode;656;4908.244,2814.739;Inherit;False;Blacklighting;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;653;1328.58,1443.438;Inherit;False;normal_viewdir;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.NormalizeNode;652;4545.773,2145.852;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.NormalizeNode;651;4548.418,2266.042;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;555;3850.625,4689.145;Float;False;LightsStates;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;210;598.0093,969.0223;Inherit;False;209;normalMap;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ComponentMaskNode;663;4741.265,2152.314;Inherit;False;True;True;True;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;658;4325.126,2264.369;Inherit;False;Constant;_Color6;Color 5;8;0;Create;True;0;0;0;False;0;False;0.9960784,0.9529412,0.8352941,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;214;5260.651,1540.956;Float;False;True;-1;2;ASEMaterialInspector;0;0;CustomLighting;shader_CelShadingV2;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Opaque;0.5;True;True;0;False;Opaque;;Geometry;All;14;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;False;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;4;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
Node;AmplifyShaderEditor.CommentaryNode;573;2979.507,2792.091;Inherit;False;2131.798;1292.748;Black Lighting;0;Black Lighting;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;574;3260.846,3365.848;Inherit;False;1244.118;682.9761;Small lumière;0;Small lumière;1,1,1,1;0;0
WireConnection;209;0;208;0
WireConnection;583;0;582;0
WireConnection;586;0;583;0
WireConnection;586;1;584;0
WireConnection;589;0;585;0
WireConnection;591;0;589;0
WireConnection;591;1;587;1
WireConnection;592;0;586;0
WireConnection;594;0;588;0
WireConnection;596;0;590;0
WireConnection;599;0;596;0
WireConnection;599;1;593;1
WireConnection;600;0;598;0
WireConnection;600;1;597;0
WireConnection;602;0;595;0
WireConnection;601;0;591;0
WireConnection;601;1;594;0
WireConnection;611;0;605;0
WireConnection;611;1;603;0
WireConnection;611;2;603;0
WireConnection;612;0;601;0
WireConnection;612;1;588;0
WireConnection;613;0;600;0
WireConnection;610;0;604;0
WireConnection;610;1;606;0
WireConnection;607;0;602;0
WireConnection;607;1;599;0
WireConnection;615;0;614;0
WireConnection;615;1;608;0
WireConnection;618;1;611;0
WireConnection;621;0;610;0
WireConnection;621;1;609;0
WireConnection;619;0;612;0
WireConnection;616;0;607;0
WireConnection;616;1;595;0
WireConnection;628;0;617;0
WireConnection;628;1;622;0
WireConnection;218;0;215;0
WireConnection;218;1;216;0
WireConnection;623;0;610;0
WireConnection;623;1;621;0
WireConnection;626;0;619;0
WireConnection;626;1;615;0
WireConnection;627;0;616;0
WireConnection;624;0;620;0
WireConnection;624;1;618;0
WireConnection;629;0;601;0
WireConnection;629;1;626;0
WireConnection;219;0;218;0
WireConnection;219;1;217;0
WireConnection;630;0;625;0
WireConnection;630;1;623;0
WireConnection;631;0;624;0
WireConnection;632;0;627;0
WireConnection;632;1;628;0
WireConnection;633;0;630;0
WireConnection;633;1;629;0
WireConnection;634;0;607;0
WireConnection;634;1;632;0
WireConnection;341;0;333;0
WireConnection;413;0;219;0
WireConnection;342;0;331;0
WireConnection;416;0;413;0
WireConnection;416;1;408;0
WireConnection;350;0;342;0
WireConnection;350;1;341;0
WireConnection;637;0;635;0
WireConnection;637;1;634;0
WireConnection;636;1;633;0
WireConnection;636;0;625;0
WireConnection;639;0;637;0
WireConnection;638;0;636;0
WireConnection;319;0;350;0
WireConnection;561;0;416;0
WireConnection;562;0;561;0
WireConnection;647;0;643;0
WireConnection;647;1;642;0
WireConnection;647;2;640;0
WireConnection;647;3;641;0
WireConnection;650;0;597;0
WireConnection;650;1;646;0
WireConnection;657;0;663;0
WireConnection;657;1;660;0
WireConnection;648;0;647;0
WireConnection;648;1;645;0
WireConnection;648;2;644;0
WireConnection;660;0;651;0
WireConnection;665;0;650;0
WireConnection;666;0;655;0
WireConnection;666;1;662;0
WireConnection;653;0;666;0
WireConnection;652;0;659;0
WireConnection;651;0;658;0
WireConnection;663;0;652;0
WireConnection;214;2;665;0
WireConnection;214;13;648;0
ASEEND*/
//CHKSM=AFF793A9DAE1B82E0CED0FE18FA7FC6ECF42C29B