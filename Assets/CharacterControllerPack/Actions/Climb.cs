﻿using UnityEngine;

namespace CharacterController_1Person
{
    public class Climb : MonoBehaviour
    {
        public LayerMask interactionLayer;
        public LayerMask verificationLayer;
        [HideInInspector] public GameObject currentHitObject;

        #region Variables
        private float sphereCastDistance = 0.6f;
        public float distanceV;
        private float sphereCastRadius = 0.1f;

        //Condition 1
        private float currentHitDistance;
        //Condition 2
        private float currentHitDistance2;
        //Condition 3
        private float currentHitDistance3;
        private float currentHitDistance4;
        public float currentHitDistanceVerif;

        private Vector3 originTop;
        private Vector3 originMiddle;
        private Vector3 originBottom;
        private Vector3 forwardDirection;
        private Vector3 downDirection;
        private Vector3 sphereOne;

        public RaycastHit FirstHorizontalHit;
        public RaycastHit rayHit;
        public RaycastHit ThirdHorizontalHit;
        public RaycastHit SecondHorizontalHit;
        #endregion

        [HideInInspector] public int climbPhase = 1;
        public bool climb;

        private void Update()
        {
            //Attribution des valeurs (origin, direction, sphereOne) Vector3
            originTop = new Vector3(this.transform.position.x, this.transform.position.y + 2f, this.transform.position.z);
            originMiddle = new Vector3(this.transform.position.x, this.transform.position.y + 1f, this.transform.position.z);
            originBottom = new Vector3(this.transform.position.x, this.transform.position.y + 0.5f, this.transform.position.z);
            forwardDirection = this.transform.forward;
            downDirection = -transform.up;
            sphereOne = originTop + forwardDirection * currentHitDistance;

            //verification Raycast
            RaycastHit verif;
            if(Physics.Raycast(new Vector3(originMiddle.x, originMiddle.y + 0.5f, originMiddle.z), transform.up, out verif, distanceV, verificationLayer))
            {
                currentHitDistanceVerif = verif.distance;
            }
            else
            {
                currentHitDistanceVerif = distanceV;
            }

            //First Raycast
            RaycastHit FirstHorizontalHit;
            if (Physics.Raycast(originTop, forwardDirection, out FirstHorizontalHit, sphereCastDistance, interactionLayer))
            {
                currentHitObject = FirstHorizontalHit.transform.gameObject;
                currentHitDistance = FirstHorizontalHit.distance;
            }
            else
            {
                currentHitDistance = sphereCastDistance;
                currentHitObject = null;
            }

            //Second Raycast
            RaycastHit FirstVerticalHit;
            if (Physics.Raycast(sphereOne, downDirection, out FirstVerticalHit, sphereCastDistance, interactionLayer))
            {
                currentHitObject = FirstVerticalHit.transform.gameObject;
                currentHitDistance2 = FirstVerticalHit.distance;
            }
            else
            {
                currentHitDistance2 = sphereCastDistance;
            }

            //Third Raycast
            RaycastHit ThirdHorizontalHit;
            if (Physics.Raycast(originMiddle, forwardDirection, out ThirdHorizontalHit, sphereCastDistance, interactionLayer))
            {
                currentHitObject = ThirdHorizontalHit.transform.gameObject;
                currentHitDistance4 = ThirdHorizontalHit.distance;
            }
            else
            {
                currentHitDistance4 = sphereCastDistance;
            }

            //Fourth Raycast
            RaycastHit SecondHorizontalHit;
            if (Physics.Raycast(originBottom, forwardDirection, out SecondHorizontalHit, sphereCastDistance, interactionLayer))
            {
                currentHitObject = SecondHorizontalHit.transform.gameObject;
                currentHitDistance3 = SecondHorizontalHit.distance;
            }
            else
            {
                currentHitDistance3 = sphereCastDistance;
            }

            //Fonction de vérification
            Verification();
        }

        private void Verification()
        {
            //Fonction de vérification
            bool valideFirst, valideSecond, valideThird, valideFourth;
            //Condition 1
            valideFirst = currentHitDistance >= sphereCastDistance;
            //Condition 2
            valideSecond = currentHitDistance2 > 0.1f && currentHitDistance2 <= 0.9f || currentHitDistance2 == sphereCastDistance;
            //Condition 3
            valideThird = currentHitDistance4 > 0.1f && currentHitDistance4 <= 0.3f && currentHitDistance3 > 0.1f && currentHitDistance3 <= 0.3f;
            //Verification bool
            valideFourth = currentHitDistanceVerif > 0.8f;
            //Final Condition
            climb = valideFirst && valideSecond && valideThird && valideFourth;
        }

        private void RaycastTest(RaycastHit rayHit, Vector3 origin, Vector3 direction, float distance, LayerMask layer)
        {
            if (Physics.Raycast(origin, direction, out rayHit, distance, layer))
            {
                currentHitObject = rayHit.transform.gameObject;
                currentHitDistance2 = this.rayHit.distance;
            }
            else
            {
                currentHitDistance2 = sphereCastDistance;
            }
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawLine(originTop, originTop + forwardDirection * currentHitDistance);
            Gizmos.DrawSphere(originTop + forwardDirection * currentHitDistance, sphereCastRadius);

            Gizmos.color = Color.red;
            Gizmos.DrawLine(sphereOne, sphereOne + downDirection * currentHitDistance2);
            Gizmos.DrawSphere(sphereOne + downDirection * currentHitDistance2, sphereCastRadius);

            Gizmos.color = Color.yellow;
            Gizmos.DrawLine(originMiddle, originMiddle + forwardDirection * currentHitDistance4);
            Gizmos.DrawSphere(originMiddle + forwardDirection * currentHitDistance4, sphereCastRadius);

            Gizmos.color = Color.magenta;
            Gizmos.DrawLine(originBottom, originBottom + forwardDirection * currentHitDistance3);
            Gizmos.DrawSphere(originBottom + forwardDirection * currentHitDistance3, sphereCastRadius);

            Gizmos.color = Color.black;
            Gizmos.DrawLine(originMiddle, originMiddle + transform.up * currentHitDistanceVerif);
            Gizmos.DrawSphere(originMiddle + transform.up * currentHitDistanceVerif, sphereCastRadius);
        }
    }
}
