// GENERATED AUTOMATICALLY FROM 'Assets/CharacterControllerPack/CharacterController (1Person)/Inputs/Controls.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @Controls : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @Controls()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""Controls"",
    ""maps"": [
        {
            ""name"": ""Locomotion"",
            ""id"": ""43d5be42-c202-4333-a2d5-2e59b4cac3d6"",
            ""actions"": [
                {
                    ""name"": ""Move"",
                    ""type"": ""PassThrough"",
                    ""id"": ""219184bb-eccf-4786-a903-48deb5531540"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Look"",
                    ""type"": ""PassThrough"",
                    ""id"": ""a33a1cd3-b867-4763-b895-02ca6fa8ebf3"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Jump"",
                    ""type"": ""Button"",
                    ""id"": ""1a5e3549-6fd6-4223-8b23-001a147450d0"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": ""Arrows Key"",
                    ""id"": ""2199f119-c2bb-4d86-b5a1-1dc190deae6e"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""098c87a7-bd48-415f-bb24-557756c20bf7"",
                    ""path"": ""<Keyboard>/upArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""19a6c4bd-9177-4db9-a4ff-762c399064d0"",
                    ""path"": ""<Keyboard>/downArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""30a1f641-b6c8-45a4-908a-e65766289b5c"",
                    ""path"": ""<Keyboard>/leftArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""9ac6bfb6-91ed-4a58-bd20-835a9e5ba6f8"",
                    ""path"": ""<Keyboard>/rightArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""148e59eb-7664-4c6e-8408-b046ad979558"",
                    ""path"": ""<Gamepad>/leftStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""Keys"",
                    ""id"": ""9ec83ac4-e3a0-4975-8462-25347200f4e8"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""7d45cc1f-07a6-4b13-8d01-afeef6e17a9f"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""ab7b2e1e-f1f9-4047-8e67-7d30446b2e29"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""c329d9f8-3206-40b0-9096-53d807fd70ab"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""a75390aa-286d-4c51-a451-e32961466ac7"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""5667e25c-0576-48a1-a13f-6c8e948448a2"",
                    ""path"": ""<Mouse>/delta"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Look"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""2a0f1d1a-7bb9-49a2-88c2-16f0d690123d"",
                    ""path"": ""<Gamepad>/rightStick"",
                    ""interactions"": """",
                    ""processors"": ""ScaleVector2(x=7,y=7)"",
                    ""groups"": """",
                    ""action"": ""Look"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""78a77f34-1006-4a8d-8b05-e50c72a50685"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Jump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""89e262d7-aad8-4071-ac84-2fbbf81a7fc9"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Jump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""Interaction"",
            ""id"": ""c14db29e-1996-4482-a90c-cc3ffa47277b"",
            ""actions"": [
                {
                    ""name"": ""Interact"",
                    ""type"": ""Button"",
                    ""id"": ""d0bc8685-a77c-4016-b716-2f29ff542d29"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""PushHorizontal"",
                    ""type"": ""PassThrough"",
                    ""id"": ""5aed4d78-b854-4fe3-80fc-e3db63553b6f"",
                    ""expectedControlType"": ""Axis"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""PushVertical"",
                    ""type"": ""PassThrough"",
                    ""id"": ""0416ac37-cd5d-446c-aefa-3bb1dee91bfe"",
                    ""expectedControlType"": ""Axis"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""FlashLight"",
                    ""type"": ""Button"",
                    ""id"": ""caa593a1-f6e7-481a-988d-d998d3759056"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""1da347af-6dd3-40b0-b04e-bc266d1649d6"",
                    ""path"": ""<Keyboard>/numpad1"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Interact"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""d82cacaf-350a-4c45-b7ae-32de2e25f5c8"",
                    ""path"": ""<Gamepad>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Interact"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""46e0006b-8d35-413f-9a38-1ce720bacb5d"",
                    ""path"": ""<Keyboard>/e"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Interact"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""Arrows Key"",
                    ""id"": ""cb78215a-66db-4eaa-91f8-94180bc14f86"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""PushHorizontal"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""f9e82e44-68a3-4316-b269-d9a57e2b1398"",
                    ""path"": ""<Keyboard>/downArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""PushHorizontal"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""dd2114a3-774f-43e8-b1f7-6b4dec053ce7"",
                    ""path"": ""<Keyboard>/upArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""PushHorizontal"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""Stick Pad"",
                    ""id"": ""aaf1270b-cf82-477f-93b7-494be4374ef0"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""PushHorizontal"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""e21d2688-1a2c-4c09-9a7c-19c4c1493082"",
                    ""path"": ""<Gamepad>/leftStick/down"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""PushHorizontal"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""03b54551-5ad1-4022-b550-9a8498e06301"",
                    ""path"": ""<Gamepad>/leftStick/up"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""PushHorizontal"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""Keys"",
                    ""id"": ""71b2d57e-81d6-4be1-9c0a-b92ee4f48238"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""PushHorizontal"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""af94dc16-0930-4932-9e8b-e0069b186c16"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""PushHorizontal"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""0d2d6dc7-5d7d-4010-b5ee-0454baa4f10a"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""PushHorizontal"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""Arrows Key"",
                    ""id"": ""c6950efb-c0aa-4a07-86d4-22fac70e7c89"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""PushVertical"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""ea60c066-e42a-4221-95b1-7ff244a68d08"",
                    ""path"": ""<Keyboard>/downArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""PushVertical"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""7efeadae-ed84-46f9-a436-42f7ac6503c2"",
                    ""path"": ""<Keyboard>/upArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""PushVertical"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""Stick Pad"",
                    ""id"": ""7cc86bb1-bf90-4668-9ecf-2ab5d2bd9340"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""PushVertical"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""1414382e-5e0f-4424-8ab7-33242cccd658"",
                    ""path"": ""<Gamepad>/leftStick/down"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""PushVertical"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""fa56c641-ae1b-4e02-b08c-5a59ac4473ae"",
                    ""path"": ""<Gamepad>/leftStick/up"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""PushVertical"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""Keys"",
                    ""id"": ""e7d8b0fc-aad1-49a5-acec-589d8be657f9"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""PushVertical"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""724b0f24-879d-4cc9-a4d9-665b0045551b"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""PushVertical"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""a3fd5483-e2b9-493a-bd23-2f7ad521cc49"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""PushVertical"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""90da9d98-5a57-4bc0-bedf-f1e433467e9c"",
                    ""path"": ""<Gamepad>/dpad/up"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""FlashLight"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""Menu"",
            ""id"": ""c520e1c1-c0cd-4f93-84d8-90ece4a70a06"",
            ""actions"": [
                {
                    ""name"": ""Pause"",
                    ""type"": ""Button"",
                    ""id"": ""be63c751-f2d1-4d40-9211-5eed967b93f2"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Select"",
                    ""type"": ""PassThrough"",
                    ""id"": ""b23d8662-fc47-4692-81ac-6482c63abadf"",
                    ""expectedControlType"": ""Axis"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Submit"",
                    ""type"": ""Button"",
                    ""id"": ""2c544f91-92c9-4501-9f39-b7ed53da30e9"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Back"",
                    ""type"": ""Button"",
                    ""id"": ""999c929d-d957-448d-8028-bec15303130f"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": ""1D Axis"",
                    ""id"": ""bbeaae30-66b2-47b4-8f03-a8936d02177b"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Select"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""ee84059c-c6f7-434e-b764-385bdec31926"",
                    ""path"": ""<Gamepad>/leftStick/up"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Select"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""812efed0-c45a-41e3-b301-b37666e7bec4"",
                    ""path"": ""<Gamepad>/leftStick/down"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Select"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""37f75617-c480-4b28-8deb-6541092aa6d1"",
                    ""path"": ""<Keyboard>/p"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Pause"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""e79abd6e-36ca-4f97-8a20-7227b6219d27"",
                    ""path"": ""<Gamepad>/start"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Pause"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""c0716e95-76f1-425d-bcb8-095e948c9dbb"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Submit"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""b62f3cdd-c078-4845-aef3-e09bbbd06401"",
                    ""path"": ""<Gamepad>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Back"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""Cheats"",
            ""id"": ""ea0752b8-0561-4d20-a40d-dd79836747c8"",
            ""actions"": [
                {
                    ""name"": ""Teleport1stFloor"",
                    ""type"": ""Button"",
                    ""id"": ""8dc58449-0bd5-4e52-9927-387553cc3c06"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Teleport2ndFloor"",
                    ""type"": ""Button"",
                    ""id"": ""44044bcc-2b3e-4111-bf92-d72d3dea887e"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Teleport3rdFloor"",
                    ""type"": ""Button"",
                    ""id"": ""df3044c2-46e8-4a24-a50e-2bf1c33833fa"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""ResetGame"",
                    ""type"": ""Button"",
                    ""id"": ""ef50f6a9-8af4-43e2-a20a-709e9216feb9"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Skip204"",
                    ""type"": ""Button"",
                    ""id"": ""7a54190a-11a6-4ef0-84b5-7796966c5a2f"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""OpenDoors"",
                    ""type"": ""Button"",
                    ""id"": ""074442ec-e0a2-4454-8cc2-b45708fa7698"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Ending"",
                    ""type"": ""Button"",
                    ""id"": ""fd01ed52-4889-4762-9895-828249580a11"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""eaf62cf3-3e56-47d3-abcb-b269f9e7a2cf"",
                    ""path"": ""<Keyboard>/f1"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Teleport1stFloor"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""506e1e24-7828-419b-a2c7-d463bf28a1d8"",
                    ""path"": ""<Keyboard>/f2"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Teleport2ndFloor"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""3d031d5f-3e95-415b-bd5f-ee6e0ebb6e81"",
                    ""path"": ""<Keyboard>/f3"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Teleport3rdFloor"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""a599d005-3efa-4538-a8a8-a3f55c4aa012"",
                    ""path"": ""<Keyboard>/f4"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ResetGame"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""db5acea0-3ce5-4af3-b75e-763dc2d807a6"",
                    ""path"": ""<Keyboard>/f5"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Skip204"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""9c819b41-c8d7-4129-b5bd-b7fd56eb4342"",
                    ""path"": ""<Keyboard>/f6"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""OpenDoors"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""c11a2ec5-2069-47aa-ba2e-04f3156c35b6"",
                    ""path"": ""<Keyboard>/f7"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Ending"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // Locomotion
        m_Locomotion = asset.FindActionMap("Locomotion", throwIfNotFound: true);
        m_Locomotion_Move = m_Locomotion.FindAction("Move", throwIfNotFound: true);
        m_Locomotion_Look = m_Locomotion.FindAction("Look", throwIfNotFound: true);
        m_Locomotion_Jump = m_Locomotion.FindAction("Jump", throwIfNotFound: true);
        // Interaction
        m_Interaction = asset.FindActionMap("Interaction", throwIfNotFound: true);
        m_Interaction_Interact = m_Interaction.FindAction("Interact", throwIfNotFound: true);
        m_Interaction_PushHorizontal = m_Interaction.FindAction("PushHorizontal", throwIfNotFound: true);
        m_Interaction_PushVertical = m_Interaction.FindAction("PushVertical", throwIfNotFound: true);
        m_Interaction_FlashLight = m_Interaction.FindAction("FlashLight", throwIfNotFound: true);
        // Menu
        m_Menu = asset.FindActionMap("Menu", throwIfNotFound: true);
        m_Menu_Pause = m_Menu.FindAction("Pause", throwIfNotFound: true);
        m_Menu_Select = m_Menu.FindAction("Select", throwIfNotFound: true);
        m_Menu_Submit = m_Menu.FindAction("Submit", throwIfNotFound: true);
        m_Menu_Back = m_Menu.FindAction("Back", throwIfNotFound: true);
        // Cheats
        m_Cheats = asset.FindActionMap("Cheats", throwIfNotFound: true);
        m_Cheats_Teleport1stFloor = m_Cheats.FindAction("Teleport1stFloor", throwIfNotFound: true);
        m_Cheats_Teleport2ndFloor = m_Cheats.FindAction("Teleport2ndFloor", throwIfNotFound: true);
        m_Cheats_Teleport3rdFloor = m_Cheats.FindAction("Teleport3rdFloor", throwIfNotFound: true);
        m_Cheats_ResetGame = m_Cheats.FindAction("ResetGame", throwIfNotFound: true);
        m_Cheats_Skip204 = m_Cheats.FindAction("Skip204", throwIfNotFound: true);
        m_Cheats_OpenDoors = m_Cheats.FindAction("OpenDoors", throwIfNotFound: true);
        m_Cheats_Ending = m_Cheats.FindAction("Ending", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // Locomotion
    private readonly InputActionMap m_Locomotion;
    private ILocomotionActions m_LocomotionActionsCallbackInterface;
    private readonly InputAction m_Locomotion_Move;
    private readonly InputAction m_Locomotion_Look;
    private readonly InputAction m_Locomotion_Jump;
    public struct LocomotionActions
    {
        private @Controls m_Wrapper;
        public LocomotionActions(@Controls wrapper) { m_Wrapper = wrapper; }
        public InputAction @Move => m_Wrapper.m_Locomotion_Move;
        public InputAction @Look => m_Wrapper.m_Locomotion_Look;
        public InputAction @Jump => m_Wrapper.m_Locomotion_Jump;
        public InputActionMap Get() { return m_Wrapper.m_Locomotion; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(LocomotionActions set) { return set.Get(); }
        public void SetCallbacks(ILocomotionActions instance)
        {
            if (m_Wrapper.m_LocomotionActionsCallbackInterface != null)
            {
                @Move.started -= m_Wrapper.m_LocomotionActionsCallbackInterface.OnMove;
                @Move.performed -= m_Wrapper.m_LocomotionActionsCallbackInterface.OnMove;
                @Move.canceled -= m_Wrapper.m_LocomotionActionsCallbackInterface.OnMove;
                @Look.started -= m_Wrapper.m_LocomotionActionsCallbackInterface.OnLook;
                @Look.performed -= m_Wrapper.m_LocomotionActionsCallbackInterface.OnLook;
                @Look.canceled -= m_Wrapper.m_LocomotionActionsCallbackInterface.OnLook;
                @Jump.started -= m_Wrapper.m_LocomotionActionsCallbackInterface.OnJump;
                @Jump.performed -= m_Wrapper.m_LocomotionActionsCallbackInterface.OnJump;
                @Jump.canceled -= m_Wrapper.m_LocomotionActionsCallbackInterface.OnJump;
            }
            m_Wrapper.m_LocomotionActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Move.started += instance.OnMove;
                @Move.performed += instance.OnMove;
                @Move.canceled += instance.OnMove;
                @Look.started += instance.OnLook;
                @Look.performed += instance.OnLook;
                @Look.canceled += instance.OnLook;
                @Jump.started += instance.OnJump;
                @Jump.performed += instance.OnJump;
                @Jump.canceled += instance.OnJump;
            }
        }
    }
    public LocomotionActions @Locomotion => new LocomotionActions(this);

    // Interaction
    private readonly InputActionMap m_Interaction;
    private IInteractionActions m_InteractionActionsCallbackInterface;
    private readonly InputAction m_Interaction_Interact;
    private readonly InputAction m_Interaction_PushHorizontal;
    private readonly InputAction m_Interaction_PushVertical;
    private readonly InputAction m_Interaction_FlashLight;
    public struct InteractionActions
    {
        private @Controls m_Wrapper;
        public InteractionActions(@Controls wrapper) { m_Wrapper = wrapper; }
        public InputAction @Interact => m_Wrapper.m_Interaction_Interact;
        public InputAction @PushHorizontal => m_Wrapper.m_Interaction_PushHorizontal;
        public InputAction @PushVertical => m_Wrapper.m_Interaction_PushVertical;
        public InputAction @FlashLight => m_Wrapper.m_Interaction_FlashLight;
        public InputActionMap Get() { return m_Wrapper.m_Interaction; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(InteractionActions set) { return set.Get(); }
        public void SetCallbacks(IInteractionActions instance)
        {
            if (m_Wrapper.m_InteractionActionsCallbackInterface != null)
            {
                @Interact.started -= m_Wrapper.m_InteractionActionsCallbackInterface.OnInteract;
                @Interact.performed -= m_Wrapper.m_InteractionActionsCallbackInterface.OnInteract;
                @Interact.canceled -= m_Wrapper.m_InteractionActionsCallbackInterface.OnInteract;
                @PushHorizontal.started -= m_Wrapper.m_InteractionActionsCallbackInterface.OnPushHorizontal;
                @PushHorizontal.performed -= m_Wrapper.m_InteractionActionsCallbackInterface.OnPushHorizontal;
                @PushHorizontal.canceled -= m_Wrapper.m_InteractionActionsCallbackInterface.OnPushHorizontal;
                @PushVertical.started -= m_Wrapper.m_InteractionActionsCallbackInterface.OnPushVertical;
                @PushVertical.performed -= m_Wrapper.m_InteractionActionsCallbackInterface.OnPushVertical;
                @PushVertical.canceled -= m_Wrapper.m_InteractionActionsCallbackInterface.OnPushVertical;
                @FlashLight.started -= m_Wrapper.m_InteractionActionsCallbackInterface.OnFlashLight;
                @FlashLight.performed -= m_Wrapper.m_InteractionActionsCallbackInterface.OnFlashLight;
                @FlashLight.canceled -= m_Wrapper.m_InteractionActionsCallbackInterface.OnFlashLight;
            }
            m_Wrapper.m_InteractionActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Interact.started += instance.OnInteract;
                @Interact.performed += instance.OnInteract;
                @Interact.canceled += instance.OnInteract;
                @PushHorizontal.started += instance.OnPushHorizontal;
                @PushHorizontal.performed += instance.OnPushHorizontal;
                @PushHorizontal.canceled += instance.OnPushHorizontal;
                @PushVertical.started += instance.OnPushVertical;
                @PushVertical.performed += instance.OnPushVertical;
                @PushVertical.canceled += instance.OnPushVertical;
                @FlashLight.started += instance.OnFlashLight;
                @FlashLight.performed += instance.OnFlashLight;
                @FlashLight.canceled += instance.OnFlashLight;
            }
        }
    }
    public InteractionActions @Interaction => new InteractionActions(this);

    // Menu
    private readonly InputActionMap m_Menu;
    private IMenuActions m_MenuActionsCallbackInterface;
    private readonly InputAction m_Menu_Pause;
    private readonly InputAction m_Menu_Select;
    private readonly InputAction m_Menu_Submit;
    private readonly InputAction m_Menu_Back;
    public struct MenuActions
    {
        private @Controls m_Wrapper;
        public MenuActions(@Controls wrapper) { m_Wrapper = wrapper; }
        public InputAction @Pause => m_Wrapper.m_Menu_Pause;
        public InputAction @Select => m_Wrapper.m_Menu_Select;
        public InputAction @Submit => m_Wrapper.m_Menu_Submit;
        public InputAction @Back => m_Wrapper.m_Menu_Back;
        public InputActionMap Get() { return m_Wrapper.m_Menu; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(MenuActions set) { return set.Get(); }
        public void SetCallbacks(IMenuActions instance)
        {
            if (m_Wrapper.m_MenuActionsCallbackInterface != null)
            {
                @Pause.started -= m_Wrapper.m_MenuActionsCallbackInterface.OnPause;
                @Pause.performed -= m_Wrapper.m_MenuActionsCallbackInterface.OnPause;
                @Pause.canceled -= m_Wrapper.m_MenuActionsCallbackInterface.OnPause;
                @Select.started -= m_Wrapper.m_MenuActionsCallbackInterface.OnSelect;
                @Select.performed -= m_Wrapper.m_MenuActionsCallbackInterface.OnSelect;
                @Select.canceled -= m_Wrapper.m_MenuActionsCallbackInterface.OnSelect;
                @Submit.started -= m_Wrapper.m_MenuActionsCallbackInterface.OnSubmit;
                @Submit.performed -= m_Wrapper.m_MenuActionsCallbackInterface.OnSubmit;
                @Submit.canceled -= m_Wrapper.m_MenuActionsCallbackInterface.OnSubmit;
                @Back.started -= m_Wrapper.m_MenuActionsCallbackInterface.OnBack;
                @Back.performed -= m_Wrapper.m_MenuActionsCallbackInterface.OnBack;
                @Back.canceled -= m_Wrapper.m_MenuActionsCallbackInterface.OnBack;
            }
            m_Wrapper.m_MenuActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Pause.started += instance.OnPause;
                @Pause.performed += instance.OnPause;
                @Pause.canceled += instance.OnPause;
                @Select.started += instance.OnSelect;
                @Select.performed += instance.OnSelect;
                @Select.canceled += instance.OnSelect;
                @Submit.started += instance.OnSubmit;
                @Submit.performed += instance.OnSubmit;
                @Submit.canceled += instance.OnSubmit;
                @Back.started += instance.OnBack;
                @Back.performed += instance.OnBack;
                @Back.canceled += instance.OnBack;
            }
        }
    }
    public MenuActions @Menu => new MenuActions(this);

    // Cheats
    private readonly InputActionMap m_Cheats;
    private ICheatsActions m_CheatsActionsCallbackInterface;
    private readonly InputAction m_Cheats_Teleport1stFloor;
    private readonly InputAction m_Cheats_Teleport2ndFloor;
    private readonly InputAction m_Cheats_Teleport3rdFloor;
    private readonly InputAction m_Cheats_ResetGame;
    private readonly InputAction m_Cheats_Skip204;
    private readonly InputAction m_Cheats_OpenDoors;
    private readonly InputAction m_Cheats_Ending;
    public struct CheatsActions
    {
        private @Controls m_Wrapper;
        public CheatsActions(@Controls wrapper) { m_Wrapper = wrapper; }
        public InputAction @Teleport1stFloor => m_Wrapper.m_Cheats_Teleport1stFloor;
        public InputAction @Teleport2ndFloor => m_Wrapper.m_Cheats_Teleport2ndFloor;
        public InputAction @Teleport3rdFloor => m_Wrapper.m_Cheats_Teleport3rdFloor;
        public InputAction @ResetGame => m_Wrapper.m_Cheats_ResetGame;
        public InputAction @Skip204 => m_Wrapper.m_Cheats_Skip204;
        public InputAction @OpenDoors => m_Wrapper.m_Cheats_OpenDoors;
        public InputAction @Ending => m_Wrapper.m_Cheats_Ending;
        public InputActionMap Get() { return m_Wrapper.m_Cheats; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(CheatsActions set) { return set.Get(); }
        public void SetCallbacks(ICheatsActions instance)
        {
            if (m_Wrapper.m_CheatsActionsCallbackInterface != null)
            {
                @Teleport1stFloor.started -= m_Wrapper.m_CheatsActionsCallbackInterface.OnTeleport1stFloor;
                @Teleport1stFloor.performed -= m_Wrapper.m_CheatsActionsCallbackInterface.OnTeleport1stFloor;
                @Teleport1stFloor.canceled -= m_Wrapper.m_CheatsActionsCallbackInterface.OnTeleport1stFloor;
                @Teleport2ndFloor.started -= m_Wrapper.m_CheatsActionsCallbackInterface.OnTeleport2ndFloor;
                @Teleport2ndFloor.performed -= m_Wrapper.m_CheatsActionsCallbackInterface.OnTeleport2ndFloor;
                @Teleport2ndFloor.canceled -= m_Wrapper.m_CheatsActionsCallbackInterface.OnTeleport2ndFloor;
                @Teleport3rdFloor.started -= m_Wrapper.m_CheatsActionsCallbackInterface.OnTeleport3rdFloor;
                @Teleport3rdFloor.performed -= m_Wrapper.m_CheatsActionsCallbackInterface.OnTeleport3rdFloor;
                @Teleport3rdFloor.canceled -= m_Wrapper.m_CheatsActionsCallbackInterface.OnTeleport3rdFloor;
                @ResetGame.started -= m_Wrapper.m_CheatsActionsCallbackInterface.OnResetGame;
                @ResetGame.performed -= m_Wrapper.m_CheatsActionsCallbackInterface.OnResetGame;
                @ResetGame.canceled -= m_Wrapper.m_CheatsActionsCallbackInterface.OnResetGame;
                @Skip204.started -= m_Wrapper.m_CheatsActionsCallbackInterface.OnSkip204;
                @Skip204.performed -= m_Wrapper.m_CheatsActionsCallbackInterface.OnSkip204;
                @Skip204.canceled -= m_Wrapper.m_CheatsActionsCallbackInterface.OnSkip204;
                @OpenDoors.started -= m_Wrapper.m_CheatsActionsCallbackInterface.OnOpenDoors;
                @OpenDoors.performed -= m_Wrapper.m_CheatsActionsCallbackInterface.OnOpenDoors;
                @OpenDoors.canceled -= m_Wrapper.m_CheatsActionsCallbackInterface.OnOpenDoors;
                @Ending.started -= m_Wrapper.m_CheatsActionsCallbackInterface.OnEnding;
                @Ending.performed -= m_Wrapper.m_CheatsActionsCallbackInterface.OnEnding;
                @Ending.canceled -= m_Wrapper.m_CheatsActionsCallbackInterface.OnEnding;
            }
            m_Wrapper.m_CheatsActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Teleport1stFloor.started += instance.OnTeleport1stFloor;
                @Teleport1stFloor.performed += instance.OnTeleport1stFloor;
                @Teleport1stFloor.canceled += instance.OnTeleport1stFloor;
                @Teleport2ndFloor.started += instance.OnTeleport2ndFloor;
                @Teleport2ndFloor.performed += instance.OnTeleport2ndFloor;
                @Teleport2ndFloor.canceled += instance.OnTeleport2ndFloor;
                @Teleport3rdFloor.started += instance.OnTeleport3rdFloor;
                @Teleport3rdFloor.performed += instance.OnTeleport3rdFloor;
                @Teleport3rdFloor.canceled += instance.OnTeleport3rdFloor;
                @ResetGame.started += instance.OnResetGame;
                @ResetGame.performed += instance.OnResetGame;
                @ResetGame.canceled += instance.OnResetGame;
                @Skip204.started += instance.OnSkip204;
                @Skip204.performed += instance.OnSkip204;
                @Skip204.canceled += instance.OnSkip204;
                @OpenDoors.started += instance.OnOpenDoors;
                @OpenDoors.performed += instance.OnOpenDoors;
                @OpenDoors.canceled += instance.OnOpenDoors;
                @Ending.started += instance.OnEnding;
                @Ending.performed += instance.OnEnding;
                @Ending.canceled += instance.OnEnding;
            }
        }
    }
    public CheatsActions @Cheats => new CheatsActions(this);
    public interface ILocomotionActions
    {
        void OnMove(InputAction.CallbackContext context);
        void OnLook(InputAction.CallbackContext context);
        void OnJump(InputAction.CallbackContext context);
    }
    public interface IInteractionActions
    {
        void OnInteract(InputAction.CallbackContext context);
        void OnPushHorizontal(InputAction.CallbackContext context);
        void OnPushVertical(InputAction.CallbackContext context);
        void OnFlashLight(InputAction.CallbackContext context);
    }
    public interface IMenuActions
    {
        void OnPause(InputAction.CallbackContext context);
        void OnSelect(InputAction.CallbackContext context);
        void OnSubmit(InputAction.CallbackContext context);
        void OnBack(InputAction.CallbackContext context);
    }
    public interface ICheatsActions
    {
        void OnTeleport1stFloor(InputAction.CallbackContext context);
        void OnTeleport2ndFloor(InputAction.CallbackContext context);
        void OnTeleport3rdFloor(InputAction.CallbackContext context);
        void OnResetGame(InputAction.CallbackContext context);
        void OnSkip204(InputAction.CallbackContext context);
        void OnOpenDoors(InputAction.CallbackContext context);
        void OnEnding(InputAction.CallbackContext context);
    }
}
