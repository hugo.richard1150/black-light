﻿using UnityEngine;
using UnityEngine.InputSystem;

namespace CharacterController_1Person
{
    public class CInput : MonoBehaviour, IInput
    {
        private static InputManager _instance;
        public static InputManager Instance
        {
            get => _instance;
            set => _instance = value;
        }
        public InteractionInputData interactionInputData;
        [HideInInspector] public Controls controls;
        [HideInInspector] public Gamepad gamepad;

        private Vector2 movementInput;
        private Vector2 mouseInput;

        public float actualSpeed;
        protected float mouseSpeed;
        protected float padSpeed;

        protected bool pushFlag;
        public bool gamePadConnect;
        public bool mouseActive;
        [HideInInspector] public float currentTime;
        [HideInInspector] public bool isMoving;
        [HideInInspector] public bool isCarrying;
        [HideInInspector] public bool interactionOneTap;
        [SerializeField] private bool isPaused;

        #region Attribution des valeurs de l'interface
        public Controls Controls => controls;
        public bool GamePadConnect => gamePadConnect;
        public Vector2 MovementInput => movementInput;
        public Vector2 MouseInput => mouseInput;
        public float ActualSpeed => actualSpeed;
        public float MouseSpeed => mouseSpeed;
        public float PadSpeed => padSpeed;
        public bool IsMoving => isMoving;
        public bool PushFlag => pushFlag;
        public bool IsCarrying => isCarrying;
        public bool InteractionOnetap => interactionOneTap;
        public bool IsPaused => isPaused;
        public float CurrentTime => currentTime;
        #endregion

        #region Collect des valeurs input
        public Vector2 GetMovement()
        {
            return controls.Locomotion.Move.ReadValue<Vector2>();
        }
        public Vector2 GetMouse()
        {
            return controls.Locomotion.Look.ReadValue<Vector2>();
        }

        public bool GetJump()
        {
            return controls.Locomotion.Jump.triggered;
        }

        public bool GetActionShort()
        {
            return controls.Interaction.Interact.triggered;
        }

        public float GetMenuSelect()
        {
            return controls.Menu.Select.ReadValue<float>();
        }

        public bool GetSubmit()
        {
            return controls.Menu.Submit.triggered;
        }

        public bool GetFlashLight()
        {
            return controls.Interaction.FlashLight.triggered;
        }
        public bool GameisPaused()
        {
            return controls.Menu.Pause.triggered;
        }
        public bool BackButton()
        {
            return controls.Menu.Back.triggered;
        }
        #endregion
    }

    public interface IInput
    {
        Controls Controls
        {
            get;
        }
        bool GamePadConnect
        {
            get;
        }
        Vector2 MovementInput
        {
            get;
        }
        Vector2 MouseInput
        {
            get;
        }
        float ActualSpeed
        {
            get;
        }

        float MouseSpeed
        {
            get;
        }
        float PadSpeed
        {
            get;
        }
        float CurrentTime
        {
            get;
        }
        bool IsMoving
        {
            get;
        }
        bool PushFlag
        {
            get;
        }
        bool IsCarrying
        {
            get;
        }

        bool IsPaused
        {
            get;
        }
    }
}
