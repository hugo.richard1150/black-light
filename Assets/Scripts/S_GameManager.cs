﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CharacterController_1Person;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class S_GameManager : MonoBehaviour
{
    public InputManager inputManager;
    public GameObject master;
    public GameObject pauseMenu;
    bool checkpause;
    bool newGamePadConnect;
    GameObject eventsyst;

    private void Start()
    {
        eventsyst = GameObject.Find("EventSystem");
    }
    void Update()
    {
        Debug.developerConsoleVisible = false;

        //Debug.Log("inputManager.gamePadConnect :  " + inputManager.gamePadConnect);
        if (inputManager.GameisPaused())
        {
            if (checkpause)
            {
                Resume();
            }
            else
            {
                pauseM();
            }
        }
        if(newGamePadConnect != inputManager.gamePadConnect)
        {
            if (inputManager.gamePadConnect == true)
            {
                Cursor.visible = false;
                eventsyst.GetComponent<UnityEngine.EventSystems.EventSystem>().SetSelectedGameObject(master);
                newGamePadConnect = inputManager.gamePadConnect;
            }
            else
            {
                Cursor.visible = true;
                eventsyst.GetComponent<UnityEngine.EventSystems.EventSystem>().SetSelectedGameObject(null);
                newGamePadConnect = inputManager.gamePadConnect;
            }
        }
    }

    public void pauseM()
    {
        pauseMenu.gameObject.SetActive(true);
        GameObject.Find("CharacterController (1Person)").GetComponent<PlayerManager>().enabled = false;
        Time.timeScale = 0f;
        checkpause = true;
        if (inputManager.gamePadConnect == true)
        {
                eventsyst.GetComponent<UnityEngine.EventSystems.EventSystem>().SetSelectedGameObject(null);

                Cursor.visible = false;
                eventsyst.GetComponent<UnityEngine.EventSystems.EventSystem>().SetSelectedGameObject(master);

        }
        else
        {
                Cursor.visible = true;
                eventsyst.GetComponent<UnityEngine.EventSystems.EventSystem>().SetSelectedGameObject(null);
        }
        newGamePadConnect = inputManager.gamePadConnect;
    }

    public void Resume()
    {
        pauseMenu.gameObject.SetActive(false);
        GameObject.Find("CharacterController (1Person)").GetComponent<PlayerManager>().enabled = true;
        Time.timeScale = 1;
        Cursor.visible = false;
        checkpause = false;
    }

    public void Quit()
    {
        Application.Quit(); 
    }
}
