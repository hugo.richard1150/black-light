﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CharacterController_1Person
{
    public class Collect : Interactable
    {
        [SerializeField] private PlayerManager playerManager;
        [SerializeField] private InputManager inputManager;
        [SerializeField] private Transform mainCamera;

        public Transform interactionPoint;

        [SerializeField] private float force = 500f;

        private bool carried = false;
        public bool touched = false;

        public void Start()
        {
            inputManager = GameObject.FindGameObjectWithTag("Player").GetComponent<InputManager>();
            playerManager = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerManager>();
            mainCamera = Camera.main.transform;
        }

        public override void OnInteract()
        {
            base.OnInteract();
            Pick();
        }

        private void FixedUpdate()
        {
            if (carried)
            {
                //GameObject.Find("InteractionUIPanel").GetComponent<InteractionPanel>().ResetUI();

                if (touched)
                {
                    inputManager.isCarrying = false;
                    GetComponent<Rigidbody>().isKinematic = false;
                    transform.parent = null;
                    carried = false;
                    touched = false;
                    playerManager.item = 0;
                }

                if (!inputManager.isCarrying)
                {
                    GetComponent<Rigidbody>().isKinematic = false;
                    transform.parent = null;
                    carried = false;
                    GetComponent<Rigidbody>().AddForce(mainCamera.forward * force);
                    playerManager.item = 0;
                }
            }
        }

        public void Pick()
        {
            inputManager.isCarrying = true;
            playerManager.item = 1;
            GetComponent<Rigidbody>().isKinematic = true;
            transform.position = interactionPoint.position;
            transform.parent = interactionPoint;
            carried = true;
        }

        /*private void OnTriggerEnter()
        {
            if (carried)
            {
                touched = true;
            }
        }*/
    }
}
