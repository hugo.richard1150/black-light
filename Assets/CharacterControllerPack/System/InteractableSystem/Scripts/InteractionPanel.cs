﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace CharacterController_1Person
{
    public class InteractionPanel : MonoBehaviour
    {
        [Space]
        [Header("References")]
        public Interactable interactable;
        public ActDialog actDialog;
        [Space]
        [Header("Variables Settings")]
        public int actualAnim;

        public void SetTooltip(TextMeshProUGUI messageText, string tooltip)
        {
            messageText.SetText(tooltip);
        }

        public void DisplayInteractAnim(Animator target, string animName, int animState)
        {
            target.Play(animName);
            actualAnim = animState;
        }

        public void UpdateProgressBar(Image progressBar,float fillAmount)
        {
            if (progressBar != null)
            {
                progressBar.fillAmount = fillAmount;
            }
        }

        public void ResetUI(Image progressBar, TextMeshProUGUI messageText)
        {
            progressBar.fillAmount = 0;
            messageText.SetText("");
        }
    }
}
