﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Circle : MonoBehaviour
{
    public GameObject target;

    public float minDistance;
    public float maxDistance;

    public float minScale;
    public float maxScale;

    public float renderDistance;

    public GameObject UIComponent;

    void Update()
    {
        transform.LookAt(target.transform);

        maxDistance = (this.transform.position - target.transform.position).sqrMagnitude;

        float scale = Mathf.Lerp(minScale, maxScale, Mathf.InverseLerp(maxDistance, minDistance, maxScale));
        transform.localScale = new Vector3(scale /8, scale /8, scale /8);

        renderDistance = maxDistance;

        if(renderDistance < 1.4)
        {
            this.transform.GetChild(0).gameObject.SetActive(true);
        }
        else
        {
            this.transform.GetChild(0).gameObject.SetActive(false);
        }

        renderDistance = (this.transform.position - target.transform.position).sqrMagnitude;
    }

    public void Out()
    {
        UIComponent.SetActive(false);
    }
}
