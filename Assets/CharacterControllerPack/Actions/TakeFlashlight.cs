﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CharacterController_1Person;

public class TakeFlashlight : Interactable
{
    InputManager inputManager;
    InteractionManager interactionManager;
    CharacterController ChController;

    GameObject controller;
    public Transform interactionPoint;

    public Camera mainCam;
    public Camera cullingCam;

    private float force = 10;
    public bool objectTake;
    public GameObject gameobjectTaken;

    private void Awake()
    {
        inputManager = GameObject.Find("CharacterController (1Person)").GetComponent<InputManager>();
        interactionManager = GameObject.Find("CharacterController (1Person)").GetComponent<InteractionManager>();
        controller = GameObject.Find("CharacterController (1Person)");
        ChController = controller.GetComponent<CharacterController>();
        //interactionPoint = controller.transform.GetChild(1);
    }
    public override void OnInteract()
    {
        base.OnInteract();
        FlashlightTaking();
    }

    private void FixedUpdate()
    {
        if (objectTake && inputManager.GetActionShort() && interactionManager.keepTasking != 1)
        {
            //Enlever objet
            objectTake = false;
            mainCam.cullingMask |= 1 << LayerMask.NameToLayer("Object");
            cullingCam.gameObject.SetActive(false);
            ChController.radius = 0.5f;
            StartCoroutine("WaitingInteract");
        }

        if (objectTake)
        {
            gameobjectTaken.GetComponent<Rigidbody>().isKinematic = false;
            gameobjectTaken.GetComponent<Rigidbody>().useGravity = false;
            gameobjectTaken.GetComponent<BoxCollider>().enabled = false;
            gameobjectTaken.transform.position = Vector3.Lerp(gameobjectTaken.transform.position, interactionPoint.position, Time.deltaTime * 100);
            gameobjectTaken.transform.parent = interactionPoint.transform;
            gameobjectTaken.transform.localScale = new Vector3(5, 5, 1.428572f);
            gameobjectTaken.transform.localRotation = Quaternion.Euler(90, 0, 90);
        }
        else if (!objectTake && gameobjectTaken != null)
        {
            gameobjectTaken.GetComponent<Rigidbody>().useGravity = true;
            gameobjectTaken.GetComponent<BoxCollider>().enabled = true;
            gameobjectTaken.transform.parent = null;
            gameobjectTaken.transform.localScale = new Vector3(1,1,1);
            gameobjectTaken = null;
        }
    }

    void FlashlightTaking()
    {
        if (!objectTake && transform.gameObject.tag == "Flashlights")
        {
            //Prendre objet
            objectTake = true;
            gameobjectTaken = transform.gameObject;
            mainCam.cullingMask &= ~(1 << LayerMask.NameToLayer("Object"));
            cullingCam.gameObject.SetActive(true);
            ChController.radius = 0.65f;
        }
    }

    IEnumerator WaitingInteract()
    {
        transform.gameObject.layer = 13;
        yield return new WaitForSeconds(0.5f);
        transform.gameObject.layer = 16;
    }

    private void OnCollisionEnter(Collision collision)
    {
        transform.localScale = new Vector3(1, 1, 1);
    }
}
