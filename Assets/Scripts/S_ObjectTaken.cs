﻿using UnityEngine;
using CharacterController_1Person;

public class S_ObjectTaken : MonoBehaviour
{
    private bool objectTake;
    private CharacterController controller;
    public GameObject gameobjectTaken;
    public Transform interactionPoint;
    public Camera mainCam;
    public Camera cullingCam;

    private RaycastHit hit;
    private Ray ray;
    public LayerMask layerObject;

    public FMODUnity.StudioEventEmitter flashlightPickSound;
    private FMODUnity.StudioEventEmitter flashlightFallSound;

    private void Awake()
    {
        controller = GetComponent<CharacterController>();
    }

    private void Update()
    {
        if (mainCam.isActiveAndEnabled)
        {
            ray = Camera.main.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0f));

            if (GameObject.Find("CharacterController (1Person)").GetComponent<InputManager>().GetActionShort())
            {
                if (Physics.Raycast(ray, out hit, Mathf.Infinity, layerObject))
                {
                    if (objectTake && GameObject.Find("CharacterController (1Person)").GetComponent<InteractionManager>().keepTasking != 1)
                    {
                        //Enlever objet
                        objectTake = false;
                        flashlightFallSound.Play();
                        mainCam.cullingMask |= 1 << LayerMask.NameToLayer("Object");
                        cullingCam.gameObject.SetActive(false);
                        controller.radius = 0.5f;
                    }
                    else if (!objectTake && hit.transform.gameObject.tag == "Flashlights")
                    {
                        //Prendre objet
                        objectTake = true;
                        gameobjectTaken = hit.transform.gameObject;
                        flashlightPickSound.Play();
                        mainCam.cullingMask &= ~(1 << LayerMask.NameToLayer("Object"));
                        cullingCam.gameObject.SetActive(true);
                        controller.radius = 0.65f;
                    }
                }
            }

            if (objectTake)
            {
                gameobjectTaken.GetComponent<Rigidbody>().useGravity = false;
                gameobjectTaken.GetComponent<BoxCollider>().enabled = false;
                gameobjectTaken.transform.position = Vector3.Lerp(gameobjectTaken.transform.position, interactionPoint.position, Time.deltaTime * 100);
                gameobjectTaken.transform.parent = interactionPoint.transform;
                gameobjectTaken.transform.localRotation = Quaternion.Euler(90, 0, 90);
            }
            else if (!objectTake && gameobjectTaken != null)
            {
                gameobjectTaken.GetComponent<Rigidbody>().useGravity = true;
                gameobjectTaken.GetComponent<BoxCollider>().enabled = true;
                gameobjectTaken.transform.parent = null;
                gameobjectTaken = null;
            }
        }
    }
}
