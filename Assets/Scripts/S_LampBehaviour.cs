﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;
public class S_LampBehaviour : MonoBehaviour
{
    private Light pointWhiteLight;

    public GameObject[] objectsAffected;
    public enum LightType { WhiteLight, BlackLight};
    public LightType lightType;

    public Color colorWhiteLight;
    public Color colorBlackLight;

    public GameObject[] vfx;

    public GameObject radiusLightCollider;
    private S_SphereLight sphereLightCollider;

    private void Start()
    {
        pointWhiteLight = GetComponentInChildren<Light>();
        RadiusLightCollider();
    }

    private void Update()
    {
        if (lightType == LightType.WhiteLight)
        {
            pointWhiteLight.color = colorWhiteLight;
            pointWhiteLight.intensity = 0.5f;
            sphereLightCollider.whitelight = true;
            radiusLightCollider.transform.localScale = Vector3.Lerp(radiusLightCollider.transform.localScale, new Vector3(pointWhiteLight.range * 2, pointWhiteLight.range * 2, pointWhiteLight.range * 2), 1f);
            foreach (GameObject item in vfx)
            {
                item.gameObject.GetComponent<VisualEffect>().enabled = false;
            }
        }
        else if (lightType == LightType.BlackLight)
        {
            pointWhiteLight.color = colorBlackLight;
            pointWhiteLight.intensity = 1.5f;
            sphereLightCollider.whitelight = false;
            radiusLightCollider.transform.localScale = Vector3.Lerp(radiusLightCollider.transform.localScale, new Vector3(0,0,0), 8f);
            foreach (GameObject item in vfx)
            {
                item.gameObject.GetComponent<VisualEffect>().enabled =true;
            }
        }
    }

    private void RadiusLightCollider()
    {
        radiusLightCollider = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        radiusLightCollider.transform.parent = transform;
        radiusLightCollider.transform.position = transform.position;
        radiusLightCollider.GetComponent<SphereCollider>().isTrigger = true;
        radiusLightCollider.GetComponent<MeshRenderer>().enabled = false;
        sphereLightCollider = radiusLightCollider.AddComponent<S_SphereLight>();
        Rigidbody lightColliderRb = radiusLightCollider.AddComponent<Rigidbody>();
        lightColliderRb.useGravity = false;
    }
}
