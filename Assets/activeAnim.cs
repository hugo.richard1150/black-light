﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class activeAnim : MonoBehaviour
{
    public Animator animatoro;
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            animatoro.enabled = true;
        }
    }
}
