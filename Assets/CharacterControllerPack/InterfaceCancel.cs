﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CharacterController_1Person;

public class InterfaceCancel : MonoBehaviour
{
    void ResetInterface(){
        InteractionManager interactionManager = GetComponent<InteractionManager>();
        if(interactionManager != null){
            interactionManager.interactPanel.DisplayInteractAnim(interactionManager.animCross, "CrossInverse", 0);
            interactionManager.interactPanel.SetTooltip(interactionManager._interactable.messageText, "");
            }
        }
}
