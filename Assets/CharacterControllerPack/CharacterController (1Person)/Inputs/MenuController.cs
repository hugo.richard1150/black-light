﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CharacterController_1Person;

public class MenuController : MonoBehaviour
{
    public InputManager inputManager;
    public int index;
    public bool KeyDown;
    public int maxIndex;

    private void Update()
    {
        if(inputManager.GetMenuSelect() != 0)
        {
            if (!KeyDown)
            {
                if(inputManager.GetMenuSelect() < 0)
                {
                    if(index < maxIndex)
                    {
                        index++;
                    }
                }
                else if(inputManager.GetMenuSelect() > 0)
                {
                    if(index > 0)
                    {
                        index--;
                    }
                    else
                    {
                        index = maxIndex;
                    }
                }
                KeyDown = true;
            }
        }
        else
        {
            KeyDown = false;
        }
    }
}
