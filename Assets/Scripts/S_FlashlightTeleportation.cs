﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class S_FlashlightTeleportation : MonoBehaviour
{
    public Vector3 pos1Floor;
    public Vector3 pos2Floor;
    public Vector3 pos3Floor;

    private S_ObjectTaken objectTaken;
    public GameObject flashlight;

    private void Start()
    {
        objectTaken = FindObjectOfType<S_ObjectTaken>();
    }

    public void OnTriggerStay(Collider other)
    {
        if (objectTaken.gameobjectTaken != null)
        {
            if (other.gameObject.tag == "1FloorFlashlight")
            {
                pos1Floor = flashlight.transform.position;
            }
            else if (other.gameObject.tag == "2FloorFlashlight")
            {
                pos2Floor = flashlight.transform.position;
            }
            else if (other.gameObject.tag == "3FloorFlashlight")
            {
                pos3Floor = flashlight.transform.position;
            }
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        if (objectTaken.gameobjectTaken == null)
        {
            if (other.gameObject.tag == "1FloorFlashlight")
            {
                flashlight.transform.position = pos1Floor;
            }
            else if (other.gameObject.tag == "2FloorFlashlight")
            {
                flashlight.transform.position = pos2Floor;
            }
            else if (other.gameObject.tag == "3FloorFlashlight")
            {
                flashlight.transform.position = pos3Floor;
            }
        }
    }
}
