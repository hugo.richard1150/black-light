﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CharacterController_1Person;

public class S_bustedDiag : MonoBehaviour
{
    [Header("Reference")]
    public string[] bustedDialog;
    public void nameBusted()
    {
        int indexBusted = 0;
        if (bustedDialog != null && indexBusted < bustedDialog.Length)
        {
            foreach (string item in bustedDialog)
            {
                string tmpString = bustedDialog[indexBusted];
                GameObject.Find(tmpString).GetComponent<ActDialog>().busted = true;
                // Debug.Log(tmpString);
                indexBusted += 1;
            }
        }
    }
}