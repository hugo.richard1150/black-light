﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CharacterController_1Person
{
    [CreateAssetMenu(fileName = "Interaction Data", menuName = "InteractionSystem/InteractionData")]
    public class InteractionData : ScriptableObject
    {
        private Interactable b_interactable;

        public Interactable Interaction
        {
            get => b_interactable;
            set => b_interactable = value;
        }

        public void Interact()
        {
            b_interactable.OnInteract();
            ResetData();
        }

        public bool IsSameInteractable(Interactable _newInteractable) => b_interactable == _newInteractable;
        public bool IsEmpty() => b_interactable == null;
        public void ResetData() => b_interactable = null;

    }
}
