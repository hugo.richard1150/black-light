﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CharacterController_1Person;

public class S_Room306 : MonoBehaviour
{
    public S_LampBehaviour corridorLamp;
    public GameObject door;
    public Collider entranceTrigger;
    //public bool playerHasEntered = false;
    [SerializeField] private bool doorUnlocked = false;

    void Update()
    {
        if (!doorUnlocked)
        {
            if (corridorLamp.lightType == S_LampBehaviour.LightType.WhiteLight)
            {
                SwitchActivated();
            }
        }
    }

    public void PlayerHasEntered()
    {
        door.GetComponent<Open>().DoorClose();
        door.GetComponent<Open>().condition = false;
        doorUnlocked = false;
    }
    
    void SwitchActivated()
    {
        door.GetComponent<Open>().DoorOpen();
        doorUnlocked = true;
    }
}
