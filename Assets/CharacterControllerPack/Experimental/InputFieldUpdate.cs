﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class InputFieldUpdate : MonoBehaviour
{
    public InputField inputField;
    public string actualValue;
    public Text text;

    private void Awake()
    {
        inputField = this.gameObject.GetComponent<InputField>();
        text = inputField.GetComponent<InputField>().textComponent;
    }

    public void FieldUpdate(Slider targetSlider)
    {
        actualValue = targetSlider.value.ToString();
        text.text = actualValue;
    }
}
