﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Detection : MonoBehaviour
{
    public Collider[] interactableCurrent;
    public Collider[] interactableIn;
    public Collider[] interactablOut;

    public float size;
    public LayerMask layer;

    private void FixedUpdate()
    {
        interactableIn = Physics.OverlapSphere(transform.position, size, layer);

        /*foreach (var interact in interactableIn)
        {
            UI ui = interact.GetComponent<UI>();
            ui.active = true;
            ui.Display();
        }

        interactablOut = interactableCurrent.Except(interactableIn).ToArray();

        foreach (var interact in interactablOut)
        {
            UI ui = interact.GetComponent<UI>();
            ui.active = false;
            ui.Display();
        }*/
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, size);
    }
}
