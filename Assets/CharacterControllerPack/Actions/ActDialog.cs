﻿using System.Collections;
using UnityEngine;
using TMPro;
using CharacterController_1Person;
using UnityEngine.UI;

public class ActDialog : MonoBehaviour
{
    [Space]
    [Header("Variables Settings")]
    public bool readAuto;
    public bool mustBeSeen = false;
    public bool needCompare = false;
    public bool busted = false;
    public int read = 0;
    public bool secondFloorEntrance = false;
    public bool thirdFloorEntrance = false;
    private S_MusicManager musicManager;
    [Space]
    [Header("Data")]
    public DialogData dialogData;
    public DialogData dialogDataCompare;
    public FMOD.Studio.EventInstance eventCall;
    public string fmodNameEvent;
    [Space]
    [Header("UserInterface Settings")]
    public Image iconTimer;
    public Image progressBar;
    public TextMeshProUGUI text;
    public GameObject dialogPanel;
    public TextMeshProUGUI textDisplay;

    private void Awake()
    {
        dialogData.deblockText = "";
        dialogData.index = 0;
        busted = false;
        read = 0;

        dialogPanel = GameObject.Find("DialogPanel");
        textDisplay = dialogPanel.transform.GetChild(0).GetComponent<TextMeshProUGUI>();
    }

    private void Start()
    {
        dialogPanel.SetActive(false);
        musicManager = GameObject.Find("MusicManager").GetComponent<S_MusicManager>();
    }

    //Display and start the reading dialog
    public void StartDialog()
    {
   
        if (busted == false)
        {
            if (read == 0)
            {
                if (mustBeSeen)
                {
                    nameBusted();
                    dialogPanel.SetActive(true);
                    StartCoroutine(Type());
                }
                else
                {
                    if (!string.IsNullOrEmpty(dialogData.needDeblock) && dialogDataCompare.deblockText == dialogData.needDeblock && needCompare == true)
                    {
                        nameBusted();
                        dialogPanel.SetActive(true);
                        StartCoroutine(Type());
                    }
                    else if (needCompare == false)
                    {
                        nameBusted();
                        dialogPanel.SetActive(true);
                        StartCoroutine(Type());
                    }
                }
            }
        }



    }
    private void nameBusted()
    {
        int indexBusted = 0;
        if (dialogData.NameADBusted != null && indexBusted<dialogData.NameADBusted.Length)
        {
            foreach (string item in dialogData.NameADBusted)
            {
                string tmpString = dialogData.NameADBusted[indexBusted];
                 GameObject.Find(tmpString).GetComponent<ActDialog>().busted = true;
               // Debug.Log(tmpString);
                indexBusted += 1;
            }
        }
     }
    //Fonction for typing the dialog (each letter in sentences with a specific timing)
    IEnumerator Type()
    {
        if (dialogData.index < dialogData.voiceName.Length)
        {
            eventCall = FMODUnity.RuntimeManager.CreateInstance("event:/voices/" + dialogData.voiceName[dialogData.index]);
            eventCall.start();
            eventCall.release();
        }
        foreach (char letter in dialogData.sentences[dialogData.index].ToCharArray())
        {
            textDisplay.text += letter;
            yield return new WaitForSeconds(dialogData.typingSpeed[dialogData.index]);
        }
        if (textDisplay.text == dialogData.sentences[dialogData.index])
        {
            if (readAuto)
            {
                yield return new WaitForSeconds(dialogData.waitingSpeed[dialogData.index]);
                NextSentence();
            }
            else
            {
                if(dialogData.index < dialogData.sentences.Length - 1)
                {
                    yield return new WaitForSeconds(dialogData.waitingSpeed[dialogData.index]);
                    textDisplay.text = "";
                    dialogPanel.SetActive(false);
                    dialogData.index++;
                }
                else
                {
                    read = 1;
                    yield return new WaitForSeconds(dialogData.waitingSpeed[dialogData.index]);
                    textDisplay.text = "";
                    dialogPanel.SetActive(false);
                }
            }
        }
    }

    //Fonction for read the entire dialog in one time
    public void NextSentence()
    {
        if (dialogData.index < dialogData.sentences.Length - 1)
        {
            dialogData.index++;
            textDisplay.text = "";
            StartCoroutine(Type());
        }
        else
        {
            read = 1;
            textDisplay.text = "";
            dialogPanel.SetActive(false);
            dialogData.deblockText = dialogData.name;
        }
    }
    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player") && !dialogPanel.activeSelf)
        {
            GetComponent<BoxCollider>().enabled = false;
            StartDialog();

            if (secondFloorEntrance && !musicManager.secondFloor)
            {
                musicManager.SecondFloorMusic();
            }
            else if (thirdFloorEntrance && !musicManager.thirdFloor)
            {
                musicManager.ThirdFloorMusic();
            }
        }
    }
}

