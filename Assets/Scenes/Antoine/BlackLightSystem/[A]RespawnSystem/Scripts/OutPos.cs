﻿using UnityEngine;

public class OutPos : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Player"))
        {
            other.gameObject.transform.position = GameManager.Instance.lastCheckPoint.position;
            Physics.SyncTransforms();
        }
    }
}
