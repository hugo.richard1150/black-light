﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class isActive : MonoBehaviour
{
    public GameObject condition;
    public LayerMask actualLayer;

    private void Awake()
    {
        actualLayer = gameObject.layer;
    }

    private void Update()
    {
        ConditionConfirm(condition);
    }

    private void ConditionConfirm(GameObject obj)
    {
        if (obj.GetComponent<MeshRenderer>().enabled == false)
        {
            gameObject.layer = 0;
        }
        else
        {
            gameObject.layer = actualLayer;
        }
    }
}
