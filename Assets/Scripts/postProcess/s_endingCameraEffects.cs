﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Rendering.PostProcessing;
using UnityEngine.UI;

public class s_endingCameraEffects : MonoBehaviour
{
    private PostProcessVolume volume;
    private Image fadeToWhiteImage;
    public ActDialog dialog;
    private S_MusicManager music;

    private bool phase0 = false;
    private bool phase0End = false;
    private bool phase1 = false;
    private bool phase2 = false;
    private bool phase3 = false;
    private bool phase4 = false;
    private bool phase5 = false;
    private bool phase6 = false;
    private bool phase7 = false;

    private float timeFade = 0f;
    private float currentAlpha = 0f;
    private float endingFadeTimer = 0f;

    private void Start()
    {
        volume = gameObject.GetComponent<PostProcessVolume>();
        fadeToWhiteImage = GameObject.Find("FadeToWhite").GetComponent<Image>();
        music = GameObject.Find("MusicManager").GetComponent<S_MusicManager>();
    }

    private void Update()
    {
        if (phase0)
        {
            endingFadeTimer = endingFadeTimer + 2 * Time.deltaTime;

            currentAlpha = Mathf.Lerp(0f, 1f, endingFadeTimer);

            fadeToWhiteImage.color = new Color(0, 0, 0, currentAlpha);
        }
        else if (phase0End)
        {
            endingFadeTimer = endingFadeTimer + Time.deltaTime;

            currentAlpha = Mathf.Lerp(1f, 0f, endingFadeTimer);

            fadeToWhiteImage.color = new Color(0, 0, 0, currentAlpha);
        }
        else if (phase1)
        {
            Vignette vignette;
            endingFadeTimer = 0f;
            timeFade = timeFade + 0.5f * Time.deltaTime;
            volume.profile.TryGetSettings(out vignette);

            float value = Mathf.Lerp(1f, 0.35f, timeFade);
            vignette.intensity.value = value;
        }
        else if (phase2)
        {
            Vignette vignette;

            volume.profile.TryGetSettings(out vignette);

            vignette.intensity.value = Mathf.Lerp(0.35f, 0.60f, Mathf.PingPong(Time.time, 0.5f));
        }
        else if (phase3)
        {
            Vignette vignette;
            LensDistortion lens;
            timeFade = timeFade + 0.25f * Time.deltaTime;

            volume.profile.TryGetSettings(out vignette);
            volume.profile.TryGetSettings(out lens);

            vignette.intensity.value = Mathf.Lerp(0.35f, 0.60f, Mathf.PingPong(Time.time, 0.5f));
            lens.intensity.value = Mathf.Lerp(0f, -100f, timeFade);
        }
        else if (phase4)
        {
            Vignette vignette;
            LensDistortion lens;
            timeFade = timeFade + 0.25f * Time.deltaTime;

            volume.profile.TryGetSettings(out vignette);
            volume.profile.TryGetSettings(out lens);

            vignette.intensity.value = Mathf.Lerp(0.35f, 0.60f, Mathf.PingPong(Time.time, 0.5f));
            lens.intensity.value = Mathf.Lerp(-100f, 0f, timeFade);
        }
        else if (phase5)
        {
            ChromaticAberration chromatic;
            Vignette vignette;
            LensDistortion lens;
            timeFade = timeFade + 0.5f * Time.deltaTime;

            volume.profile.TryGetSettings(out chromatic);
            volume.profile.TryGetSettings(out vignette);
            volume.profile.TryGetSettings(out lens);


            vignette.intensity.value = Mathf.Lerp(0.35f, 0.60f, Mathf.PingPong(Time.time, 0.5f));
            Color vignetteColor = Color.Lerp(Color.black, Color.red, timeFade);
            float value = Mathf.Lerp(0.5f, 0.35f, timeFade);
            chromatic.intensity.value = value;
            vignette.color.value = vignetteColor;
            lens.intensity.value = Mathf.Lerp(0f, 70f, timeFade);
        }
        else if (phase6)
        {
            ChromaticAberration chromatic;
            Vignette vignette;
            timeFade = timeFade + 0.5f * Time.deltaTime;

            volume.profile.TryGetSettings(out chromatic);
            volume.profile.TryGetSettings(out vignette);

            vignette.intensity.value = Mathf.Lerp(0.35f, 0.50f, Mathf.PingPong(Time.time, 0.1f));
            float value = Mathf.Lerp(0.2f, 1, timeFade);
            chromatic.intensity.value = value;
            vignette.color.value = Color.red;
        }
        else if (phase7)
        {
            ChromaticAberration chromatic;
            Vignette vignette;
            endingFadeTimer = endingFadeTimer + 0.5f * Time.deltaTime;

            volume.profile.TryGetSettings(out chromatic);
            volume.profile.TryGetSettings(out vignette);

            vignette.intensity.value = Mathf.Lerp(0.35f, 0.50f, Mathf.PingPong(Time.time, 0.1f));
            chromatic.intensity.value = 1;
            vignette.color.value = Color.red;

            currentAlpha = Mathf.Lerp(0f, 1f, endingFadeTimer);

            fadeToWhiteImage.color = new Color(1, 1, 1, currentAlpha);
        }
    }

    public void EndingDialog()
    {
        dialog.StartDialog();
    }

    public void EndingPhase0()
    {
        timeFade = 0;
        phase0 = true;
        music.underground = true;
    }

    public void EndingPhase0End()
    {
        timeFade = 0;
        phase0 = false;
        phase0End = true;
    }

    public void EndingPhase1()
    {
        timeFade = 0;
        phase0End = false;
        phase1 = true;
    }

    public void EndingPhase2()
    {
        timeFade = 0;
        phase1 = false;
        phase2 = true;
    }

    public void EndingPhase3()
    {
        timeFade = 0;
        phase2 = false;
        phase3 = true;
    }

    public void EndingPhase4()
    {
        timeFade = 0;
        phase3 = false;
        phase4 = true;
    }

    public void EndingPhase5()
    {
        timeFade = 0;
        phase4 = false;
        phase5 = true;
    }

    public void EndingPhase6()
    {
        timeFade = 0;
        phase5 = false;
        phase6 = true;
    }

    public void EndingPhase7()
    {
        timeFade = 0;
        phase6 = false;
        phase7 = true;
    }

    public void Ending()
    {
        Destroy(GameObject.Find("MusicManager"));
        SceneManager.LoadScene(0);
    }
}
