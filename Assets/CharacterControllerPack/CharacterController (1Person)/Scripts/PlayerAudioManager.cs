﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CharacterController_1Person;

public class PlayerAudioManager : MonoBehaviour
{
    private PlayerLocomotion player;

    private void Start()
    {
        player = gameObject.GetComponentInParent<PlayerLocomotion>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (player.isJumping)
            player.landed = true;

        if (other.gameObject.CompareTag("FloorConcrete"))
        {
            player.stepsSound.SetParameter("GroundType", 0);
            player.fallSound.SetParameter("FallGroundType", 0);
            player.flashlightSound.SetParameter("GroundType", 0);
        }
        else if (other.gameObject.CompareTag("FloorWood"))
        {
            player.stepsSound.SetParameter("GroundType", 1);
            player.fallSound.SetParameter("FallGroundType", 1);
            player.flashlightSound.SetParameter("GroundType", 1);
        }
        else if (other.gameObject.CompareTag("FloorCarpet"))
        {
            player.stepsSound.SetParameter("GroundType", 2);
            player.fallSound.SetParameter("FallGroundType", 2);
            player.flashlightSound.SetParameter("GroundType", 2);
        }
        else if (other.gameObject.CompareTag("Furniture"))
        {
            player.stepsSound.SetParameter("GroundType", 3);
            player.fallSound.SetParameter("FallGroundType", 3);
            player.flashlightSound.SetParameter("GroundType", 3);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (player.isJumping)
            player.landed = false;
    }
}
