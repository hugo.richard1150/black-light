﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class S_HighlightObject : MonoBehaviour
{
    private Renderer _renderer;
    private MaterialPropertyBlock _propBlock;

    public void Update()
    {
        _propBlock = new MaterialPropertyBlock();
        _renderer = GetComponent<Renderer>();

        _renderer.GetPropertyBlock(_propBlock);

        _propBlock.SetFloat("Boolean_8ED864", 1);
        _propBlock.SetFloat("Vector1_FE91FFE2", 50);

        _renderer.SetPropertyBlock(_propBlock);
    }
}
