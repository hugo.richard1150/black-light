﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class s_fadeVignette : MonoBehaviour
{
    public PostProcessVolume volume;
    public PostProcessVolume endingVolume;
    private float timeFade = 0f;
    private bool block = false;

    void Update()
    {
        if (block == true)
        {
            Vignette vignette;
            timeFade = timeFade + 0.5f*Time.deltaTime;
            volume.profile.TryGetSettings(out vignette);
            endingVolume.profile.TryGetSettings(out vignette);
            vignette.rounded.overrideState = true;
            vignette.rounded.value = true;
            float value = Mathf.Lerp(0f, 1f, timeFade);
            // Debug.Log(value);
            vignette.intensity.value = value;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            block = true;
        }

    }
}
