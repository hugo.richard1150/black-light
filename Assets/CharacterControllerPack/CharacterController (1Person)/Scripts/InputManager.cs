﻿using UnityEngine;
using UnityEngine.InputSystem;

namespace CharacterController_1Person
{
    public class InputManager : CInput
    {
        public GameObject[] optionMenu;
        public S_Menu menu;
        private void Awake()
        {
#if UNITY_EDITOR
            Debug.unityLogger.logEnabled = true;
#else
            Debug.unityLogger.logEnabled = false;
#endif

            //Création de l'instance pour être récupérer de n'importe ou
            if(Instance != null && Instance != this)
            {
                Destroy(this.gameObject);
            }
            else
            {
                Instance = this;
            }
            //Setup du curseur au début et attribution de l'état du gampad actuel à la valeur gamepad
            Cursor.visible = false;
            gamepad = Gamepad.current;
        }
        private void Start()
        {
            //Remise à zéro de l'état de l'UI
            interactionInputData.ResetInput();
            currentTime = 0;
        }
        private void Update()
        {
            //Attribution et déclanchement de la fonction qui s'occupe de connaître l'état du gamepad (connecter ou déconnecter)
            gamepad = Gamepad.current;
            GamepadState("Gamepad Connected !", "Gamepad Disconnected !");
            GetInteractionInputData();
            for (int i = 0; i < optionMenu.Length; i++)
            {
                if (optionMenu[i].activeInHierarchy && BackButton() == true)
                {
                    for (int z = 0; z < optionMenu.Length; z++)
                    {
                        optionMenu[z].SetActive(false);
                        if (menu != null)
                        {
                            menu.voidBackButton();
                        }
                    }
                }
            }
        }
        private void OnEnable()
        {
            //Lorsque les inputs sont enable
            if (controls == null)
            {
                controls = new Controls();
            }
            controls.Enable();
        }
        private void OnDisable()
        {
            //Lorsque les inputs sont disable
            controls.Disable();
        }

        public void GetInteractionInputData()
        {
            //Attribution de variable (bool, float, trigger) à différent input
            controls.Interaction.Interact.started += ctx => { interactionInputData.InteractedClicked = true; pushFlag = true; };
            controls.Interaction.Interact.canceled += ctx => { interactionInputData.InteractedReleased = true; pushFlag = false; };
 
            if (interactionInputData.InteractedClicked && interactionInputData.InteractedReleased)
            {
                interactionInputData.ResetInput();
            }

            if (isCarrying)
            {
                controls.Interaction.Interact.started += ctx => isCarrying = false;
            }

            interactionOneTap = GetActionShort();
        }

        private void GamepadState(string debugMessage_connected, string debugMessage_disconnected)
        {
            //Fonction qui renvoie l'état de la manettte
            if (gamepad != null)
            {
                gamePadConnect = true;
                actualSpeed = padSpeed;

                if (gamePadConnect)
                {
                    if (currentTime == 0)
                    {
                        Debug.LogWarning(debugMessage_connected);
                        currentTime = 1;
                    }
                }
            }
            else
            {
                gamePadConnect = false;
                actualSpeed = mouseSpeed;

                if (!gamePadConnect)
                {
                    if (currentTime == 1)
                    {
                        Debug.LogWarning(debugMessage_disconnected);
                        currentTime = 0;
                    }
                }
            }
        }
    }
}
