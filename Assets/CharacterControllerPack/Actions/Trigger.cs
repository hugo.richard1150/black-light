﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CharacterController_1Person;

public class Trigger : Interactable
{
    [Space]
    [Header("References")]
    //References elements
    private TakeFlashlight takeFlashlight;
    public Open targetLock;
    public GameObject flashlight;
    public GameObject support;
    public Transform conditionPos1;
    public Transform flashlightParentPoint;
    private GameObject playerHand;
    [Space]
    [Header("Variables Settings")]
    //References variables
    public int capacity = 1;
    public bool flashlightIsHere;
    public bool followFlashlight;

    private void Awake()
    {
        playerHand = GameObject.Find("CharacterController (1Person)").transform.GetChild(1).gameObject;
        takeFlashlight = GameObject.Find("Flashlight (1)").GetComponent<TakeFlashlight>();
    }

    private void Update()
    {
        UnlockCondition();
        SupportVelocity(flashlight);
    }

    private void FixedUpdate()
    {
        FollowTriggerSupport(support.transform);
    }

    void FollowTriggerSupport(Transform support)
    {
        support = support.gameObject.transform;
        transform.position = new Vector3(transform.position.x, transform.position.y, support.transform.position.z);
    }

    void SupportVelocity(GameObject flashlight)
    {
        if(capacity == 0 && flashlightIsHere)
        {
            if (support.GetComponent<Push>().move)
            {
                flashlight.layer = 0;
            }
            else
            {
                flashlight.layer = 16;
            }
        }
    }

    void UnlockCondition()
    {
        bool condition1 = support.transform.position.z > -5.7f && support.transform.position.z < -5.5f;

        if(flashlightIsHere)
        {
            if(condition1)
            {
                targetLock.condition = true;
            }
            else
            {
                targetLock.condition = false;
            }
        }
        else
        {
            targetLock.condition = false;
        }

        if (followFlashlight)
        {
            flashlight.transform.parent = flashlightParentPoint.transform;
        }
        else
        {
            if(flashlight != null)
            {
                flashlight.transform.parent = null;
            }
        }
    }

    public override void OnInteract()
    {
        base.OnInteract();

        if (capacity == 1 && !flashlightIsHere)
        {
            AddFlashlight(flashlight);
        }
    }

    void AddFlashlight(GameObject flashlight)
    {
        if(flashlight != null)
        {
            takeFlashlight.objectTake = false;

            flashlight.GetComponent<Rigidbody>().isKinematic = true;
            takeFlashlight.gameobjectTaken.transform.parent = null;
            flashlight.transform.position = flashlightParentPoint.transform.position;
            flashlight.transform.rotation = flashlightParentPoint.transform.rotation;

            capacity = 0;
            followFlashlight = true;
            flashlightIsHere = true;

            transform.gameObject.layer = 0;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if(other.gameObject.transform.GetChild(1).childCount > 0)
            {
                transform.gameObject.layer = 12;
                capacity = 1;
                flashlightIsHere = false;
                followFlashlight = false;

                flashlight = other.gameObject.transform.GetChild(1).transform.GetChild(0).gameObject;
                Debug.Log(other.name);
            }
            else
            {
                //transform.gameObject.layer = 0;
            }
        }
    }
}
